/*
 *  File:   IMMDeviceEnumeratorEmulator.hpp
 *  Author: Anenzoras
 *  Date:   2019-02-12
 *
 *  Brief:  Definition of the fake class that will proxy an IMMDeviceEnumerator
 *  object after the call to the fake CoCreateInstance function.
 */

#ifndef IMMDEVICEENUMERATOREMULATOR_HPP
#define IMMDEVICEENUMERATOREMULATOR_HPP

/**/

/* External library includes */
#include <mmdeviceapi.h> /* Core Audio for IMMDeviceEnumerator */

/**
 *  @property {IID const} IID_IMMDeviceEnumerator
 *      The unique identifier for the IMMDeviceEnumerator interface.
 */
extern IID const IID_IMMDeviceEnumerator;

/**
 *  @class IMMDeviceEnumeratorEmulator
 *      Class that will replace the IMMDeviceEnumerator object returned by
 *      CoCreateInstance and act as a proxy.
 *
 *  @note
 *      The class keeps track of the original object, but increment its
 *      reference counter when instancied. This means the .CreateInstance()
 *      caller can call Release() on the parameter without fear afterward.
 *
 *  @note
 *      Documentation of interface functions comes from Microsoft's official
 *      documentation.
 */
class IMMDeviceEnumeratorEmulator
    : public IMMDeviceEnumerator
{
public:
    /*
     * IMMDeviceEnumeratorEmulator stuff
     */
    /**
     *  @function [static] CreateInstance
     *      Returns an instance of IMMDeviceEnumeratorEmulator to encapsulate
     *      thingies.
     *
     *  @param {IMMDeviceEnumerator *} original
     *      The original IMMDeviceEnumerator object pointer, so we keep track
     *      of things.
     *
     *  @returns {IMMDeviceEnumeratorEmulator *}
     *      A pointer to our newly created proxy.
     */
    static IMMDeviceEnumeratorEmulator * CreateInstance(
        IMMDeviceEnumerator * original);

    /**
     *  @function ~IMMDeviceEnumeratorEmulator
     *      Destructor. Releases its reference to the original object.
     */
    virtual ~IMMDeviceEnumeratorEmulator();

    /*
     * IMMDeviceEnumerator stuff
     */
    /**
     *  @function EnumAudioEndpoints
     *      The EnumAudioEndpoints method generates a collection of audio
     *      endpoint devices that meet the specified criteria.
     *
     *  @param {EDataFlow} dataFlow
     *      The data-flow direction for the endpoint devices in the collection.
     *      The caller should set this parameter to one of the following
     *      EDataFlow enumeration values:
     *      - eRender
     *      - eCapture
     *      - eAll
     *      If the caller specifies eAll, the method includes both rendering and
     *      capture endpoints in the collection.
     *  @param {DWORD} dwStateMask
     *      The state or states of the endpoints that are to be included in the
     *      collection. The caller should set this parameter to the bitwise OR
     *      of one or more of the following DEVICE_STATE_XXX constants:
     *      - DEVICE_STATE_ACTIVE
     *      - DEVICE_STATE_DISABLED
     *      - DEVICE_STATE_NOTPRESENT
     *      - DEVICE_STATE_UNPLUGGED
     *      For example, if the caller sets the dwStateMask parameter to
     *      DEVICE_STATE_ACTIVE | DEVICE_STATE_UNPLUGGED, the method includes
     *      endpoints that are either active or unplugged from their jacks, but
     *      excludes endpoints that are on audio adapters that have been
     *      disabled or are not present. To include all endpoints, regardless of
     *      state, set dwStateMask = DEVICE_STATEMASK_ALL.
     *  @param {IMMDeviceCollection **} ppDevices
     *      Pointer to a pointer variable into which the method writes the
     *      address of the IMMDeviceCollection interface of the
     *      device-collection object. Through this method, the caller obtains a
     *      counted reference to the interface. The caller is responsible for
     *      releasing the interface, when it is no longer needed, by calling the
     *      interface's Release method. If the EnumAudioEndpoints call fails,
     *      *ppDevices is NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if parameter ppDevices is NULL.
     *      E_INVALIDARG if parameter dataFlow or dwStateMask is out of range.
     *      E_OUTOFMEMORY if out of memory.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE EnumAudioEndpoints(EDataFlow dataFlow,
        DWORD dwStateMask,
        IMMDeviceCollection ** ppDevices);

    /**
     *  @function GetDefaultAudioEndpoint
     *      The GetDefaultAudioEndpoint method retrieves the default audio
     *      endpoint for the specified data-flow direction and role.
     */
    HRESULT STDMETHODCALLTYPE GetDefaultAudioEndpoint(EDataFlow dataFlow,
        ERole role,
        IMMDevice ** ppEndpoint);

    /**
     *  @function GetDevice
     *      The GetDevice method retrieves an audio endpoint device that is
     *      identified by an endpoint ID string.
     */
    HRESULT STDMETHODCALLTYPE GetDevice(LPCWSTR pwstrId, IMMDevice ** ppDevice);

    /**
     *  @function RegisterEndpointNotificationCallback
     *      The RegisterEndpointNotificationCallback method registers a client's
     *      notification callback interface.
     */
    HRESULT STDMETHODCALLTYPE RegisterEndpointNotificationCallback(
        IMMNotificationClient * pClient);

    /**
     *  @function UnregisterEndpointNotificationCallback
     *      The UnregisterEndpointNotificationCallback method deletes the
     *      registration of a notification interface that the client registered
     *      in a previous call to the
     *      IMMDeviceEnumerator::RegisterEndpointNotificationCallback method.
     */
    HRESULT STDMETHODCALLTYPE UnregisterEndpointNotificationCallback(
        IMMNotificationClient * pClient);

    /*
     * IUnknown stuff
     */
    /**
     *  @function AddRef
     *      Increments the reference count for an interface on an object. This
     *      method should be called for every new copy of a pointer to an
     *      interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE AddRef();

    /**
     *  @function Release
     *      Decrements the reference count for an interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE Release();

    /**
     *  @function QueryInterface
     *      Retrieves pointers to the supported interfaces on an object.
     *
     *  @param {REFIID} riid
     *      The TODO
     *
     *  @returns {HRESULT}
     *      - S_OK if the interface is supported;
     *      - E_POINTER if ppvObject is NULL;
     *      - E_NOINTERFACE otherwise.
     */
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject);

private:
    /*
     *  IMMDeviceEnumeratorEmulator stuff
     */

    /**
     *  @function IMMDeviceEnumeratorEmulator
     *      Constructor. Private for reasons.
     *
     *  @param {IMMDeviceEnumerator *} original
     *      Pointer to the actual object we are proxying.
     */
    IMMDeviceEnumeratorEmulator(IMMDeviceEnumerator * original);

    /**
     *  @property {IMMDeviceEnumerator} original
     */
    IMMDeviceEnumerator * original;

    /*
     *  IUnknown-related stuff
     */
    /**
     *  @property {ULONG} referenceCount
     *      Used to implement .AddRef() et .Release() reference counting
     *      mechanism
     */
    ULONG referenceCount;
};

#endif // IMMDEVICEENUMERATOREMULATOR_HPP
