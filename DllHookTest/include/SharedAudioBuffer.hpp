/*
 *  File:   SharedAudioBuffer.hpp
 *  Author: Anenzoras
 *  Date:   2019-02-19
 *
 *  Brief:  Contains a class definition for the shared buffer which will be used
 *      on windows to share the audio signal interprocess.
 */

#ifndef SHAREDAUDIOBUFFER_HPP
#define SHAREDAUDIOBUFFER_HPP

/* Standard library includes */
#include <cmath> /* floor(), sinf() */
#include <cstdint> /* types */

/**
 *  @function getCurrentTime
 *      Returns the current system time in form of seconds.
 *
 *  @returns {float}
 *      The time in seconds.
 */
float getCurrentTime(void);

/**
 *  @class SharedAudioBuffer
 *      Only one object of this class will be placed in shared memory and be
 *      used accross all concerned processes.
 *      The class contains a buffer array and mechanisms for synchronicity.
 */
class SharedAudioBuffer
{
public:
    enum Status
    {
        SUCCESS,
        FAILURE,
        F_POINTER,

        Status_COUNT
    };

    /**
     *  @property {bool} connected
     *
     */
    bool connected = TRUE;

    /* See readBuffer implementation */
    float const initialTime = getCurrentTime(); // seconds
    float const refreshRate = 1.0f/20.0f; // seconds

    /*
     For current test, let's define samples as 16bit pcm 44100hz, on 2-channels.
     */
    uint32_t sampleSize = 16; // bits
    uint32_t sampleRate = 44100; // hertz
    uint32_t channelCount = 2;
    uint32_t frameSize = (channelCount * (sampleSize / 8)); // bytes
    uint32_t bytesPerSecond = (sampleRate * frameSize);

    SharedAudioBuffer(void)
    {
    }

    /**
     *  @function getBufferSize
     *      Returns the buffer size for a call to readBuffer().
     *
     *  __TEMP
     *
     *  @returns {uint32_t}
     *      The size of the buffer in bytes.
     */
    uint32_t getBufferSize(void)
    {
        return (uint32_t)(this->refreshRate * this->bytesPerSecond);
    }

    float getOffset(void)
    {
        float uptime = getCurrentTime() - initialTime;

        return floor(uptime / this->refreshRate) * this->refreshRate;
    }

    /**
     *  @function readBuffer
     *      Reads the audio buffer from shared memory and place the contents
     *      into the destination parameter.
     *
     *  @warning
     *      This function might lock the calling thread for synchronization
     *      purposes. This should be fast enough to not be a hinder, but you are
     *      warned.
     *
     *  @param {char *} destination
     *      A non-NULL array in which the samples will be put.
     *  @param {float} masterVolume
     *      A modification on the volume to apply to all samples, regardless of
     *      channel. Is considered in [0.0; 1.0] range.
     *  @param {float[]} volumes
     *      Array of volumes per channel.
     *      Are considered in [0.0; 1.0] range.
     *      If NULL, no modification are applied.
     *
     *  @returns {SharedAudioBuffer::Status}
     *      Possible code values are:
     *      - SUCCESS when the buffer could be read;
     *      - F_POINTER if the destination parameter is NULL;
     *      - FAILURE if an unknown problem occured.
     */
    Status readBuffer(char * destination, float masterVolume, float volumes[])
    {
        if (destination == NULL)
        {
            return F_POINTER;
        }

        /*
        Note: for now and test purposes, the read function generates
            a sin wave.
        */
        static const float PI = 3.1415926535f;
        static const float PITCH = 440.0f;
        static const float RADIANS_PER_SECOND = PITCH * 2.0f * PI;
        static const uint32_t FRAME_COUNT = (this->sampleRate * this->refreshRate);
        static const float SECONDS_PER_FRAME = 1.0f / this->sampleRate;

        /* Intention: writing samples depending on time since the first call was
         *  made
         * Let say the refresh time is 1/20th of a second (meaning we simulate a
         *  write of 1/20th of the 44100 frames per second)
         */
        float offset = getOffset();

        for (uint32_t frameI = 0; frameI < FRAME_COUNT; frameI++)
        {
            float sample = sinf((offset + frameI * SECONDS_PER_FRAME)
                * RADIANS_PER_SECOND);

            for (uint32_t channel = 0; channel < this->channelCount; channel++)
            {
                uint32_t destinationOffset;
                int16_t * pSample;

                destinationOffset = (frameI * this->channelCount) + channel;
                destinationOffset *= this->sampleSize;
                pSample = (int16_t *)(destination + destinationOffset);

                /* Resample before */
                *pSample = static_cast<int16_t>(sample * INT16_MAX);

                /* Apply volume */
                if (volumes != NULL)
                {
                    *pSample *= volumes[channel];
                }
                *pSample *= masterVolume;
            }
        }

        return SUCCESS;
    }



private:
    // mutex_t incrementMutex;
    // mutex_t decrementMutex;
    uint32_t readerCount; // is not locked by itself, but the last reader should
                          // signal he is the last one (this means increment and
                          // decrement must be atomic!) to the writer

    char * buffer;
    /*
    Design de la synchro en pseudocode:
        process_t X;
        if (Voceliand.wantWrite)
        {
            if (readerCount > 0) // might be atomic
            {
                lockVariableIncrementation(readerCount);
                waitFor(readerCount == 0);
            }

            // no reader here then

            writeThings();

            unlock(readerCount);

            // Ok, problem potentiel ici si un reader ne veut pas lâcher la lecture
            // ce qui ne devrait pas être le cas en audio, mais c'est pas dit.
            // WARNING WARNING WARNING: faire en sorte que la lecture se fasse AFAP
        }
        if (X.wantRead())
        {
            waitAndIncrement(readerCount);

            readThings(); // cf commentaire au dessus, DOIT terminer

            decrement(readerCount); // send signal if readerCount == 0 ?
        }
    */
};

// TODO not like this
extern SharedAudioBuffer sharedBuffer;

#endif // SHAREDAUDIOBUFFER_HPP
