/*
 *  File:   IMMDeviceCollectionEmulator
 *  Author: Anenzoras
 *  Date:   2019-02-13
 *
 *  Brief:  Definition of the class proxying an IMMDeviceCollection interface,
 *      retrieved by a call to IMMDeviceEnumeratorEmulator::EnumAudioEndpoints
 */

#ifndef IMMDEVICECOLLECTIONEMULATOR_HPP
#define IMMDEVICECOLLECTIONEMULATOR_HPP

/* External library includes */
#include <mmdeviceapi.h> /* Core Audio for IMMDeviceCollection */

/**
 *  @property {IID const} IID_IMMDeviceCollection
 *      The unique identifier for the IMMDeviceCollection interface.
 */
extern IID const IID_IMMDeviceCollection;

/**
 *  @class IMMDeviceCollectionEmulator
 *      Class that will replace the IMMDeviceCollection object returned by
 *      IMMDeviceEnumeratorEmulator::EnumAudioEndpoints() and act as a proxy.
 *
 *  @note
 *      The class keeps track of the original object, but increment its
 *      reference counter when instancied. This means the .CreateInstance()
 *      caller can call Release() on the parameter without fear afterward.
 *
 *  @note
 *      Documentation of interface functions comes from Microsoft's official
 *      documentation.
 */
class IMMDeviceCollectionEmulator
    : public IMMDeviceCollection
{
public:
    /*
     * IMMDeviceCollectionEmulator stuff
     */
    /**
     *  @function [static] CreateInstance
     *      Returns an instance of IMMDeviceCollectionEmulator to encapsulate
     *      thingies.
     *
     *  @param {IMMDeviceCollection *} original
     *      The original IMMDeviceCollection object pointer, so we keep track of
     *      things.
     *
     *  @returns {IMMDeviceCollectionEmulator *}
     *      A pointer to our newly created proxy.
     */
    static IMMDeviceCollectionEmulator * CreateInstance(
        IMMDeviceCollection * original);

    /**
     *  @function ~IMMDeviceCollectionEmulator
     *      Destructor. Releases its reference to the original object.
     */
    virtual ~IMMDeviceCollectionEmulator();

    /*
     * IMMDeviceCollection stuff
     */
    /**
     *  @function GetCount
     *      The GetCount method retrieves a count of the devices in the device
     *      collection.
     *
     *  @param {UINT *} pcDevices
     *      Pointer to a UINT variable into which the method writes the number
     *      of devices in the device collection.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pcDevices is NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetCount(UINT * pcDevices);

    /**
     *  @function Item
     *      The Item method retrieves a pointer to the specified item in the
     *      device collection.
     *
     *  @param {UINT} nDevice
     *      The device number. If the collection contains n devices, the devices
     *      are numbered 0 to n-1.
     *
     *  @param {IMMDevice **} ppDevice
     *      Pointer to a pointer variable into which the method writes the
     *      address of the IMMDevice interface of the specified item in the
     *      device collection. Through this method, the caller obtains a counted
     *      reference to the interface. The caller is responsible for releasing
     *      the interface, when it is no longer needed, by calling the
     *      interface's Release method. If the Item call fails, *ppDevice is
     *      NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if parameter ppDevice is NULL.
     *      E_INVALIDARG if parameter nDevice is not a valid device number.
     *      E_OUTOFMEMORY if the system is out of memory (NOT OFFICIALY
     *          DOCUMENTED YOU MICROSOFT ASSES).
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE Item(UINT nDevice, IMMDevice ** ppDevice);

    /*
     * IUnknown stuff
     */
    /**
     *  @function AddRef
     *      Increments the reference count for an interface on an object. This
     *      method should be called for every new copy of a pointer to an
     *      interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE AddRef();

    /**
     *  @function Release
     *      Decrements the reference count for an interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE Release();

    /**
     *  @function QueryInterface
     *      Retrieves pointers to the supported interfaces on an object.
     *
     *  @param {REFIID} riid
     *      The TODO
     *
     *  @returns {HRESULT}
     *      - S_OK if the interface is supported;
     *      - E_POINTER if ppvObject is NULL;
     *      - E_NOINTERFACE otherwise.
     */
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject);
private:
    /*
     *  IMMDeviceCollectionEmulator members
     */

    /**
     *  @function IMMDeviceCollectionEmulator
     *      Constructor. Private for reasons.
     *
     *  @param {IMMDeviceCollection *} original
     *      Pointer to the actual object we are proxying.
     */
    IMMDeviceCollectionEmulator(IMMDeviceCollection * original);

    /**
     *  @property {IMMDeviceCollection} original
     */
    IMMDeviceCollection * original;

    /*
     *  IUnknown-related stuff
     */
    /**
     *  @property {ULONG} referenceCount
     *      Used to implement .AddRef() et .Release() reference counting
     *      mechanism
     */
    ULONG referenceCount;
};

#endif // IMMDEVICECOLLECTIONEMULATOR_HPP
