/*
 *  File:   IMMDeviceEmulator
 *  Author: Anenzoras
 *  Date:   2019-02-14
 *
 *  Brief:  Definition of the fake class that will emulate an IMMDevice
 *  object after the call to the IMMDeviceCollectionEmulator::Item() or the
 *  or the IMMDeviceEnumeratorEmulator::GetDevice() functions.
 */

#ifndef IMMDEVICEEMULATOR_HPP
#define IMMDEVICEEMULATOR_HPP

/* External library includes */
#include <Endpointvolume.h> /* IAudioEndpointVolume,
                             * AUDIO_VOLUME_NOTIFICATION_DATA */
#include <mmdeviceapi.h> /* Core Audio for IMMDevice */

/**
 *  @property {IID const} IID_IMMDevice
 *      The unique identifier for the IMMDevice interface.
 */
extern IID const IID_IMMDevice;

/**
 *  @property {wchar_t litteral} LDEVICE_ID
 *      This is the ID that should be returned by IMMDeviceEmulator::GetId() and
 *      used by IMMDeviceCollection::GetDevice() functions.
 *
 *      Under my win7, the devices id are on this form (number probably vary):
 *      "{0.0.1.00000000}.{edc95957-f747-4fca-8edd-9b09a0ca7459}". But microsoft
 *      tells that this string should be considered as opaque, as implementation
 *      may vary, and since our program is proxying anyway, we are using a
 *      custom string.
 *      -Anenzoras, 2019-02-14
 */
#define IMMDeviceEmulator_LDEVICE_ID (L"VoceliandDevice")

/**
 *  @structure SessionInfo_s
 *      Structure to describe the information related to a session, notably
 *      volume.
 *
 *      Concerning volume information, the db volume range seems to be
 *      completely arbitrary. As per reading windows documentation, it does not
 *      seem to have anything to do with dBFS. On top of it, one implementation
 *      of the IAudioEndpointVolume interface (that uses this structure) in an
 *      largely used program choses arbitrary values for range:
 *      https://www.winehq.org/pipermail/wine-patches/2016-March/148040.html
 *      (well at least in this version). But this also corrolates with, for
 *      instance, OBS. Yes. In OBS, which captures sound emitted by the system
 *      and by microphones, you can modify the volume using a slider for each.
 *      Said slider prints value in dB and although max value is 0.0 dB (making
 *      it kind of dBFS relatable), min value is -120.0 dB (to be honest, it's
 *      -inf for silence).
 *      I'm hesitant on what to do, but I suppose I'm going for an arbitrary
 *      range of [-120.0; 0.0] dB corresponding to scalar range [0.0; 1.0].
 *      Except obviously that since dB is on a logarithmic scale, there's a
 *      little adaptations to make.
 *      -Anenzoras, 2019-03-16
 */
struct SessionInfo_s
{
    static UINT const CHANNEL_COUNT = 2;

    /**
     *  @property {UINT} channelCount
     *      The number of channel of our endpoint device.
     *      Actually different that the negotiated value between client and us.
     *      TODO initialize this differently when we make modify (maybe) the
     *      endpoint.
     */
    UINT const channelCount = SessionInfo_s::CHANNEL_COUNT;
    /**
     *  @property {UINT const} volumeStepCount
     *      A counter for the "step" in dB we can make on a VolumeStepUp() or
     *      VolumeStepDown() call (see IAudioEndpointVolume interface).
     *      Arbitrary choice of 16 steps, so that it's significant without being
     *      too big.
     */
    UINT const volumeStepCount = 16;
    float const maxDB = 0.0f;
    float const minDB = -120.0f;
    /**
     *  @property {float const} dBFactor
     *      A factor to use when converting from "dB" to scalar.
     *
     *      The general float to dBFS formula is
     *      valueDBFS = 20 * log10(abs(valueScalar))
     *      where value is in [-1; 1] and is a sample value.
     *      Here, baseValue is in [0; 1] so I guess we don't need the abs().
     *      But to really go along our chosen dB min and max, we need a factor
     *      that's in our range to replace the 20 factor. We chose the average
     *      of our range, minus one to be sure that 120.0 dB is the minimum
     *      possible.
     *      -Anenzoras, 2019-03-15
     */
    float const dBFactor;
    float const volumeIncrementDB;

    bool muted = FALSE;
    float masterVolume = 1.0f;
    /**
     *  @property {float[]} volumes
     *      Static array to give the volume per channel.
     */
    float volumes[SessionInfo_s::CHANNEL_COUNT] = {1.0f, 1.0f};

    /**
     *  @property {IAudioEndpointVolumeCallback *} changeListeners
     *      Holds instances to implementations of the callback interface that
     *      get called when the volume is changed.
     *      Basic array because... marshaling? (Attemps to see if it is the
     *      problem)
     */
    IAudioEndpointVolumeCallback ** changeListeners = NULL;
    UINT listenersCapacity = 0;
    UINT listenersCount = 0;

    SessionInfo_s(void)
        : dBFactor(abs(this->maxDB + this->maxDB) - 1.0f),
        volumeIncrementDB(abs((this->maxDB - this->minDB)
            / (float)this->volumeStepCount))
    {
    }

    /**
     *  @function addListener
     *      Adds the given listener to the array of listener.
     *      Implements auto-resize.
     *
     *  @param {IAudioEndpointVolumeCallback *} listener
     *      The listener to add to our array.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the paramter listener is NULL.
     *      E_OUTOFMEMORY if the array attempted rellocation and could not.
     *          In that case the listener is not added to the array, but the
     *          array still exists.
     */
    HRESULT addListener(IAudioEndpointVolumeCallback * listener)
    {
        if (listener == NULL)
        {
            return E_POINTER;
        }

        /* Test resize */
        if (this->listenersCount + 1 > this->listenersCapacity)
        {
            UINT newCapacity;
            IAudioEndpointVolumeCallback ** newPtr = NULL;

            newCapacity = (this->listenersCapacity == 0)
                ? 16
                : this->listenersCapacity * 2;

            newPtr = (IAudioEndpointVolumeCallback **) realloc(this->changeListeners,
                sizeof(IAudioEndpointVolumeCallback *) * newCapacity);

            if (newPtr == NULL)
            {
                return E_OUTOFMEMORY;
            }

            this->changeListeners = newPtr;
            this->listenersCapacity = newCapacity;
        }

        this->changeListeners[this->listenersCount] = listener;
        this->listenersCount++;

        return S_OK;
    }

    /**
     *  @function removeListener
     *      Removes the given listener from the array of listener.
     *      TODO Implement auto-resize.
     *
     *  @param {IAudioEndpointVolumeCallback *} listener
     *      The listener to remove from our array.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the paramter listener is NULL.
     *      E_INVALIDARG if the paramter listener is not inside the array.
     */
    HRESULT removeListener(IAudioEndpointVolumeCallback * listener)
    {
        if (listener == NULL)
        {
            return E_POINTER;
        }

        size_t i;

        /* Search */
        for (i = 0; i < this->listenersCount; i++)
        {
            if (this->changeListeners[i] == listener)
            {
                this->changeListeners[i] = NULL;
            }
        }

        /* Safety */
        if (i == this->listenersCount)
        {
            return E_INVALIDARG;
        }

        /* Shift */
        for (; i < this->listenersCount - 1; i++)
        {
            this->changeListeners[i] = this->changeListeners[i + 1];
        }

        this->listenersCount--;

        return S_OK;
    }

    /**
     *  @function notifyChangeListeners
     *      Helper to notify all listeners in changeListeners that a volume
     *      changed.
     */
    HRESULT notifyChangeListeners(GUID const * guid)
    {
        HRESULT status;
        /*  Because of the structure specification, we have to dynamically allocate
         *  this so we can add additionnal channel info to the memory.
         *  Somebody said something about weird unsafe shit?
         *  -Anenzoras, 2019-03-17 */
        void * memzone;
        AUDIO_VOLUME_NOTIFICATION_DATA * data;
        float * part2;
        memzone = CoTaskMemAlloc(sizeof(AUDIO_VOLUME_NOTIFICATION_DATA)
            + (sizeof(float) * (this->channelCount - 1)));
        if (memzone == NULL)
        {
            return E_OUTOFMEMORY;
        }

        /* We do this because we have no idea if the system doesn't do something
         * stupid with structure padding and property position or not.
         * data->afChannelVolumes might not be at the end of the struct. */
        data = (AUDIO_VOLUME_NOTIFICATION_DATA *) memzone;
        part2 = (float *)((char *) memzone
            + sizeof(AUDIO_VOLUME_NOTIFICATION_DATA));

        if (guid == NULL)
        {
            data->guidEventContext = GUID_NULL;
        }
        else
        {
            data->guidEventContext = (*guid);
        }
        data->bMuted = this->muted;
        data->fMasterVolume = this->masterVolume;
        data->nChannels = this->channelCount;
        data->afChannelVolumes[0] = this->volumes[0];
        for (size_t i = 1; i < this->channelCount; i++)
        {
            part2[i - 1] = this->volumes[i];
        }

        for (size_t i = 0; i < this->listenersCount; i++)
        {
            status = this->changeListeners[i]->OnNotify(data);
            if (status != S_OK)
            {
                break;
            }
        }

        CoTaskMemFree(memzone);

        if (status != S_OK)
        {
            return status;
        }

        return S_OK;
    }
};

/**
 *  @class IMMDeviceEmulator
 *      Class that will replace the IMMDevice object returned by
 *      IMMDeviceCollectionEmulator::Item() or by
 *      IMMDeviceEnumeratorEmulator::GetDevice() with parameter that correspond
 *      to our fake device.
 *      Also implements the IMMEndpoint interface because "After obtaining the
 *      IMMDevice interface of an audio endpoint device, a client can obtain an
 *      interface that encapsulates the endpoint-specific features of the device
 *      by calling the IMMDevice::QueryInterface method with parameter iid set
 *      to REFIID IID_IMMEndpoint. For more information, see IMMEndpoint
 *      Interface." (MS doc).
 *
 *  @note
 *      This emulator class does not keep track of any original object since we
 *      are faking it entirely.
 *
 *  @note
 *      Documentation of interface functions comes from Microsoft's official
 *      documentation.
 */
class IMMDeviceEmulator
    : public IMMDevice, public IMMEndpoint
{
public:
    /*
     * IMMDeviceEmulator stuff
     */
    /**
     *  @function [static] CreateInstance
     *      Returns an instance of IMMDeviceEmulator to encapsulate thingies.
     *
     *  @returns {IMMDeviceEmulator *}
     *      A pointer to our newly created emulator.
     */
    static IMMDeviceEmulator * CreateInstance(void);

    /**
     *  @property {SessionInfo_s} sessionInfo
     *      A stucture holding all data relative to a session (such as volume
     *      and stuff like that).
     *      Is accessed by the IAudioEndpointVolumeEmulator class and should
     *      be accessed by other windows engine interface emulators in the
     *      future. TODO (2019-03-15)
     */
    SessionInfo_s sessionInfo;

    /**
     *  @function ~IMMDeviceEmulator
     *      Destructor.
     */
    virtual ~IMMDeviceEmulator();

    /*
     * IUnknown stuff
     */
    /**
     *  @function AddRef
     *      Increments the reference count for an interface on an object. This
     *      method should be called for every new copy of a pointer to an
     *      interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE AddRef();

    /**
     *  @function Release
     *      Decrements the reference count for an interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE Release();

    /**
     *  @function QueryInterface
     *      Retrieves pointers to the supported interfaces on an object.
     *
     *  @param {REFIID} riid
     *      The TODO
     *
     *  @returns {HRESULT}
     *      - S_OK if the interface is supported;
     *      - E_POINTER if ppvObject is NULL;
     *      - E_NOINTERFACE otherwise.
     */
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject);

    /*
     * IMMDevice stuff
     */
    /**
     *  @function Activate
     *      The Activate method creates a COM object with the specified
     *      interface.
     *
     *  @param {REFIID} iid
     *      The interface identifier. This parameter is a reference to a GUID
     *      that identifies the interface that the caller requests be activated.
     *      The caller will use this interface to communicate with the COM
     *      object. Set this parameter to one of the following interface
     *      identifiers:
     *      - IID_IAudioClient
     *      - IID_IAudioEndpointVolume
     *      - IID_IAudioMeterInformation
     *      - IID_IAudioSessionManager
     *      - IID_IAudioSessionManager2
     *      - IID_IBaseFilter
     *      - IID_IDeviceTopology
     *      - IID_IDirectSound
     *      - IID_IDirectSound8
     *      - IID_IDirectSoundCapture
     *      - IID_IDirectSoundCapture8
     *      - IID_IMFTrustedOutput
     *      - IID_ISpatialAudioClient
     *      - IID_ISpatialAudioMetadataClient
     *  @param {DWORD} dwClsCtx
     *      The execution context in which the code that manages the newly
     *      created object will run. The caller can restrict the context by
     *      setting this parameter to the bitwise OR of one or more CLSCTX
     *      enumeration values. Alternatively, the client can avoid imposing any
     *      context restrictions by specifying CLSCTX_ALL. For more information
     *      about CLSCTX, see the Windows SDK documentation.
     *  @param {PROPVARIANT *} pActivationParams
     *      Set to NULL to activate an IAudioClient, IAudioEndpointVolume,
     *      IAudioMeterInformation, IAudioSessionManager, or IDeviceTopology
     *      interface on an audio endpoint device. When activating an
     *      IBaseFilter, IDirectSound, IDirectSound8, IDirectSoundCapture, or
     *      IDirectSoundCapture8 interface on the device, the caller can specify
     *      a pointer to a PROPVARIANT structure that contains
     *      stream-initialization information. For more information, see
     *      Remarks.
     *  @param {void **} ppInterface
     *      Pointer to a pointer variable into which the method writes the
     *      address of the interface specified by parameter iid. Through this
     *      method, the caller obtains a counted reference to the interface. The
     *      caller is responsible for releasing the interface, when it is no
     *      longer needed, by calling the interface's Release method. If the
     *      Activate call fails, *ppInterface is NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_NOINTERFACE if the object does not support the requested interface
     *          type.
     *      E_POINTER if parameter ppInterface is NULL.
     *      E_INVALIDARG if the pActivationParams parameter must be NULL for the
     *          specified interface and is not; or pActivationParams points to
     *          invalid data.
     *      E_OUTOFMEMORY if the system is out of memory.
     *      AUDCLNT_E_DEVICE_INVALIDATED the user has removed either the audio
     *          endpoint device or the adapter device that the endpoint device
     *          connects to.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE Activate(REFIID iid, DWORD dwClsCtx,
        PROPVARIANT * pActivationParams, void ** ppInterface);

    /**
     *  @function GetId
     *      The GetId method retrieves an endpoint ID string that identifies the
     *      audio endpoint device.
     *
     *  @param {LPWSTR *} ppstrId
     *      Pointer to a pointer variable into which the method writes the
     *      address of a null-terminated, wide-character string containing the
     *      endpoint device ID. The method allocates the storage for the string.
     *      The caller is responsible for freeing the storage, when it is no
     *      longer needed, by calling the CoTaskMemFree function. If the GetId
     *      call fails, *ppstrId is NULL. For information about CoTaskMemFree,
     *      see the Windows SDK documentation.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_OUTOFMEMORY if the system is out of memory.
     *      E_POINTER if the parameter pwstrId is NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetId(LPWSTR * ppstrId);

    /**
     *  @function GetState
     *      The GetState method retrieves the current device state.
     *
     *  @param {DWORD *} pdwState
     *      Pointer to a DWORD variable into which the method writes the current
     *      state of the device. The device-state value is one of the following
     *      DEVICE_STATE_XXX constants:
     *      - DEVICE_STATE_ACTIVE
     *      - DEVICE_STATE_DISABLED
     *      - DEVICE_STATE_NOTPRESENT
     *      - DEVICE_STATE_UNPLUGGED
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pdwState is NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetState(DWORD * pdwState);

    /**
     *  @function OpenPropertyStore
     *      The OpenPropertyStore method retrieves an interface to the device's
     *      property store.
     *
     *  @param {DWORD} stgmAccess
     *      The storage-access mode. This parameter specifies whether to open
     *      the property store in read mode, write mode, or read/write mode. Set
     *      this parameter to one of the following STGM constants:
     *      - STGM_READ
     *      - STGM_WRITE
     *      - STGM_READWRITE
     *      The method permits a client running as an administrator to open a
     *      store for read-only, write-only, or read/write access. A client that
     *      is not running as an administrator is restricted to read-only access.
     *      For more information about STGM constants, see the Windows SDK
     *      documentation.
     *  @param {IPropertyStore **} ppProperties
     *      Pointer to a pointer variable into which the method writes the
     *      address of the IPropertyStore interface of the device's property
     *      store. Through this method, the caller obtains a counted reference
     *      to the interface. The caller is responsible for releasing the
     *      interface, when it is no longer needed, by calling the interface's
     *      Release method. If the OpenPropertyStore call fails, * ppProperties
     *      is NULL. For more information about IPropertyStore, see the Windows
     *      SDK documentation.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter stgmAccess is not a valid access mode.
     *      E_POINTER if the parameter ppProperties is NULL.
     *      E_OUTOFMEMORY if the system is out of memory.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE OpenPropertyStore(DWORD stgmAccess,
        IPropertyStore ** ppProperties);

    /*
     * IMMEndpoint
     */
    /**
     *  @function GetDataFlow
     *      The GetDataFlow method indicates whether the audio endpoint device
     *      is a rendering device or a capture device.
     *
     *  @param {EDataFlow * pDataFlow}
     *      Pointer to a variable into which the method writes the data-flow
     *      direction of the endpoint device. The direction is indicated by one
     *      of the following EDataFlow enumeration constants:
     *      - eRender
     *      - eCapture
     *      The data-flow direction for a rendering device is eRender. The
     *      data-flow direction for a capture device is eCapture.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter ppDataFlow is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetDataFlow(EDataFlow * pDataFlow);
private:
    /**
     *  @function IMMDeviceEmulator
     *      Constructor. Private for reasons.
     */
    IMMDeviceEmulator();

    /*
     *  IUnknown-related stuff
     */
    /**
     *  @property {ULONG} referenceCount
     *      Used to implement .AddRef() et .Release() reference counting
     *      mechanism
     */
    ULONG referenceCount;
};

#endif // IMMDEVICEEMULATOR_HPP
