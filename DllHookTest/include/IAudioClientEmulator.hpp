/*
 *  File:   IAudioClientEmulator
 *  Author: Anenzoras
 *  Date:   2019-02-14
 *
 *  Brief:  Definition of the fake class that will emulate an IAudioClient
 *  object after the call to the IMMDevice::Activate().
 */

#ifndef IMMAUDIOCLIENTEMULATOR_HPP
#define IMMAUDIOCLIENTEMULATOR_HPP

/* External library includes */
#include <Audioclient.h> /* Core Audio for IAudioClient */

/* Project library includes */
#include "IMMDeviceEmulator.hpp"

/**
 *  @property {IID const} IID_IAudioClient
 *      The unique identifier for the IAudioClient interface.
 */
extern IID const IID_IAudioClient;

/**
 *  @function newDefaultFormat
 *      Function that generates a WAVEFORMATEX object describing the default
 *      format for our device.
 *
 *      If not NULL, the returned pointer must be freed by a call to
 *      CoTaskMemFree().
 *
 *  @returns {WAVEFORMATEX *}
 *      A pointer to a newly created WAVEFORMATEX structure describing the
 *      default audio format if the function is successful.
 *      NULL if the structure couldn't be allocated.
 */
WAVEFORMATEX * newDefaultFormat(void);

/**
 *  @class IAudioClientEmulator
 *      Class that will replace the IAudioClient object returned by
 *      IMMDevice::Activate() with the IID_IAudioClient parameter.
 *
 *  @implements IUnkown
 *  @implements IAudioClient
 *  @implements IAudioCaptureClient
 *
 *  @note
 *      This emulator class does not keep track of any original object since we
 *      are faking it entirely.
 *
 *  @note
 *      Documentation of interface functions comes from Microsoft's official
 *      documentation.
 *
 *  @note
 *      We need to use two interfaces to read data from the buffer: IAudioClient
 *      and IAudioCaptureClient. The fun thing is, both interfaces must
 *      communicate between each others (IAudioClient::Reset() needs to know if
 *      IAudioCaptureClient has a buffer being read and
 *      IAudioCaptureClient::GetBuffer() must know if IAudioClient is in the
 *      middle of a reset). Both interfaces, having a mechanism of counted
 *      references thanks to IUnkown, cannot then reference themselves. This
 *      means one class must implement both interfaces. This is corroborated
 *      by the following sentence retrieved from the IAudioClient::GetService()
 *      function's documentation: "To release the IAudioClient object and free
 *      all its associated resources, the client must release all references to
 *      any service objects that were created by calling GetService, in addition
 *      to calling Release on the IAudioClient interface itself. The client must
 *      release a service from the same thread that releases the IAudioClient
 *      object.".
 *      This means our emulator will implement both interfaces. This have one
 *      side effect: QueryInterface() can return both interfaces.
 */
class IAudioClientEmulator
    : public IAudioClient, public IAudioCaptureClient
{
public:
    /*
     * IAudioClientEmulator stuff
     */
    /**
     *  @function [static] CreateInstance
     *      Returns an instance of IAudioClientEmulator to encapsulate thingies.
     *
     *  @param {IMMDeviceEmulator *} device
     *      The device we are linked to. Necessary to access session info.
     *      IMPORTANT: The functions does not call AddRef() on it.
     *
     *  @returns {IAudioClientEmulator *}
     *      A pointer to our newly created emulator.
     */
    static IAudioClientEmulator * CreateInstance(IMMDeviceEmulator * device);
    /**
     *  @function ~IAudioClientEmulator
     *      Destructor.
     */
    virtual ~IAudioClientEmulator();

    /*
     * IAudioClientEmulator stuff
     */
    /**
     *  @function GetBufferSize
     *      The GetBufferSize method retrieves the size (maximum capacity) of
     *      the endpoint buffer.
     *  @param {UINT32 *} pNumBufferFrames
     *      Pointer to a UINT32 variable into which the method writes the number
     *      of audio frames that the buffer can hold.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_NOT_INITIALIZED if the audio stream has not been
     *          successfully initialized before calling this function.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameter pNumBufferFrames is NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetBufferSize(UINT32 * pNumBufferFrames);

    /**
     *  @function GetCurrentPadding
     *      The GetCurrentPadding method retrieves the number of frames of
     *      padding in the endpoint buffer.
     *
     *  @param {UINT32 *} pNumBufferFrames
     *      Pointer to a UINT32 variable into which the method writes the frame
     *      count (the number of audio frames of padding in the buffer).
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_NOT_INITIALIZED if the audio stream has not been
     *          successfully initialized before calling this function.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameter pNumPaddingFrames is NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetCurrentPadding(UINT32 * pNumPaddingFrames);

    /**
     *  @function GetDevicePeriod
     *      The GetDevicePeriod method retrieves the length of the periodic
     *      interval separating successive processing passes by the audio engine
     *      on the data in the endpoint buffer.
     *
     *  @param {REFERENCE_TIME *} phnsDefaultDevicePeriod
     *      Pointer to a REFERENCE_TIME variable into which the method writes a
     *      time value specifying the default interval between periodic
     *      processing passes by the audio engine. The time is expressed in
     *      100-nanosecond units. For information about REFERENCE_TIME, see the
     *      Windows SDK documentation.
     *  @param {REFERENCE_TIME *} phnsMinimumDevicePeriod
     *      Pointer to a REFERENCE_TIME variable into which the method writes a
     *      time value specifying the minimum interval between periodic
     *      processing passes by the audio endpoint device. The time is
     *      expressed in 100-nanosecond units.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameters phnsDefaultDevicePeriod and
     *          phnsMinimumDevicePeriod are both NULL.
     */
    HRESULT STDMETHODCALLTYPE GetDevicePeriod(
        REFERENCE_TIME * phnsDefaultDevicePeriod,
        REFERENCE_TIME * phnsMinimumDevicePeriod);

    /**
     *  @function GetMixFormat
     *      The GetMixFormat method retrieves the stream format that the audio
     *      engine uses for its internal processing of shared-mode streams.
     *
     *  @param {WAVEFORMATEX **} ppDeviceFormat
     *      Pointer to a pointer variable into which the method writes the
     *      address of the mix format. This parameter must be a valid, non-NULL
     *      pointer to a pointer variable. The method writes the address of a
     *      WAVEFORMATEX (or WAVEFORMATEXTENSIBLE) structure to this variable.
     *      The method allocates the storage for the structure. The caller is
     *      responsible for freeing the storage, when it is no longer needed, by
     *      calling the CoTaskMemFree function. If the GetMixFormat call fails,
     *      *ppDeviceFormat is NULL. For information about WAVEFORMATEX,
     *      WAVEFORMATEXTENSIBLE, and CoTaskMemFree, see the Windows SDK
     *      documentation.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameter ppDeviceFormat is NULL.
     *      E_OUTOFMEMORY if the system is out of memory.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetMixFormat(WAVEFORMATEX ** ppDeviceFormat);

    /**
     *  @function GetService
     *      The GetService method accesses additional services from the audio
     *      client object.
     *
     *  @param {REFIID} riid
     *      The interface ID for the requested service. The client should set
     *      this parameter to one of the following REFIID values:
     *      - IID_IAudioCaptureClient
     *      - IID_IAudioClock
     *      - IID_IAudioRenderClient
     *      - IID_IAudioSessionControl
     *      - IID_IAudioStreamVolume
     *      - IID_IChannelAudioVolume
     *      - IID_IMFTrustedOutput
     *      - IID_ISimpleAudioVolume
     *  @param {void **} ppv
     *      Pointer to a pointer variable into which the method writes the
     *      address of an instance of the requested interface. Through this
     *      method, the caller obtains a counted reference to the interface. The
     *      caller is responsible for releasing the interface, when it is no
     *      longer needed, by calling the interface's Release method. If the
     *      GetService call fails, *ppv is NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter ppv is NULL.
     *      E_NOINTERFACE if the requested interface is not available.
     *      AUDCLNT_E_NOT_INITIALIZED  if the audio stream has not been
     *          successfully initialized before calling this function.
     *      AUDCLNT_E_WRONG_ENDPOINT_TYPE if the caller tried to access an
     *          IAudioCaptureClient interface on a rendering endpoint, or an
     *          IAudioRenderClient interface on a capture endpoint.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_OUTOFMEMORY if the system is out of memory.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetService(REFIID riid, void ** ppv);

    /**
     *  @function GetStreamLatency
     *      The GetStreamLatency method retrieves the maximum latency for the
     *      current stream and can be called any time after the stream has been
     *      initialized.
     *
     *  @param {REFERENCE_TIME *} phnsLatency
     *      Pointer to a REFERENCE_TIME variable into which the method writes a
     *      time value representing the latency. The time is expressed in
     *      100-nanosecond units. For more information about REFERENCE_TIME,
     *      see the Windows SDK documentation.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_NOT_INITIALIZED  if the audio stream has not been
     *          successfully initialized before calling this function.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameter phnsLatency is NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetStreamLatency(REFERENCE_TIME * phnsLatency);

    /**
     *  @function Initialize
     *      The Initialize method initializes the audio stream.
     *
     *  @param {AUDCLNT_SHAREMODE} ShareMode
     *      The sharing mode for the connection. Through this parameter, the
     *      client tells the audio engine whether it wants to share the audio
     *      endpoint device with other clients. The client should set this
     *      parameter to one of the following AUDCLNT_SHAREMODE enumeration
     *      values:
     *      - AUDCLNT_SHAREMODE_EXCLUSIVE
     *      - AUDCLNT_SHAREMODE_SHARED
     *  @param {DWORD} StreamFlags
     *      Flags to control creation of the stream. The client should set this
     *      parameter to 0 or to the bitwise OR of one or more of the
     *      AUDCLNT_STREAMFLAGS_XXX Constants or the AUDCLNT_SESSIONFLAGS_XXX
     *      Constants.
     *  @param {REFERENCE_TIME} hnsBufferDuration
     *      The buffer capacity as a time value. This parameter is of type
     *      REFERENCE_TIME and is expressed in 100-nanosecond units. This
     *      parameter contains the buffer size that the caller requests for the
     *      buffer that the audio application will share with the audio engine
     *      (in shared mode) or with the endpoint device (in exclusive mode). If
     *      the call succeeds, the method allocates a buffer that is a least
     *      this large. For more information about REFERENCE_TIME, see the
     *      Windows SDK documentation. For more information about buffering
     *      requirements, see Remarks.
     *  @param {REFERENCE_TIME} hnsPeriodicity
     *      The device period. This parameter can be nonzero only in exclusive
     *      mode. In shared mode, always set this parameter to 0. In exclusive
     *      mode, this parameter specifies the requested scheduling period for
     *      successive buffer accesses by the audio endpoint device. If the
     *      requested device period lies outside the range that is set by the
     *      device's minimum period and the system's maximum period, then the
     *      method clamps the period to that range. If this parameter is 0,
     *      the method sets the device period to its default value. To obtain
     *      the default device period, call the IAudioClient::GetDevicePeriod
     *      method. If the AUDCLNT_STREAMFLAGS_EVENTCALLBACK stream flag is set
     *      and AUDCLNT_SHAREMODE_EXCLUSIVE is set as the ShareMode, then
     *      hnsPeriodicity must be nonzero and equal to hnsBufferDuration.
     *  @param {WAVEFORMATEX const *} pFormat
     *      Pointer to a format descriptor. This parameter must point to a valid
     *      format descriptor of type WAVEFORMATEX (or WAVEFORMATEXTENSIBLE).
     *      For more information, see Remarks.
     *  @param {LPCGUID} AudioSessionGuid
     *      Pointer to a session GUID. This parameter points to a GUID value
     *      that identifies the audio session that the stream belongs to. If the
     *      GUID identifies a session that has been previously opened, the
     *      method adds the stream to that session. If the GUID does not
     *      identify an existing session, the method opens a new session and
     *      adds the stream to that session. The stream remains a member of the
     *      same session for its lifetime. Setting this parameter to NULL is
     *      equivalent to passing a pointer to a GUID_NULL value.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_ALREADY_INITIALIZED if the IAudioClient object is already
     *          initialized.
     *      AUDCLNT_E_WRONG_ENDPOINT_TYPE if the AUDCLNT_STREAMFLAGS_LOOPBACK
     *          flag is set but the endpoint device is a capture device, not a
     *          rendering device.
     *      AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED (applies to Windows 7 and later)
     *          if the requested buffer size is not aligned. This code can be
     *          returned for a render or a capture device if the caller
     *          specified AUDCLNT_SHAREMODE_EXCLUSIVE and the
     *          AUDCLNT_STREAMFLAGS_EVENTCALLBACK flags. The caller must call
     *          Initialize again with the aligned buffer size. For more
     *          information, see Remarks.
     *      AUDCLNT_E_BUFFER_SIZE_ERROR (applies to Windows 7 and later)
     *          indicates that the buffer duration value requested by an
     *          exclusive-mode client is out of range. The requested duration
     *          value for pull mode must not be greater than 500 milliseconds;
     *          for push mode the duration value must not be greater than 2
     *          seconds.
     *      AUDCLNT_E_CPUUSAGE_EXCEEDED indicates that the process-pass duration
     *          exceeded the maximum CPU usage. The audio engine keeps track of
     *          CPU usage by maintaining the number of times the process-pass
     *          duration exceeds the maximum CPU usage. The maximum CPU usage is
     *          calculated as a percent of the engine's periodicity. The
     *          percentage value is the system's CPU throttle value (within the
     *          range of 10% and 90%). If this value is not found, then the
     *          default value of 40% is used to calculate the maximum CPU usage.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_DEVICE_IN_USE if the endpoint device is already in use.
     *          Either the device is being used in exclusive mode, or the device
     *          is being used in shared mode and the caller asked to use the
     *          device in exclusive mode.
     *      AUDCLNT_E_ENDPOINT_CREATE_FAILED if the method failed to create the
     *          audio endpoint for the render or the capture device. This can
     *          occur if the audio endpoint device has been unplugged, or the
     *          audio hardware or associated hardware resources have been
     *          reconfigured, disabled, removed, or otherwise made unavailable
     *          for use.
     *      AUDCLNT_E_INVALID_DEVICE_PERIOD (applies to Windows 7 and later)
     *          indicates that the device period requested by an exclusive-mode
     *          client is greater than 500 milliseconds.
     *      AUDCLNT_E_UNSUPPORTED_FORMAT if the audio engine (shared mode) or
     *          audio endpoint device (exclusive mode) does not support the
     *          specified format.
     *      AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED if the caller is requesting
     *          exclusive-mode use of the endpoint device, but the user has
     *          disabled exclusive-mode use of the device.
     *      AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL if the
     *          AUDCLNT_STREAMFLAGS_EVENTCALLBACK flag is set but parameters
     *          hnsBufferDuration and hnsPeriodicity are not equal.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if parameter pFormat is NULL.
     *      E_INVALIDARG if parameter pFormat points to an invalid format
     *          description; or the AUDCLNT_STREAMFLAGS_LOOPBACK flag is set but
     *          ShareMode is not equal to AUDCLNT_SHAREMODE_SHARED; or the
     *          AUDCLNT_STREAMFLAGS_CROSSPROCESS flag is set but ShareMode is
     *          equal to AUDCLNT_SHAREMODE_EXCLUSIVE.
     *          A prior call to SetClientProperties was made with an invalid
     *          category for audio/render streams.
     *      E_OUTOFMEMORY if the system is out of memory.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE Initialize(AUDCLNT_SHAREMODE ShareMode, DWORD StreamFlags,
        REFERENCE_TIME hnsBufferDuration, REFERENCE_TIME hnsPeriodicity,
        WAVEFORMATEX const * pFormat, LPCGUID AudioSessionGuid);

    /**
     *  @function IsFormatSupported
     *      The IsFormatSupported method indicates whether the audio endpoint
     *      device supports a particular stream format.
     *
     *  @param {AUDCLNT_SHAREMODE} ShareMode
     *      The sharing mode for the stream format. Through this parameter, the
     *      client indicates whether it wants to use the specified format in
     *      exclusive mode or shared mode. The client should set this parameter
     *      to one of the following AUDCLNT_SHAREMODE enumeration values:
     *      - AUDCLNT_SHAREMODE_EXCLUSIVE
     *      - AUDCLNT_SHAREMODE_SHARED
     *  @param {WAVEFORMATEX const *} pFormat
     *      Pointer to the specified stream format. This parameter points to a
     *      caller-allocated format descriptor of type WAVEFORMATEX or
     *      WAVEFORMATEXTENSIBLE. The client writes a format description to this
     *      structure before calling this method. For information about
     *      WAVEFORMATEX and WAVEFORMATEXTENSIBLE, see the Windows DDK
     *      documentation.
     *  @param {WAVEFORMATEX **} ppClosestMatch
     *      Pointer to a pointer variable into which the method writes the
     *      address of a WAVEFORMATEX or WAVEFORMATEXTENSIBLE structure. This
     *      structure specifies the supported format that is closest to the
     *      format that the client specified through the pFormat parameter. For
     *      shared mode (that is, if the ShareMode parameter is
     *      AUDCLNT_SHAREMODE_SHARED), set ppClosestMatch to point to a valid,
     *      non-NULL pointer variable. For exclusive mode, set ppClosestMatch to
     *      NULL. The method allocates the storage for the structure. The caller
     *      is responsible for freeing the storage, when it is no longer needed,
     *      by calling the CoTaskMemFree function. If the IsFormatSupported call
     *      fails and ppClosestMatch is non-NULL, the method sets
     *      *ppClosestMatch to NULL. For information about CoTaskMemFree, see
     *      the Windows SDK documentation.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeded and the audio endpoint device supports
     *          the specified stream format.
     *      S_FALSE if the method succeeded with a closest match to the
     *          specified format.
     *      AUDCLNT_E_UNSUPPORTED_FORMAT if the method succeeded but the
     *          specified format is not supported in exclusive mode.
     *      E_POINTER if the parameter pFormat is NULL, or ppClosestMatch is
     *          NULL and ShareMode is AUDCLNT_SHAREMODE_SHARED.
     *      E_INVALIDARG if the parameter ShareMode is a value other than
     *          AUDCLNT_SHAREMODE_SHARED or AUDCLNT_SHAREMODE_EXCLUSIVE.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_OUTOFMEMORY not documented, but is returned if the system is out
     *          of memory.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE IsFormatSupported(AUDCLNT_SHAREMODE ShareMode,
        WAVEFORMATEX const * pFormat, WAVEFORMATEX ** ppClosestMatch);

    /**
     *  @function Reset
     *      The Reset method resets the audio stream.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      S_FALSE if the method succeeds and the stream was already reset.
     *      AUDCLNT_E_NOT_INITIALIZED if the audio stream has not been
     *          successfully initialized.
     *      AUDCLNT_E_NOT_STOPPED if the audio stream was not stopped at the
     *          time the call was made.
     *      AUDCLNT_E_BUFFER_OPERATION_PENDING if the client is currently
     *          writing to or reading from the buffer.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE Reset();

    /**
     *  @function SetEventHandle
     *      The SetEventHandle method sets the event handle that the system
     *      signals when an audio buffer is ready to be processed by the client.
     *
     *  @param {HANDLE} eventHandle
     *      The event handle.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter eventHandle is NULL or an invalid
     *          handle.
     *      AUDCLNT_E_EVENTHANDLE_NOT_EXPECTED if the audio stream was not
     *          initialized for event-driven buffering.
     *      AUDCLNT_E_NOT_INITIALIZED if the audio stream has not been
     *          successfully initialized.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE SetEventHandle(HANDLE eventHandle);

    /**
     *  @function Start
     *      The Start method starts the audio stream.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_NOT_INITIALIZED if the audio stream has not been
     *          successfully initialized.
     *      AUDCLNT_E_NOT_STOPPED if the audio stream was not stopped at the
     *          time of the Start call.
     *      AUDCLNT_E_EVENTHANDLE_NOT_SET if the audio stream is configured to
     *          use event-driven buffering, but the caller has not called
     *          IAudioClient::SetEventHandle to set the event handle on the
     *          stream.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE Start();

    /**
     *  @function Stop
     *      The Stop method stops the audio stream.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds and stops the stream.
     *      S_FALSE if the method succeeds and the stream was already stopped.
     *      AUDCLNT_E_NOT_INITIALIZED if the client has not been successfully
     *          initialized.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE Stop();

    /*
     * IAudioCaptureClient stuff
     */
    /**
     *  @function GetBuffer
     *      Retrieves a pointer to the next available packet of data in the
     *      capture endpoint buffer.
     *
     *  @param {BYTE **} ppData
     *      Pointer to a pointer variable into which the method writes the
     *      starting address of the next data packet that is available for the
     *      client to read.
     *  @param {UINT32 *} pNumFramesToRead
     *      Pointer to a UINT32 variable into which the method writes the frame
     *      count (the number of audio frames available in the data packet). The
     *      client should either read the entire data packet or none of it.
     *  @param {DWORD *} pdwFlags
     *      Pointer to a DWORD variable into which the method writes the
     *      buffer-status flags. The method writes either 0 or the bitwise-OR
     *      combination of one or more of the following _AUDCLNT_BUFFERFLAGS
     *      enumeration values:
     *      - AUDCLNT_BUFFERFLAGS_SILENT
     *      - AUDCLNT_BUFFERFLAGS_DATA_DISCONTINUITY
     *      - AUDCLNT_BUFFERFLAGS_TIMESTAMP_ERROR
     *      Note that the AUDCLNT_BUFFERFLAGS_DATA_DISCONTINUITY flag is not
     *      supported in Windows Vista.
     *      In Windows 7, this flag can be used for glitch detection. To start
     *      the capture stream, the client application must call
     *      IAudioClient::Start followed by calls to GetBuffer in a loop to read
     *      data packets until all of the available packets in the endpoint
     *      buffer have been read. The behavior of the
     *      AUDCLNT_BUFFERFLAGS_DATA_DISCONTINUITY flag is undefined on the
     *      application's first call to GetBuffer after Start. On subsequent
     *      calls, GetBuffer sets this flag to indicate a glitch in the buffer
     *      pointed by ppData.
     *  @param {UINT64 *} pu64DevicePosition
     *      Pointer to a UINT64 variable into which the method writes the device
     *      position of the first audio frame in the data packet. The device
     *      position is expressed as the number of audio frames from the start
     *      of the stream. This parameter can be NULL if the client does not
     *      require the device position. For more information, see Remarks.
     *  @param {UINT64 *} pu64QPCPosition
     *      Pointer to a UINT64 variable into which the method writes the value
     *      of the performance counter at the time that the audio endpoint
     *      device recorded the device position of the first audio frame in the
     *      data packet. The method converts the counter value to 100-nanosecond
     *      units before writing it to *pu64QPCPosition. This parameter can be
     *      NULL if the client does not require the performance counter value.
     *      For more information, see Remarks.
     *
     *  @returns {HRESULT}
     *      S_OK if the call succeeded and *pNumFramesToRead is nonzero,
     *          indicating that a packet is ready to be read.
     *      AUDCLNT_S_BUFFER_EMPTY if the call succeeded and *pNumFramesToRead
     *          is 0, indicating that no capture data is available to be read.
     *      AUDCLNT_E_BUFFER_ERROR, Windows 7: GetBuffer failed to retrieve a
     *          data buffer and *ppData points to NULL. For more information,
     *          see Remarks.
     *      AUDCLNT_E_OUT_OF_ORDER if a previous IAudioCaptureClient::GetBuffer
     *          call is still in effect.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_BUFFER_OPERATION_PENDING if the buffer cannot be accessed
     *          because a stream reset is in progress.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameter ppData, pNumFramesToRead, or pdwFlags is
     *          NULL.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE GetBuffer(BYTE ** ppData,
        UINT32 * pNumFramesToRead, DWORD * pdwFlags,
        UINT64 * pu64DevicePosition, UINT64 * pu64QPCPosition);

    /**
     *  @function GetNextPacketSize
     *      The GetNextPacketSize method retrieves the number of frames in the
     *      next data packet in the capture endpoint buffer.
     *
     *  @param {UINT32 *} pNumFramesInNextPacket
     *      Pointer to a UINT32 variable into which the method writes the frame
     *      count (the number of audio frames in the next capture packet).
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      E_POINTER if the parameter pNumFramesInNextPacket is NULL.
     *      Other return values possible but not documented.
     *      (Non documented) AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED if the stream
     *          was initialized in exclusive mode. This function does not
     *          support exclusive mode.
     */
    HRESULT STDMETHODCALLTYPE GetNextPacketSize(
        UINT32 * pNumFramesInNextPacket);

    /**
     *  @function ReleaseBuffer
     *      The ReleaseBuffer method releases the buffer.
     *
     *  @param {UINT32} NumFramesRead
     *      The number of audio frames that the client read from the capture
     *      buffer. This parameter must be either equal to the number of frames
     *      in the previously acquired data packet or 0.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      AUDCLNT_E_INVALID_SIZE if the NumFramesRead parameter is set to a
     *          value other than the data packet size or 0.
     *      AUDCLNT_E_OUT_OF_ORDER if this call was not preceded by a
     *          corresponding IAudioCaptureClientEmulator::GetBuffer() call.
     *      AUDCLNT_E_DEVICE_INVALIDATED if the audio endpoint device has been
     *          unplugged, or the audio hardware or associated hardware
     *          resources have been reconfigured, disabled, removed, or
     *          otherwise made unavailable for use.
     *      AUDCLNT_E_SERVICE_NOT_RUNNING if the Windows audio service is not
     *          running.
     *      Other return values possible but not documented.
     */
    HRESULT STDMETHODCALLTYPE ReleaseBuffer(UINT32 NumFramesRead);

    /*
     * IUnknown stuff
     */
    /**
     *  @function AddRef
     *      Increments the reference count for an interface on an object. This
     *      method should be called for every new copy of a pointer to an
     *      interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE AddRef();

    /**
     *  @function Release
     *      Decrements the reference count for an interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE Release();

    /**
     *  @function QueryInterface
     *      Retrieves pointers to the supported interfaces on an object.
     *
     *  @param {REFIID} riid
     *      The TODO
     *
     *  @returns {HRESULT}
     *      - S_OK if the interface is supported;
     *      - E_POINTER if ppvObject is NULL;
     *      - E_NOINTERFACE otherwise.
     */
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject);
private:
    /**
     *  @property {AUDCLNT_SHAREMODE} shareMode
     *      The mode of the stream.
     *      Is AUDCLNT_SHAREMODE_SHARED by default (because abscence of
     *      nil-equivalent value in enum), but this does not mean anything
     *      before a successful call to Initialize().
     */
    AUDCLNT_SHAREMODE shareMode;

    /**
     *  @property {DWORD} streamFlags
     *      Contains the StreamFlags passed to Initialize().
     *      Is 0 before that.
     */
    DWORD streamFlags;

    /**
     *  @property {WAVEFORMATEX *} pFormat
     *      Contains the WAVEFORMATEX format that was passed to Initialize().
     *      Is NULL before that.
     */
    WAVEFORMATEX * pFormat;

    /**
     *  @property {REFERENCE_TIME} hnsBufferDuration
     *      Contains the REFERENCE_TIME time that was passed to Initialize()
     *      that determines buffer size.
     *      Is 0 before that.
     */
    REFERENCE_TIME hnsBufferDuration;

    /**
     *  @property {REFERENCE_TIME} hnsPeriodicity
     *      Contains the REFERENCE_TIME time that was passed to Initialize()
     *      for the periodicity of the buffer.
     *      Is 0 before that.
     */
    REFERENCE_TIME hnsPeriodicity;

    /**
     *  @property {bool} initialized
     *      Keeps track of the stream being initialized or not.
     *      Is FALSE before any succesful call to Initialize().
     */
    bool initialized;

    /**
     *  @property {bool} reseted
     *      Keeps track if the stream has been reset.
     *      Is TRUE before any *first* successful call to Start() where it is
     *      set to FALSE.
     *      Is set to TRUE again after a successful call to Reset().
     */
    bool reset;

    /**
     *  @property {bool} started
     *      Keeps track of the stream being started or not.
     *      Is FALSE before any call to Start().
     *      Any successful call to Start() mark it as TRUE.
     *      Any successful call to Stop() mark it as FALSE.
     */
    bool started;

    /**
     *  @property {HANDLE} eventHandle
     *      Handle to an event in case of event-driven access to the stream.
     *      Not really used for now, but hey.
     *      Is NULL until the function SetEventHandle() was successfully called,
     *      and then will be equal to the paramter of said function.
     */
    HANDLE eventHandle;

    /**
     *  @property {UINT32} frameCount
     *      Keeps track of the buffer for IAudioCaptureClient functions.
     *      Also used by IAudioClient::Reset().
     *      Is equals to the number of frame returned by the
     *      IAudioCaptureClient::GetBuffer(), or 0 if no calls were made or if
     *      a successful call to IAudioCaptureClient::ReleaseBuffer() was made.
     */
    UINT32 frameCount;

    /**
     *  @property {UINT32} bufferSize;
     *      The total size in bytes of the buffer we hold, to avoid constant
     *      syscall. Is 0 before any successful call to Initialize() in which it
     *      takes a value sufficiently large for the buffer to hold the backend
     *      data or to respect the requested size.
     */
    UINT32 bufferSize;

    /**
     *  @property {char *} buffer
     *      Buffer holding data transmitted through GetBuffer() call.
     *      Since the function doc's require for GetBuffer to return the same
     *      packet of data if the ReleaseBuffer() was called with a parameter of
     *      0, we need to hold this in. Also, we need to avoid constant syscall
     *      so this helps a bit.
     *      Is NULL before any call to Initialize(), before it is allocated to
     *      a memory area of .bufferSize bytes.
     */
    char * buffer;

    /**
     *  @property {bool} polled
     *      Keeps track of the buffer state, to know if the user requested it.
     *      This is needed for a specificity of the GetBuffer()-ReleaseBuffer()
     *      couple.
     *      If an user calls the release function with a value of 0 while
     *      there's value to read, we need to retransmit the same data in the
     *      next GetBuffer call. So we need all of this mess.
     */
    bool polled;

    /**
     *  @property {UINT64} totalFramesSinceLastReset
     *      Counts the total number of frame we sent since the stream started.
     *      Is 0 until any successful call to ReleaseBuffer(), who help counting
     *      processed frames.
     *      This is used in the GetBuffer function due to the following extract
     *      of the official documentation:
     *      "The device position that the method writes to *pu64DevicePosition
     *      is the stream-relative position of the audio frame that is currently
     *      playing through the speakers (for a rendering stream) or being
     *      recorded through the microphone (for a capture stream). The position
     *      is expressed as the number of frames from the start of the stream."
     */
    UINT64 totalFramesSinceLastReset;

    /**
     *  @property {IMMDeviceEmulator *} device
     *      The linked device object. We need this to access session
     *      information. Undefined behavior if the linked device is freed
     *      before IAudioClientEmulator.
     *
     *  @warning
     *      AddRef() IS NOT CALLED ON THE OBJECT.
     */
    IMMDeviceEmulator * device;

    /**
     *  @function IAudioClientEmulator
     *      Constructor. Private for reasons.
     *
     *  @param {IMMDeviceEmulator *} device
     *      The device we are linked to. Necessary to access session info.
     *      IMPORTANT: The functions does not call AddRef() on it.
     */
    IAudioClientEmulator(IMMDeviceEmulator * device);

    /*
     *  IUnknown-related stuff
     */
    /**
     *  @property {ULONG} referenceCount
     *      Used to implement .AddRef() et .Release() reference counting
     *      mechanism
     */
    ULONG referenceCount;
};

#endif // IMMAUDIOCLIENTEMULATOR_HPP
