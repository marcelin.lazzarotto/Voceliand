/*
 *  File:   Log.hpp
 *  Author: Anenzoras
 *  Date:   2019-02-12
 *
 *  Brief:  Definition for a small logging class, for debug purpose.
 */

#ifndef LOG_HPP
#define LOG_HPP

/* Standard library includes */
#include <cstdio> /* FILE */

/**
 *  @class Log
 *      Dumb logging class to print shit somewhere
 *      No error tested, this assumes it works!
 */
class Log
{
public:
    /**
     *  @function Log
     *      Constructor. Opens a file object.
     *
     *  @warning
     *      Assumes no error.
     *
     *  @param {char const *} filename
     *      Name of the file to open. Might create said file.
     *  @param {char const *} mode
     *      Mode according to fopen() second parameter.
     *      See http://www.cplusplus.com/reference/cstdio/fopen/ for reference.
     */
    Log(char const * filename, char const * mode);

    /**
     *  @function ~Log
     *      Destructor. Closes the file.
     */
    ~Log(void);

    /**
     *  @function write
     *      Wrapper around fprintf and fflush for file object.
     *
     *  @param {char const *} format
     *      The format string.
     *  @param {} ...
     *      Additional parameter list for fprintf.
     */
    void write(char const * format, ...);

    FILE * file;
};

extern Log * dbglog;

#endif // LOG_HPP
