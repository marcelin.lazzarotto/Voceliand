/*
 *  File:   IAudioEndpointVolumeEmulator.hpp
 *  Author: Anenzoras
 *  Date:   2019-03-15
 */

#ifndef VOCELIAND_IAUDIOENDPOINTVOLUMEEMULATOR_HPP
#define VOCELIAND_IAUDIOENDPOINTVOLUMEEMULATOR_HPP

/* External library includes */
#include <endpointvolume.h> /* IAudioEndpointVolume */

/* Project includes */
#include "IMMDeviceEmulator.hpp"

extern IID const IID_IAudioEndpointVolume;

/**
 *  @class IAudioEndpointVolume
 *      Class dealing with volume of an audio session.
 *      Is closely linked to a device because a device is the closest
 *      representation of an audio session.
 *      Undefined behavior if the item is kept up and its functions called while
 *      the related IMMDevice has been released by all holders.
 */
class IAudioEndpointVolumeEmulator
    : public IAudioEndpointVolume
{
public:
    /*
     * IAudioEndpointVolumeEmulator stuff
     */
    /**
     *  @function [static] CreateInstance
     *      Static function to retrieve an instance of the
     *      IAudioEndpointVolumeEmulator classes, to go along MS syntax.
     *
     *  @param {IMMDeviceEmulator const *} device
     *      The device to which this class is linked.
     */
    static IAudioEndpointVolumeEmulator * CreateInstance(
        IMMDeviceEmulator * device);

    /**
     *  @function ~IAudioEndpointVolumeEmulator
     *       Destructor.
     */
    virtual ~IAudioEndpointVolumeEmulator();

    /*
     * IUnknown stuff
     */
    /**
     *  @function AddRef
     *      Increments the reference count for an interface on an object. This
     *      method should be called for every new copy of a pointer to an
     *      interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE AddRef();

    /**
     *  @function Release
     *      Decrements the reference count for an interface on an object.
     *
     *  @returns {ULONG}
     *      The method returns the new reference count. This value is intended
     *      to be used only for test purposes.
     */
    ULONG STDMETHODCALLTYPE Release();

    /**
     *  @function QueryInterface
     *      Retrieves pointers to the supported interfaces on an object.
     *
     *  @param {REFIID} riid
     *      The TODO
     *
     *  @returns {HRESULT}
     *      - S_OK if the interface is supported;
     *      - E_POINTER if ppvObject is NULL;
     *      - E_NOINTERFACE otherwise.
     */
    HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void ** ppvObject);

    /*
     * IAudioEndpointVolume stuff
     */

    /**
     *  @function GetChannelCount
     *      The GetChannelCount method gets a count of the channels in the audio
     *      stream that enters or leaves the audio endpoint device.
     *
     *  @param {UINT *} pnChannelCount
     *      Pointer to a UINT variable into which the method writes the channel
     *      count.
     *
     *  @returns {HRESULT}
     *      S_OK if the function succeeds
     *      E_POINTER if the paramter pnChannelCount is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetChannelCount(UINT * pnChannelCount);

    /**
     *  @function GetChannelVolumeLevel
     *      The GetChannelVolumeLevel method gets the volume level, in decibels,
     *      of the specified channel in the audio stream that enters or leaves
     *      the audio endpoint device.
     *
     *  @param {UINT} nChannel
     *      The channel number. If the audio stream has n channels, the channels
     *      are numbered from 0 to n– 1. To obtain the number of channels in the
     *      stream, call the IAudioEndpointVolume::GetChannelCount method.
     *  @param {float *} pfLevelDB
     *      Pointer to a float variable into which the method writes the volume
     *      level in decibels. To get the range of volume levels obtained from
     *      this method, call the IAudioEndpointVolume::GetVolumeRange method.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter nChannel is greater than or equal to
     *          the number of channels in the stream.
     *      E_POINTER if parameter pfLevelDB is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetChannelVolumeLevel(UINT nChannel,
        float * pfLevelDB);

    /**
     *  @function GetChannelVolumeLevelScalar
     *      The GetChannelVolumeLevelScalar method gets the normalized,
     *      audio-tapered volume level of the specified channel of the audio
     *      stream that enters or leaves the audio endpoint device.
     *
     *  @param {UINT} nChannel
     *      The channel number. If the audio stream contains n channels, the
     *      channels are numbered from 0 to n– 1. To obtain the number of
     *      channels, call the IAudioEndpointVolume::GetChannelCount method.
     *  @param {float *} pfLevel
     *      Pointer to a float variable into which the method writes the volume
     *      level. The level is expressed as a normalized value in the range
     *      from 0.0 to 1.0.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter nChannel is greater than or equal to
     *          the number of channels in the stream.
     *      E_POINTER if the parameter pfLevel is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetChannelVolumeLevelScalar(UINT nChannel,
        float * pfLevel);

    /**
     *  @function GetMasterVolumeLevel
     *      The GetMasterVolumeLevel method gets the master volume level, in
     *      decibels, of the audio stream that enters or leaves the audio
     *      endpoint device.
     *
     *  @param {float *} pfLevelDB
     *      Pointer to the master volume level. This parameter points to a float
     *      variable into which the method writes the volume level in decibels.
     *      To get the range of volume levels obtained from this method, call
     *      the IAudioEndpointVolume::GetVolumeRange method.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pfLevelDB is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetMasterVolumeLevel(float * pfLevelDB);

    /**
     *  @function GetMasterVolumeLevelScalar
     *      The GetMasterVolumeLevelScalar method gets the master volume level
     *      of the audio stream that enters or leaves the audio endpoint device.
     *      The volume level is expressed as a normalized, audio-tapered value
     *      in the range from 0.0 to 1.0.
     *
     *  @param {float *} pfLevel
     *      Pointer to the master volume level. This parameter points to a float
     *      variable into which the method writes the volume level. The level is
     *      expressed as a normalized value in the range from 0.0 to 1.0.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pfLevel is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetMasterVolumeLevelScalar(float * pfLevel);

    /**
     *  @function GetMute
     *      The GetMute method gets the muting state of the audio stream that
     *      enters or leaves the audio endpoint device.
     *
     *  @param {BOOL *} pbMute
     *      Pointer to a BOOL variable into which the method writes the muting
     *      state. If *pbMute is TRUE, the stream is muted. If FALSE, the stream
     *      is not muted.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pbMute is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetMute(BOOL * pbMute);

    /**
     *  @function GetVolumeRange
     *      The GetVolumeRange method gets the volume range, in decibels, of the
     *      audio stream that enters or leaves the audio endpoint device.
     *
     *  @param {float *} pflVolumeMindB
     *      Pointer to the minimum volume level. This parameter points to a
     *      float variable into which the method writes the minimum volume level
     *      in decibels. This value remains constant for the lifetime of the
     *      IAudioEndpointVolume interface instance.
     *  @param {float *} pflVolumeMaxdB
     *      Pointer to the maximum volume level. This parameter points to a
     *      float variable into which the method writes the maximum volume level
     *      in decibels. This value remains constant for the lifetime of the
     *      IAudioEndpointVolume interface instance.
     *  @param {float *} pflVolumeIncrementdB
     *      Pointer to the volume increment. This parameter points to a float
     *      variable into which the method writes the volume increment in
     *      decibels. This increment remains constant for the lifetime of the
     *      IAudioEndpointVolume interface instance.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pfLevelMinDB, pfLevelMaxDB, or
     *          pfVolumeIncrementDB is NULL.
     */
    HRESULT STDMETHODCALLTYPE GetVolumeRange(float * pflVolumeMindB,
        float * pflVolumeMaxdB, float * pflVolumeIncrementdB);

    /**
     *  @function GetVolumeStepInfo
     *      The GetVolumeStepInfo method gets information about the current step
     *      in the volume range.
     *
     *  @param {UINT *} pnStep
     *      Pointer to a UINT variable into which the method writes the current
     *      step index. This index is a value in the range from 0 to
     *      *pStepCount-1, where 0 represents the minimum volume level and
     *      *pStepCount-1 represents the maximum level.
     *  @param {UINT *} pnStepCount
     *      Pointer to a UINT variable into which the method writes the number
     *      of steps in the volume range. This number remains constant for the
     *      lifetime of the IAudioEndpointVolume interface instance.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameters pnStep and pnStepCount are both NULL.
     */
    HRESULT STDMETHODCALLTYPE GetVolumeStepInfo(UINT * pnStep,
        UINT * pnStepCount);

    /**
     *  @function QueryHardwareSupport
     *      The QueryHardwareSupport method queries the audio endpoint device
     *      for its hardware-supported functions.
     *
     *  @param {DWORD *} pdwHardwareSupportMask
     *      Pointer to a DWORD variable into which the method writes a hardware
     *      support mask that indicates the hardware capabilities of the audio
     *      endpoint device. The method can set the mask to 0 or to the
     *      bitwise-OR combination of one or more ENDPOINT_HARDWARE_SUPPORT_XXX
     *      constants.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pdwHardwareSupportMask is NULL.
     */
    HRESULT STDMETHODCALLTYPE QueryHardwareSupport(
        DWORD * pdwHardwareSupportMask);

    /**
     *  @function RegisterControlChangeNotify
     *      The RegisterControlChangeNotify method registers a client's
     *      notification callback interface.
     *
     *  @param {IAudioEndpointVolumeCallback *} pNotify
     *      Pointer to the IAudioEndpointVolumeCallback interface that the
     *      client is registering for notification callbacks. If the
     *      RegisterControlChangeNotify method succeeds, it calls the AddRef
     *      method on the client's IAudioEndpointVolumeCallback interface.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pNotify is NULL.
     */
    HRESULT STDMETHODCALLTYPE RegisterControlChangeNotify(
        IAudioEndpointVolumeCallback * pNotify);

    /**
     *  @function SetChannelVolumeLevel
     *      The SetChannelVolumeLevel method sets the volume level, in decibels,
     *      of the specified channel of the audio stream that enters or leaves
     *      the audio endpoint device.
     *
     *  @param {UINT} nChannel
     *      The channel number. If the audio stream contains n channels, the
     *      channels are numbered from 0 to n– 1. To obtain the number of
     *      channels, call the IAudioEndpointVolume::GetChannelCount method.
     *  @param {float} fLevelDB
     *      The new volume level in decibels. To obtain the range and
     *      granularity of the volume levels that can be set by this method,
     *      call the IAudioEndpointVolume::GetVolumeRange method.
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the
     *      SetChannelVolumeLevel call changes the volume level of the endpoint,
     *      all clients that have registered IAudioEndpointVolumeCallback
     *      interfaces with that endpoint will receive notifications. In its
     *      implementation of the OnNotify method, a client can inspect the
     *      event-context GUID to discover whether it or another client is the
     *      source of the volume-change event. If the caller supplies a NULL
     *      pointer for this parameter, the notification routine receives the
     *      context GUID value GUID_NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter nChannel is greater than or equal to
     *          the number of channels in the stream; or parameter fLevelDB lies
     *          outside of the volume range supported by the device.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE SetChannelVolumeLevel(UINT nChannel,
        float fLevelDB, LPCGUID pguidEventContext);

    /**
     *  @function SetChannelVolumeLevelScalar
     *      The SetChannelVolumeLevelScalar method sets the normalized,
     *      audio-tapered volume level of the specified channel in the audio
     *      stream that enters or leaves the audio endpoint device.
     *
     *  @param {UINT} nChannel
     *      The channel number. If the audio stream contains n channels, the
     *      channels are numbered from 0 to n– 1. To obtain the number of
     *      channels, call the IAudioEndpointVolume::GetChannelCount method.
     *  @param {float} fLevel
     *      The volume level. The volume level is expressed as a normalized
     *      value in the range from 0.0 to 1.0.
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the
     *      SetChannelVolumeLevelScalar call changes the volume level of the
     *      endpoint, all clients that have registered
     *      IAudioEndpointVolumeCallback interfaces with that endpoint will
     *      receive notifications. In its implementation of the OnNotify method,
     *      a client can inspect the event-context GUID to discover whether it
     *      or another client is the source of the volume-change event. If the
     *      caller supplies a NULL pointer for this parameter, the notification
     *      routine receives the context GUID value GUID_NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter nChannel is greater than or equal to
     *          the number of channels in the stream; or parameter fLevel is
     *          outside the range from 0.0 to 1.0.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE SetChannelVolumeLevelScalar(UINT nChannel,
        float fLevel, LPCGUID pguidEventContext);

    /**
     *  @function SetMasterVolumeLevel
     *      The SetMasterVolumeLevel method sets the master volume level, in
     *      decibels, of the audio stream that enters or leaves the audio
     *      endpoint device.
     *
     *  @param {float} fLevelDB
     *      The new master volume level in decibels. To obtain the range and
     *      granularity of the volume levels that can be set by this method,
     *      call the IAudioEndpointVolume::GetVolumeRange method.
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the
     *      SetMasterVolumeLevel call changes the volume level of the endpoint,
     *      all clients that have registered IAudioEndpointVolumeCallback
     *      interfaces with that endpoint will receive notifications. In its
     *      implementation of the OnNotify method, a client can inspect the
     *      event-context GUID to discover whether it or another client is the
     *      source of the volume-change event. If the caller supplies a NULL
     *      pointer for this parameter, the notification routine receives the
     *      context GUID value GUID_NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter fLevelDB lies outside of the volume
     *          range supported by the device.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE SetMasterVolumeLevel(float fLevelDB,
        LPCGUID pguidEventContext);

    /**
     *  @function SetMasterVolumeLevelScalar
     *      The SetMasterVolumeLevelScalar method sets the master volume level
     *      of the audio stream that enters or leaves the audio endpoint device.
     *      The volume level is expressed as a normalized, audio-tapered value
     *      in the range from 0.0 to 1.0.
     *
     *  @param {float} fLevel
     *      The new master volume level. The level is expressed as a normalized
     *      value in the range from 0.0 to 1.0.
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the
     *      SetMasterVolumeLevelScalar call changes the volume level of the
     *      endpoint, all clients that have registered
     *      IAudioEndpointVolumeCallback interfaces with that endpoint will
     *      receive notifications. In its implementation of the OnNotify method,
     *      a client can inspect the event-context GUID to discover whether it
     *      or another client is the source of the volume-change event. If the
     *      caller supplies a NULL pointer for this parameter, the notification
     *      routine receives the context GUID value GUID_NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_INVALIDARG if the parameter fLevelDB lies outside of the volume
     *          range from 0.0 to 1.0.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE SetMasterVolumeLevelScalar(float fLevel,
        LPCGUID pguidEventContext);

    /**
     *  @function SetMute
     *      The SetMute method sets the muting state of the audio stream that
     *      enters or leaves the audio endpoint device.
     *
     *  @param {BOOL} bMute
     *      The new muting state. If bMute is TRUE, the method mutes the stream.
     *      If FALSE, the method turns off muting.
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the SetMute call
     *      changes the muting state of the endpoint, all clients that have
     *      registered IAudioEndpointVolumeCallback interfaces with that
     *      endpoint will receive notifications. In its implementation of the
     *      OnNotify method, a client can inspect the event-context GUID to
     *      discover whether it or another client is the source of the
     *      control-change event. If the caller supplies a NULL pointer for this
     *      parameter, the notification routine receives the context GUID value
     *      GUID_NULL.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      S_FALSE if the method succeeds but the given mute value is the same
     *          as the current state.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE SetMute(BOOL bMute, LPCGUID pguidEventContext);

    /**
     *  @function UnregisterControlChangeNotify
     *      The UnregisterControlChangeNotify method deletes the registration of
     *      a client's notification callback interface that the client
     *      registered in a previous call to the
     *      IAudioEndpointVolume::RegisterControlChangeNotify method.
     *
     *  @param {IAudioEndpointVolumeCallback *} pNotify
     *      Pointer to the client's IAudioEndpointVolumeCallback interface. The
     *      client passed this same interface pointer to the endpoint volume
     *      object in a previous call to the
     *      IAudioEndpointVolume::RegisterControlChangeNotify method. If the
     *      UnregisterControlChangeNotify method succeeds, it calls the Release
     *      method on the client's IAudioEndpointVolumeCallback interface.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_POINTER if the parameter pNotify is NULL.
     */
    HRESULT STDMETHODCALLTYPE UnregisterControlChangeNotify(
        IAudioEndpointVolumeCallback * pNotify);

    /**
     *  @function VolumeStepDown
     *      The VolumeStepDown method decrements, by one step, the volume level
     *      of the audio stream that enters or leaves the audio endpoint device.
     *
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the
     *      VolumeStepDown call changes the volume level of the endpoint, all
     *      clients that have registered IAudioEndpointVolumeCallback interfaces
     *      with that endpoint will receive notifications. In its implementation
     *      of the OnNotify method, a client can inspect the event-context GUID
     *      to discover whether it or another client is the source of the
     *      volume-change event. If the caller supplies a NULL pointer for this
     *      parameter, the client's notification method receives a NULL context
     *      pointer.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE VolumeStepDown(LPCGUID pguidEventContext);

    /**
     *  @function VolumeStepUp
     *      The VolumeStepUp method increments, by one step, the volume level of
     *      the audio stream that enters or leaves the audio endpoint device.
     *
     *  @param {LPCGUID} pguidEventContext
     *      Context value for the IAudioEndpointVolumeCallback::OnNotify method.
     *      This parameter points to an event-context GUID. If the VolumeStepUp
     *      call changes the volume level of the endpoint, all clients that have
     *      registered IAudioEndpointVolumeCallback interfaces with that
     *      endpoint will receive notifications. In its implementation of the
     *      OnNotify method, a client can inspect the event-context GUID to
     *      discover whether it or another client is the source of the
     *      volume-change event. If the caller supplies a NULL pointer for this
     *      parameter, the client's notification method receives a NULL context
     *      pointer.
     *
     *  @returns {HRESULT}
     *      S_OK if the method succeeds.
     *      E_OUTOFMEMORY if the system is out of memory.
     */
    HRESULT STDMETHODCALLTYPE VolumeStepUp(LPCGUID pguidEventContext);

private:
    /*
     * IAudioClientEmulator stuff
     */
    /**
     *  @function IAudioEndpointVolumeEmulator
     *      Constructor. Private for reasons. Stores the given pointer.
     *
     *  @param {IMMDeviceEmulator const *} device
     *      The device to which this object is linked. WARNING: DOES NOT CALL
     *      AddRef() ON THE OBJECT!
     */
    IAudioEndpointVolumeEmulator(IMMDeviceEmulator * device);

    /**
     *  @property {ULONG} referenceCount
     *      Used to implement .AddRef() et .Release() reference counting
     *      mechanism
     */
    ULONG referenceCount;

    /**
     *  @propety {IMMDeviceEmulator const *} device
     *      The device with which the instance will be linked. Undefined
     *      behavior if the device is released meanwhile an
     *      IAudioEndpointVolumeEmulator object is used.
     */
    IMMDeviceEmulator * device;
};

#endif // VOCELIAND_IAUDIOENDPOINTVOLUMEEMULATOR_HPP
