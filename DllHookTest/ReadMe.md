# What's that

This is a preliminary test, proof of concept of a future proof of concept on how
to mimic a microphone onto other programs.

This uses DLL injection and function hooking.

DllHookTest.exe, the injector, calls onto several windows specific functions to
load a DLL (located in the binaries/[platform]/ directory) into other process.
Upon loading that DLL will perform Hooking by replacing the CoCreateInstance
function by a proxy function. The proxy function will give access to a specific
interface that act as a proxy for the process (or rely on the base
CoCreateInstance function for others interfaces). The goal is to be as
transparent as possible for the running process and the system.

Of course, this method is highly unstable and does not entirely work for now,
yet I am confident that this solution will perform decently in a not-so-far
future.

# Proxied interfaces

Current Windows Audio API proxied interfaces are:

- IMMDeviceEnumerator: an interface that enumerates the devices on Windows.
- IMMDeviceCollection: an interface that is an collection of IMMDevice objects.
- IMMDevice and IEndpoint: interfaces that gives access to several metadata of
    the device (name, id, type (in/out)...).
- IAudioClient and IAudioCaptureClient: interfaces that deals with communication
    by itself. For now, the proxy class is problematic due to marshaling, which
    is used by portaudio, which itself is used by audacity, the third-party
    program into which the current manual tests are performed.

# Additional tools

The makefile is here to help compiling the DLL in itself that will be used for
hooking and the program DllHookTest.exe that performs injection.

For basic debugging the DLL implements a small class, Log, that is used to
open a file in the binaries/[platform]/ directory so that we have more info on
what's happening.

# Note

Is used with audacity 2.3 at least, and make sure to select WASAPI in audacity
interface selection (top left).

THIS DIRECTORY WILL BE DELETED ONCE EVERYTHING WORKS AT LEAST A BIT AND MERGED
INTO Voceliand!
