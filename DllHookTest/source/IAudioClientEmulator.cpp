/*
 *  File:   IAudioClientEmulator.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-14
 *
 *  Brief:  Implementation of the class emulating an IAudioClient interface.
 */

/* External library includes */
#include <Audiosessiontypes.h> /* StreamFlags of Initialize() */

/* Project includes */
#include "IAudioClientEmulator.hpp"
#include "SharedAudioBuffer.hpp"
#include "Log.hpp"

IID const IID_IAudioClient = __uuidof(IAudioClient);
/**
 *  @property {IID const} IID_IAudioRenderClient
 *      The unique identifier for the IAudioRenderClient interface.
 */
IID const IID_IAudioRenderClient = __uuidof(IAudioRenderClient);
/**
 *  @property {IID const} IID_IAudioCaptureClient
 *      The unique identifier for the IAudioCaptureClient interface.
 */
IID const IID_IAudioCaptureClient = __uuidof(IAudioCaptureClient);

/* Note 0:
 * There's an error we should return in most functions which is
 * AUDCLNT_E_SERVICE_NOT_RUNNING but I don't believe there's a situation where
 * this error happens with our hack. We might need additional checks just to
 * make sure we send this error would the case happen. But I believe there
 * aren't a lot of real-life situation where the windows audio client is not
 * running while Voceliand runs.
 * -Anenzoras, 2019-02-24 */

/* Note 1:
 * REFERENCE_TIME is a Windows typedef for 64bit integer, and represents a
 * duration in 100nanosec per unit.
 * Because, sure, why not?
 * -Anenzoras, 2019-02-24 */

/* Note 2:
 * For now, we say the device does not support exclusive mode and current
 * implementation reflect that.
 * This is probably not wanted for a microphone anyway, but make sure to check
 * one day.
 * -Anenzoras, 2019-02-24 */

/* Function that generates a WAVEFORMATEX object describing the default format
 * for our device. */
WAVEFORMATEX * newDefaultFormat(void)
{
    WAVEFORMATEX * res = (WAVEFORMATEX *) CoTaskMemAlloc(sizeof(WAVEFORMATEX));
    if (res != NULL)
    {
        res->wFormatTag = WAVE_FORMAT_PCM;
        res->nChannels = 2;
        res->nSamplesPerSec = 44100;
        res->wBitsPerSample = 16;
        res->nBlockAlign = res->nChannels * res->wBitsPerSample;
        res->nAvgBytesPerSec = res->nSamplesPerSec * res->nBlockAlign;
        res->cbSize = 0;
    }

    return res;
}

/* Helper*/
namespace
{
    // getCurrentTime(); -> in SAB for now

    /**
     *  @function getPerformanceCounter
     *      A necessary helper for the function GetBuffer(), this function
     *      return the performance counter as follow (extracted from the MS
     *      doc):
     *      "The performance counter value that GetBuffer writes to
     *      *pu64QPCPosition is not the raw counter value obtained from the
     *      QueryPerformanceCounter function. If t is the raw counter value, and
     *      if f is the frequency obtained from the QueryPerformanceFrequency
     *      function, GetBuffer calculates the performance counter value as
     *      follows:
     *      The result is expressed in 100-nanosecond units."
     *
     *  @returns {UINT64}
     *      The computed performance counter.
     */
    UINT64 getPerformanceCounter(void)
    {
        static LARGE_INTEGER frequency;

        QueryPerformanceFrequency(&frequency);

        LARGE_INTEGER time;

        // Get the current time
        QueryPerformanceCounter(&time);

        return (1e7 * time.QuadPart / frequency.QuadPart);
    }
}

/*
 *  IAudioClientEmulator stuff
 */
/* Returns an instance of IAudioClientEmulator to encapsulate thingies. */
IAudioClientEmulator * IAudioClientEmulator::CreateInstance(
    IMMDeviceEmulator * device)
{
    if (device == NULL)
    {
        return NULL;
    }

    IAudioClientEmulator * emulator = new IAudioClientEmulator(device);
    if (emulator == NULL)
    {
        return NULL;
    }

    emulator->AddRef();

    return emulator;
}

/* Constructor. Private for reasons. */
IAudioClientEmulator::IAudioClientEmulator(IMMDeviceEmulator * device)
{
    /* IAudioClientEmulator */
    this->device = device;

    this->shareMode = AUDCLNT_SHAREMODE_SHARED;
    this->streamFlags = 0;
    this->pFormat = NULL;
    this->hnsBufferDuration = 0;
    this->hnsPeriodicity = 0;
    this->initialized = FALSE;
    this->reset = TRUE;
    this->started = FALSE;
    this->eventHandle = NULL;

    this->frameCount = 0;
    this->bufferSize = 0;
    this->buffer = NULL;
    this->totalFramesSinceLastReset = 0;

    /* IUnknown */
    this->referenceCount = 0;
}

/* Destructor. */
IAudioClientEmulator::~IAudioClientEmulator()
{
    if (this->pFormat)
    {
        CoTaskMemFree(this->pFormat);
    }

    if (this->buffer)
    {
        delete this->buffer;
    }
}

/*
 * IUnknown stuff
 */
/* Increments the reference count for an interface on an object. This method
 * should be called for every new copy of a pointer to an interface on an
 * object. */
ULONG IAudioClientEmulator::AddRef()
{
    dbglog->write("Enter IAudioClientEmulator::AddRef()\n");

    InterlockedIncrement(&this->referenceCount);

    dbglog->write("Exit IAudioClientEmulator::AddRef()\n");

    return this->referenceCount;
}

/* Decrements the reference count for an interface on an object. */
ULONG IAudioClientEmulator::Release()
{
    dbglog->write("Enter IAudioClientEmulator::Release()\n");

    ULONG ref = InterlockedDecrement(&this->referenceCount);
    if (this->referenceCount == 0)
    {
        /* End of life */
        delete this;
    }

    dbglog->write("Exit IAudioClientEmulator::Release()\n");

    return ref;
}

/* Retrieves pointers to the supported interfaces on an object. */
HRESULT IAudioClientEmulator::QueryInterface(REFIID riid,
    void ** ppvObject)
{
    dbglog->write("Enter IAudioClientEmulator::QueryInterface()\n");

    if (ppvObject == NULL)
    {
        return E_POINTER;
    }
    (*ppvObject) = NULL;

    HRESULT result = E_NOINTERFACE;

    if (IsEqualIID(riid, IID_IUnknown))
    {
        (*ppvObject) = this;

        dbglog->write("    IUnkown\n");

        result = S_OK;
    }
    else if (IsEqualIID(riid, IID_IAudioClient))
    {
        (*ppvObject) = static_cast<IAudioClient *>(this);

        dbglog->write("    IAudioClient\n");

        result = S_OK;
    }
    else if (IsEqualIID(riid, IID_IAudioCaptureClient))
    {
        (*ppvObject) = static_cast<IAudioCaptureClient *>(this);

        dbglog->write("    IAudioCaptureClient\n");

        result = S_OK;
    }

    if (result == S_OK)
    {
        this->AddRef();
    }
    else
    {
        wchar_t * riidString = NULL;
        StringFromCLSID(riid, &riidString);

        fwprintf(dbglog->file, L"    %ls\n", riidString);

        CoTaskMemFree(riidString);
    }

    dbglog->write("Exit IAudioClientEmulator::QueryInterface()\n");

    return result;
}

/* The GetBufferSize method retrieves the size (maximum capacity) of the
 * endpoint buffer. */
HRESULT IAudioClientEmulator::GetBufferSize(UINT32 * pNumBufferFrames)
{
    dbglog->write("Enter IAudioClientEmulator::GetBufferSize()\n");

    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (pNumBufferFrames == NULL)
    {
        return E_POINTER;
    }

    (*pNumBufferFrames) = this->bufferSize;

    dbglog->write("Exit IAudioClientEmulator::GetBufferSize()\n");

    return S_OK;
}

/* The GetCurrentPadding method retrieves the number of frames of padding in the
 * endpoint buffer. */
HRESULT IAudioClientEmulator::GetCurrentPadding(UINT32 * pNumPaddingFrames)
{
    dbglog->write("Enter IAudioClientEmulator::GetCurrentPadding()\n");

    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (pNumPaddingFrames == NULL)
    {
        return E_POINTER;
    }

    /* TODO
     * This needs to be changed when SharedAudioBuffer is reworked after basic
     * testing when merging with Voceliand.
     * -Anenzoras, 2019-02-27 */
    (*pNumPaddingFrames) = sharedBuffer.getBufferSize();

    dbglog->write("Exit IAudioClientEmulator::GetCurrentPadding()\n");

    return S_OK;
}

/* The GetDevicePeriod method retrieves the length of the periodic interval
 * separating successive processing passes by the audio engine on the data in
 * the endpoint buffer. */
HRESULT IAudioClientEmulator::GetDevicePeriod(
    REFERENCE_TIME * phnsDefaultDevicePeriod,
    REFERENCE_TIME * phnsMinimumDevicePeriod)
{
    dbglog->write("Enter IAudioClientEmulator::GetDevicePeriod()\n");
    /* See note 0 on top of the file regarding AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if ((phnsDefaultDevicePeriod == NULL) && (phnsMinimumDevicePeriod == NULL))
    {
        return E_POINTER;
    }

    /* TODO: We send the same value for both for now; be sure to check that one
     * day.
     * -Anenzoras, 2019-02-24 */

    if (phnsDefaultDevicePeriod != NULL)
    {
        (*phnsDefaultDevicePeriod) = (LONGLONG)(sharedBuffer.refreshRate * 1.0e7f);
    }
    if (phnsMinimumDevicePeriod != NULL)
    {
        (*phnsMinimumDevicePeriod) = (LONGLONG)(sharedBuffer.refreshRate * 1.0e7f);
    }

    dbglog->write("Exit IAudioClientEmulator::GetDevicePeriod()\n");

    return S_OK;
}

/* The GetMixFormat method retrieves the stream format that the audio engine
 * uses for its internal processing of shared-mode streams. */
HRESULT IAudioClientEmulator::GetMixFormat(WAVEFORMATEX ** ppDeviceFormat)
{
    dbglog->write("Enter IAudioClientEmulator::GetMixFormat()\n");
    /* See note 0 on top of the file regarding AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (ppDeviceFormat == NULL)
    {
        return E_POINTER;
    }

    (*ppDeviceFormat) = newDefaultFormat();
    if ((*ppDeviceFormat) == NULL)
    {
        return E_OUTOFMEMORY;
    }

    dbglog->write("Exit IAudioClientEmulator::GetMixFormat()\n");

    return S_OK;
}

/* The GetService method accesses additional services from the audio client
 * object. */
HRESULT IAudioClientEmulator::GetService(REFIID riid, void ** ppv)
{
    dbglog->write("Enter IAudioClientEmulator::GetService()\n");
    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (ppv == NULL)
    {
        return E_POINTER;
    }

    if (!this->initialized)
    {
        ppv = NULL;
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (!sharedBuffer.connected)
    {
        ppv = NULL;
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    /* Specific error case */
    if (IsEqualIID(riid, IID_IAudioRenderClient))
    {
        return AUDCLNT_E_WRONG_ENDPOINT_TYPE;
    }

    HRESULT result = E_NOINTERFACE;

    if (IsEqualIID(riid, IID_IAudioCaptureClient))
    {
        dbglog->write("    IAudioCaptureClient\n");

        /* We actully don't return E_OUTOFMEMORY. */
        (*ppv) = static_cast<IAudioCaptureClient *>(this);
        this->AddRef();

        result = S_OK;
    }
    else
    {
        dbglog->write("    NOINTERFACE\n");

        /* We support only IID_IAudioCaptureClient. I believe that's all we
         * should support, but let's look into it later, shall we?
         * -Anenzoras, 2019-02-27 */
        (*ppv) = NULL;
    }

    dbglog->write("Exit IAudioClientEmulator::GetService()\n");

    return result;
}

/* The GetStreamLatency method retrieves the maximum latency for the
 * current stream and can be called any time after the stream has been
 * initialized. */
HRESULT IAudioClientEmulator::GetStreamLatency(REFERENCE_TIME * phnsLatency)
{
    dbglog->write("Enter IAudioClientEmulator::GetStreamLatency()\n");
    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (phnsLatency == NULL)
    {
        return E_POINTER;
    }

    /* TODO
     * Let's arbitrary say the maximum latency is equals to 0.5 seconds.
     * (Reminder that REFERENCE_TIME is in 100-nanosec unit).
     * Pretty bad, hehe ^^' n_n u_u -_-
     * -Anenzoras, 2019-02-27 */
    (*phnsLatency) = 5 * 1e6;

    dbglog->write("Exit IAudioClientEmulator::GetStreamLatency()\n");

    return S_OK;
}

/**
 *  @function Initialize
 *      The Initialize method initializes the audio stream.
 */
HRESULT IAudioClientEmulator::Initialize(AUDCLNT_SHAREMODE ShareMode, DWORD StreamFlags,
    REFERENCE_TIME hnsBufferDuration, REFERENCE_TIME hnsPeriodicity,
    WAVEFORMATEX const * pFormat, LPCGUID AudioSessionGuid)
{
    dbglog->write("Enter IAudioClientEmulator::Initialize()\n");
    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    /* I'm unsure on how to deal with AUDCLNT_E_CPUUSAGE_EXCEEDED error, let's
     * ignore this for now and just assume that our program is fast enough to
     * deal with that.
     * -Anenzoras, 2019-02-25 */

    if (this->initialized)
    {
        dbglog->write("    0\n");
        return AUDCLNT_E_ALREADY_INITIALIZED;
    }

    if (StreamFlags & AUDCLNT_STREAMFLAGS_LOOPBACK)
    {
        dbglog->write("    1\n");
        return AUDCLNT_E_WRONG_ENDPOINT_TYPE;
    }

    if (!sharedBuffer.connected)
    {
        /* There's two different error returned by the Initialize() function
         * that should be returned for the same reason (according to MS' doc
         * anyway): if the audio endpoint device has been unplugged, or the
         * audio hardware or associated hardware resources have been
         * reconfigured, disabled, removed, or otherwise made unavailable for
         * use. The two errors are AUDCLNT_E_DEVICE_INVALIDATED and
         * AUDCLNT_E_ENDPOINT_CREATE_FAILED. In my opinion, the most precise one
         * in our case and in term of exhaustivity is
         * AUDCLNT_E_DEVICE_INVALIDATED because the other one's documentation
         * might imply that there's other cases where it could be returned
         * (however, I don't know those).
         * - Anenzoras, 2019-02-25 */
        dbglog->write("    2\n");
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (ShareMode == AUDCLNT_SHAREMODE_EXCLUSIVE)
    {
        /*  Due to the above check, we exclude the following error cases from
         *  happening:
         *  - AUDCLNT_E_BUFFER_SIZE_NOT_ALIGNED (applies to Windows 7 and later)
         *      if the requested buffer size is not aligned. This code can be
         *      returned for a render or a capture device if the caller
         *      specified AUDCLNT_SHAREMODE_EXCLUSIVE and the
         *      AUDCLNT_STREAMFLAGS_EVENTCALLBACK flags. The caller must call
         *      Initialize again with the aligned buffer size. For more
         *      information, see Remarks.
         *  - AUDCLNT_E_BUFFER_SIZE_ERROR (applies to Windows 7 and later)
         *      indicates that the buffer duration value requested by an
         *      exclusive-mode client is out of range. The requested duration
         *      value for pull mode must not be greater than 500 milliseconds;
         *      for push mode the duration value must not be greater than 2
         *      seconds.
         *  - AUDCLNT_E_DEVICE_IN_USE if the endpoint device is already in use.
         *      Either the device is being used in exclusive mode, or the device
         *      is being used in shared mode and the caller asked to use the
         *      device in exclusive mode.
         *  - AUDCLNT_E_INVALID_DEVICE_PERIOD (applies to Windows 7 and later)
         *      indicates that the device period requested by an exclusive-mode
         *      client is greater than 500 milliseconds.
         *
         *  -Anenzoras, 2019-02-23
         */
        dbglog->write("    3\n");
        return AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED;
    }

    if ((StreamFlags & AUDCLNT_STREAMFLAGS_EVENTCALLBACK)
        && (hnsBufferDuration != hnsPeriodicity))
    {
        dbglog->write("    4\n");
        return AUDCLNT_E_BUFDURATION_PERIOD_NOT_EQUAL;
    }

    if (pFormat == NULL)
    {
        dbglog->write("    5\n");
        return E_POINTER;
    }

    WAVEFORMATEX * closestMatch = NULL;
    HRESULT res = S_OK;

    res = IsFormatSupported(ShareMode, pFormat, &closestMatch);
    if (res != S_OK)
    {
        if (res == E_OUTOFMEMORY)
        {
            dbglog->write("    6\n");
            return E_OUTOFMEMORY;
        }

        dbglog->write("    7\n");

        CoTaskMemFree(closestMatch);
        closestMatch = NULL;

        return AUDCLNT_E_UNSUPPORTED_FORMAT;
    }

    if (((StreamFlags & AUDCLNT_STREAMFLAGS_LOOPBACK)
            && (ShareMode != AUDCLNT_SHAREMODE_SHARED))
        || ((StreamFlags & AUDCLNT_STREAMFLAGS_CROSSPROCESS)
            && (ShareMode == AUDCLNT_SHAREMODE_EXCLUSIVE)))
    {
        /* We should also check if pFormat is a valid format description but for
         * now we don't care.
         * We also should treat "A prior call to SetClientProperties was made
         * with an invalid category for audio/render streams." but I haven't got
         * any clue here.
         * -Anenzoras, 2019-02-25 */
        dbglog->write("    8\n");
        return E_INVALIDARG;
    }

    /* For info about StreamFlags, we go to this places:
     * - https://docs.microsoft.com/fr-fr/windows/desktop/CoreAudio/audclnt-streamflags-xxx-constants
     * - https://docs.microsoft.com/fr-fr/windows/desktop/CoreAudio/audclnt-sessionflags-xxx-constants */

    if ((StreamFlags & AUDCLNT_STREAMFLAGS_CROSSPROCESS)
        || (StreamFlags & AUDCLNT_STREAMFLAGS_EVENTCALLBACK))
    {
        /* TODO -BAD-
         * All of those are the StreamFlags to treat. The function currently
         * treat them as invalid arguments.
         * -Anenzoras, 2019-02-25 */
        dbglog->write("    9\n");
        return E_INVALIDARG;
    }
    /* Same but win10 only */
    #ifdef AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM
        if (StreamFlags & AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM)
        {
            return E_INVALIDARG;
        }
    #endif
    #ifdef AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY
        if (StreamFlags & AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY)
        {
            return E_INVALIDARG;
        }
    #endif

    /* Contrary to the above if, the following concerns real invalid arguments,
     * because they dealt with rendering streams (we are capture).
     * -Anenzoras, 2019-02-25 */
    /* On top of it, windows 10 only for this one */
    #ifdef AUDCLNT_STREAMFLAGS_PREVENT_LOOPBACK_CAPTURE
        if (StreamFlags & AUDCLNT_STREAMFLAGS_PREVENT_LOOPBACK_CAPTURE)
        {
            return E_INVALIDARG;
        }
    #endif
    if ((StreamFlags & AUDCLNT_STREAMFLAGS_NOPERSIST)
        || (StreamFlags & AUDCLNT_STREAMFLAGS_RATEADJUST))
    {
        dbglog->write("    10\n");
        return E_INVALIDARG;
    }

    if ((StreamFlags & AUDCLNT_SESSIONFLAGS_EXPIREWHENUNOWNED)
        || (StreamFlags & AUDCLNT_SESSIONFLAGS_DISPLAY_HIDE)
        || (StreamFlags & AUDCLNT_SESSIONFLAGS_DISPLAY_HIDEWHENEXPIRED))
    {
        /* From my understanding, for those flags we have nothing to do.
         * I have doubts concerning AUDCLNT_SESSIONFLAGS_EXPIREWHENUNOWNED,
         * though.
         * -Anenzoras, 2019-02-25 */
    }

    this->shareMode = ShareMode;
    this->streamFlags = StreamFlags;
    this->pFormat = closestMatch;
    this->hnsBufferDuration = hnsBufferDuration;
    /* TODO
     * We currently ignore hnsPeriodicity because we consider the stream cannot
     * be in exclusive mode (and it's an exclusive mode only parameter).
     */
    this->hnsPeriodicity = hnsPeriodicity;
    /* TODO -DELAMERDE-
     * We currently ignore the AudioSessionGuid parameter. This is actually
     * pretty bad? Fix that as soon as possible?
     * -Anenzoras, 2019-02-25 */
    (void) AudioSessionGuid;

    /* Allocate buffer */
    UINT32 endpointBufferSize = sharedBuffer.getBufferSize();
    UINT32 requestedMinimumSize = (UINT32)(((float)this->hnsBufferDuration)
        * 1.0e-7f * sharedBuffer.bytesPerSecond);

    this->bufferSize = endpointBufferSize > requestedMinimumSize
        ? endpointBufferSize
        : requestedMinimumSize;
    this->buffer = new char[this->bufferSize];
    if (this->buffer == NULL)
    {
        /* Reset */
        this->shareMode = AUDCLNT_SHAREMODE_SHARED;
        this->streamFlags = 0;
        CoTaskMemFree(this->pFormat);
        this->hnsBufferDuration = 0;
        this->hnsPeriodicity = 0;

        return E_OUTOFMEMORY;
    }

    this->initialized = TRUE;

    dbglog->write("Exit IAudioClientEmulator::Initialize()\n");

    return S_OK;
}

void dbgPrintFormat(WAVEFORMATEX const * format)
{
    dbglog->write("    %hu\n"
        "    %hu\n"
        "    %u\n"
        "    %u\n"
        "    %hu\n"
        "    %hu\n"
        "    %hu\n",
        format->wFormatTag, format->nChannels, format->nSamplesPerSec,
        format->nAvgBytesPerSec, format->nBlockAlign, format->wBitsPerSample,
        format->cbSize);
}

/* The IsFormatSupported method indicates whether the audio endpoint device
 * supports a particular stream format. */
/* TODO: To improve */
HRESULT IAudioClientEmulator::IsFormatSupported(AUDCLNT_SHAREMODE ShareMode,
    WAVEFORMATEX const * pFormat, WAVEFORMATEX ** ppClosestMatch)
{
    dbglog->write("Enter IAudioClientEmulator::IsFormatSupported()\n");
    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    if ((pFormat == NULL)
        || ((ppClosestMatch == NULL)
            && ShareMode == AUDCLNT_SHAREMODE_SHARED))
    {
        (*ppClosestMatch) = NULL;
        return E_POINTER;
    }

    if ((ShareMode != AUDCLNT_SHAREMODE_SHARED)
        && (ShareMode != AUDCLNT_SHAREMODE_EXCLUSIVE))
    {
        (*ppClosestMatch) = NULL;
        return E_INVALIDARG;
    }

    if (!sharedBuffer.connected)
    {
        (*ppClosestMatch) = NULL;
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (ppClosestMatch == NULL)
    {
        return AUDCLNT_E_UNSUPPORTED_FORMAT;
    }

    (*ppClosestMatch) = newDefaultFormat();
    if ((*ppClosestMatch) == NULL)
    {
        return E_OUTOFMEMORY;
    }

    // dbgPrintFormat(pFormat);
    // dbgPrintFormat((*ppClosestMatch));

    if ((pFormat->wFormatTag == (*ppClosestMatch)->wFormatTag)
        && (pFormat->nChannels == (*ppClosestMatch)->nChannels)
        && (pFormat->nSamplesPerSec == (*ppClosestMatch)->nSamplesPerSec)
        && (pFormat->nAvgBytesPerSec == (*ppClosestMatch)->nAvgBytesPerSec)
        && (pFormat->nBlockAlign == (*ppClosestMatch)->nBlockAlign)
        && (pFormat->wBitsPerSample == (*ppClosestMatch)->wBitsPerSample)
        && (pFormat->cbSize == (*ppClosestMatch)->cbSize))
    {
        dbglog->write("Exit IAudioClientEmulator::IsFormatSupported() S_OK\n");
        return S_OK;
    }
    else
    {
        dbglog->write("Exit IAudioClientEmulator::IsFormatSupported() S_FALSE\n");
        return S_FALSE;
    }
}

/* The Reset method resets the audio stream.
 *
 *  @returns {HRESULT}
 *      S_FALSE if the method succeeds and the stream was already reset.
 */
HRESULT IAudioClientEmulator::Reset()
{
    dbglog->write("Enter IAudioClientEmulator::Reset()\n");
    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (this->started)
    {
        return AUDCLNT_E_NOT_STOPPED;
    }

    if (this->polled)
    {
        return AUDCLNT_E_BUFFER_OPERATION_PENDING;
    }

    this->totalFramesSinceLastReset = 0;
    this->reset = TRUE;

    dbglog->write("Exit IAudioClientEmulator::Reset()\n");

    return S_OK;
}

/* The SetEventHandle method sets the event handle that the system signals when
 * an audio buffer is ready to be processed by the client. */
HRESULT IAudioClientEmulator::SetEventHandle(HANDLE eventHandle)
{
    dbglog->write("Enter IAudioClientEmulator::SetEventHandle()\n");
    /* See note 0 on top of the file concerning AUDCLNT_E_SERVICE_NOT_RUNNING */
    if (eventHandle == NULL)
    {
        /* We have no real way of dealing with handle validity, so forget about
         * a check on that.
         * -Anenzoras, 2019-02-27 */
        return E_INVALIDARG;
    }

    if (!(this->streamFlags & AUDCLNT_STREAMFLAGS_EVENTCALLBACK))
    {
        return AUDCLNT_E_EVENTHANDLE_NOT_EXPECTED;
    }

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    this->eventHandle = eventHandle;

    dbglog->write("Exit IAudioClientEmulator::SetEventHandle()\n");

    return S_OK;
}

/* The Start method starts the audio stream. */
HRESULT IAudioClientEmulator::Start()
{
    dbglog->write("Enter IAudioClientEmulator::Start()\n");
    /* AUDCLNT_E_SERVICE_NOT_RUNNING: see note 0 on top of file */

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (this->started)
    {
        return AUDCLNT_E_NOT_STOPPED;
    }

    if ((this->streamFlags & AUDCLNT_STREAMFLAGS_EVENTCALLBACK)
        && this->eventHandle == NULL)
    {
        return AUDCLNT_E_EVENTHANDLE_NOT_SET;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    this->reset = FALSE;
    this->started = TRUE;

    dbglog->write("Exit IAudioClientEmulator::Start()\n");

    return S_OK;
}

/* The Stop method stops the audio stream. */
HRESULT IAudioClientEmulator::Stop()
{
    dbglog->write("Enter IAudioClientEmulator::Stop()\n");
    /* AUDCLNT_E_SERVICE_NOT_RUNNING: See note 0 on top of file */

    if (!this->started)
    {
        return S_FALSE;
    }

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    this->started = FALSE;

    dbglog->write("Exit IAudioClientEmulator::Stop()\n");

    return S_OK;
}

/*
 * IAudioCaptureClient stuff
 */
/* Retrieves a pointer to the next available packet of data in the capture
 * endpoint buffer. */
HRESULT IAudioClientEmulator::GetBuffer(BYTE ** ppData, UINT32 * pNumFramesToRead,
    DWORD * pdwFlags, UINT64 * pu64DevicePosition,
    UINT64 * pu64QPCPosition)
{
    dbglog->write("Enter IAudioClientEmulator::GetBuffer()\n");
    /* AUDCLNT_E_SERVICE_NOT_RUNNING: see note 0 on top of the file */

    if (this->polled)
    {
        return AUDCLNT_E_OUT_OF_ORDER;
    }

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (this->reset)
    {
        return AUDCLNT_E_BUFFER_OPERATION_PENDING;
    }

    if ((ppData == NULL)
        || (pNumFramesToRead == NULL)
        || (pdwFlags == NULL))
    {
        return E_POINTER;
    }

    if (this->buffer == NULL)
    {
        /* Since we will pass a pointer to our internal buffer, we need to check
        * if nobody did a funny thing to it. */
        return E_FAIL;
    }

    /* All error checks done */

    /* The case in which we retransmit the same packet */
    if (this->frameCount != 0)
    {
        this->polled = TRUE;

        return S_OK;
    }

    /* Our way of telling if there's things to read. */
    static float tmp_bufferOffset = 0;

    float currentOffset = sharedBuffer.getOffset();

    if (pu64DevicePosition != NULL)
    {
        (*pu64DevicePosition) = this->totalFramesSinceLastReset;
    }

    if (pu64QPCPosition != NULL)
    {
        (*pu64QPCPosition) = getPerformanceCounter();
    }

    /* TODO: Treat flags AUDCLNT_BUFFERFLAGS_DATA_DISCONTINUITY and
     * AUDCLNT_BUFFERFLAGS_SILENT.
     * I believe we will not have AUDCLNT_BUFFERFLAGS_TIMESTAMP_ERROR but I
     * might be wrong */
    (*pdwFlags) = 0;

    /* Case where there's nothing to read */
    if (currentOffset >= tmp_bufferOffset)
    {
        this->frameCount = 0;
        (*pNumFramesToRead) = 0;

        return AUDCLNT_S_BUFFER_EMPTY;
    }
    else
    {
        HRESULT res;
        SharedAudioBuffer::Status status;

        tmp_bufferOffset = currentOffset;

        res = this->GetNextPacketSize(&this->frameCount);
        if (res != S_OK)
        {
            return res;
        }

        /* Get volume info */
        float * volumes = NULL;
        float masterVolume = 1.0f;
        if (this->device != NULL)
        {
            volumes = this->device->sessionInfo.volumes;
            masterVolume = this->device->sessionInfo.masterVolume;
        }

        status = sharedBuffer.readBuffer(this->buffer, masterVolume, volumes);
        if (status != SharedAudioBuffer::SUCCESS)
        {
            this->frameCount = 0;
            (*ppData) = NULL;

            return AUDCLNT_E_BUFFER_ERROR;
        }

        (*pNumFramesToRead) = this->frameCount;
        (*ppData) = (BYTE *)this->buffer;

        this->polled = true;

        return S_OK;
    }

    dbglog->write("Exit IAudioClientEmulator::GetBuffer()\n");

    return S_OK;
}

/* The GetNextPacketSize method retrieves the number of frames in the next data
 * packet in the capture endpoint buffer. */
HRESULT IAudioClientEmulator::GetNextPacketSize(UINT32 * pNumFramesInNextPacket)
{
    dbglog->write("Enter IAudioClientEmulator::GetNextPacketSize()\n");
    /* AUDCLNT_E_SERVICE_NOT_RUNNING: see note 0 on top of file */

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if (this->shareMode & AUDCLNT_SHAREMODE_EXCLUSIVE)
    {
        return AUDCLNT_E_EXCLUSIVE_MODE_NOT_ALLOWED;
    }

    if (pNumFramesInNextPacket == NULL)
    {
        return E_POINTER;
    }

    if ((!this->polled) && (this->frameCount != 0))
    {
        (*pNumFramesInNextPacket) = this->frameCount;
    }
    else
    {
        /* TODO */
        (*pNumFramesInNextPacket) = sharedBuffer.getBufferSize();
    }

    dbglog->write("Exit IAudioClientEmulator::GetNextPacketSize()\n");

    return S_OK;
}

/* The ReleaseBuffer method releases the buffer. */
HRESULT IAudioClientEmulator::ReleaseBuffer(UINT32 NumFramesRead)
{
    dbglog->write("Enter IAudioClientEmulator::ReleaseBuffer()\n");
    /* AUDCLNT_E_SERVICE_NOT_RUNNING: see note 0 on top of file */

    if (!sharedBuffer.connected)
    {
        return AUDCLNT_E_DEVICE_INVALIDATED;
    }

    if (!this->initialized)
    {
        return AUDCLNT_E_NOT_INITIALIZED;
    }

    if ((NumFramesRead != 0)
        && (NumFramesRead != this->frameCount))
    {
        return AUDCLNT_E_INVALID_SIZE;
    }

    if (!this->polled)
    {
        if (NumFramesRead == 0)
        {
            return S_OK;
        }

        return AUDCLNT_E_OUT_OF_ORDER;
    }

    if (NumFramesRead != 0)
    {
        this->frameCount = 0;
        this->totalFramesSinceLastReset += this->frameCount;
    }

    this->polled = FALSE;

    dbglog->write("Exit IAudioClientEmulator::ReleaseBuffer()\n");

    return S_OK;
}
