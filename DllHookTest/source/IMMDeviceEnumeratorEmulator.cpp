/*
 *  File:   IMMDeviceEnumeratorEmulator.hpp
 *  Author: Anenzoras
 *  Date:   2019-02-12
 *
 *  Brief:  Definition of the fake class that will proxy an IMMDeviceEnumerator
 *  object after the call to the fake CoCreateInstance function.
 */

/* Project includes */
#include "IMMDeviceEnumeratorEmulator.hpp"
#include "IMMDeviceCollectionEmulator.hpp"
#include "IMMDeviceEmulator.hpp"

IID const IID_IMMDeviceEnumerator = __uuidof(IMMDeviceEnumerator);

/*
 * IMMDeviceEnumeratorEmulator functions
 */

/* Returns an instance of IMMDeviceEnumeratorEmulator to encapsulate
 * thingies. */
/* TODO safety checks */
IMMDeviceEnumeratorEmulator * IMMDeviceEnumeratorEmulator::CreateInstance(
    IMMDeviceEnumerator * original)
{
    IMMDeviceEnumeratorEmulator * proxy;

    proxy = new IMMDeviceEnumeratorEmulator(original);
    if (proxy == NULL)
    {
        return NULL;
    }

    proxy->AddRef();

    return proxy;
}

/* Destructor. Releases its reference to the original object. */
IMMDeviceEnumeratorEmulator::~IMMDeviceEnumeratorEmulator()
{
    this->original->Release();
}

/* Constructor. Private for reasons. */
IMMDeviceEnumeratorEmulator::IMMDeviceEnumeratorEmulator(
    IMMDeviceEnumerator * original)
{
    this->original = original;
    this->original->AddRef();
    this->referenceCount = 0;
}

/*
 * IMMDeviceEnumerator functions
 */

/* The EnumAudioEndpoints method generates a collection of audio endpoint
 * devices that meet the specified criteria.*/
HRESULT IMMDeviceEnumeratorEmulator::EnumAudioEndpoints(EDataFlow dataFlow,
    DWORD dwStateMask,
    IMMDeviceCollection ** ppDevices)
{
    HRESULT status = S_OK;
    IMMDeviceCollection * originalCollection = NULL;

    /* First we got our original collection for proxying purposes and most error
     * checks */
    status = this->original->EnumAudioEndpoints(dataFlow, dwStateMask,
        &originalCollection);
    if (status != S_OK)
    {
        return status;
    }

    /* If the user does not seek a capture device, return the original
     * collection */
    if (dataFlow == eRender)
    {
        (*ppDevices) = originalCollection;

        return status;
    }
    else
    {
        /* Now we create our proxy around it */
        (*ppDevices) = IMMDeviceCollectionEmulator::CreateInstance(originalCollection);
        originalCollection->Release();
        if (ppDevices == NULL)
        {
            return E_OUTOFMEMORY;
        }

        return S_OK;
    }
}

/* The GetDefaultAudioEndpoint method retrieves the default audio
 * endpoint for the specified data-flow direction and role. */
HRESULT IMMDeviceEnumeratorEmulator::GetDefaultAudioEndpoint(EDataFlow dataFlow,
    ERole role,
    IMMDevice ** ppEndpoint)
{
    return this->original->GetDefaultAudioEndpoint(dataFlow, role, ppEndpoint);
}

/* The GetDevice method retrieves an audio endpoint device that is identified
 * by an endpoint ID string. */
HRESULT IMMDeviceEnumeratorEmulator::GetDevice(LPCWSTR pwstrId,
    IMMDevice ** ppDevice)
{
    if ((pwstrId == NULL) || (ppDevice == NULL))
    {
        return E_POINTER;
    }

    if (wcscmp(IMMDeviceEmulator_LDEVICE_ID, pwstrId) != 0)
    {
        return this->original->GetDevice(pwstrId, ppDevice);
    }
    else
    {
        (*ppDevice) = IMMDeviceEmulator::CreateInstance();
        if ((*ppDevice) == NULL)
        {
            return E_OUTOFMEMORY;
        }

        return S_OK;
    }
}

/* The RegisterEndpointNotificationCallback method registers a client's
 * notification callback interface. */
HRESULT IMMDeviceEnumeratorEmulator::RegisterEndpointNotificationCallback(
    IMMNotificationClient * pClient)
{
    return this->original->RegisterEndpointNotificationCallback(pClient);
}

/* The UnregisterEndpointNotificationCallback method deletes the registration
 * of a notification interface that the client registered in a previous call to
 * the IMMDeviceEnumerator::RegisterEndpointNotificationCallback method. */
HRESULT IMMDeviceEnumeratorEmulator::UnregisterEndpointNotificationCallback(
    IMMNotificationClient * pClient)
{
    return this->original->UnregisterEndpointNotificationCallback(pClient);
}

/*
 * IUnknown stuff
 */
/* Increments the reference count for an interface on an object. This method
 * should be called for every new copy of a pointer to an interface on an
 * object. */
ULONG IMMDeviceEnumeratorEmulator::AddRef()
{
    InterlockedIncrement(&this->referenceCount);

    return this->referenceCount;
}

/* Decrements the reference count for an interface on an object. */
ULONG IMMDeviceEnumeratorEmulator::Release()
{
    ULONG ref = InterlockedDecrement(&this->referenceCount);
    if (this->referenceCount == 0)
    {
        /* End of life */
        delete this;
    }

    return ref;
}

/* Retrieves pointers to the supported interfaces on an object. */
HRESULT IMMDeviceEnumeratorEmulator::QueryInterface(REFIID riid,
    void ** ppvObject)
{
    if (ppvObject == NULL)
    {
        return E_POINTER;
    }
    *ppvObject = NULL;

    if (riid == IID_IUnknown
        || riid == IID_IMMDeviceEnumerator)
    {
        /* Increment the reference count and return the pointer. */
        *ppvObject = (LPVOID)this;
        this->AddRef();

        return S_OK;
    }
    else
    {
        /* We go agains one rule of QueryInterface doing that but not doing
         * that might cause unsuspected problems in windows internals. */
        return original->QueryInterface(riid, ppvObject);
    }
}
