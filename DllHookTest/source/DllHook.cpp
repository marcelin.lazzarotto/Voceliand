/*
 *  File:   DllHook.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-04
 *
 *  Brief:  The code of the DLL to load into other programs.
 */


/* Standard library includes */
#include <cstdint>

/* External library includes */
#include "MinHook/include/MinHook.h"

/* Project includes */
#include "Log.hpp"
#include "IMMDeviceEnumeratorEmulator.hpp"
#include "IMMDeviceCollectionEmulator.hpp"
#include "IMMDeviceEmulator.hpp"

CLSID const CLSID_MMDeviceEnumerator = __uuidof(MMDeviceEnumerator);

/* Shortcut for CoCreateInstance type */
typedef HRESULT (WINAPI * CoCreateInstance_f)(REFCLSID, LPUNKNOWN, DWORD, REFIID, LPVOID*);

/* Pointer for calling original CoCreateInstance. */
CoCreateInstance_f originalCoCreateInstance = NULL;

/* Proxy function */
HRESULT WINAPI proxyCoCreateInstance(REFCLSID rclsid,
    LPUNKNOWN pUnkOuter, DWORD dwClsContext, REFIID riid, LPVOID* ppv)
{
    if (rclsid == CLSID_MMDeviceEnumerator)
    {
        HRESULT status = NOERROR;

        IMMDeviceEnumerator * original = NULL;

        status = originalCoCreateInstance(rclsid, pUnkOuter, dwClsContext, riid,
            (LPVOID*)&original);

        if (FAILED(status))
        {
            return status;
        }

        *ppv = (LPVOID)IMMDeviceEnumeratorEmulator::CreateInstance(original);
        /* The CreateInstance function above result in incrementing the ref
         * counter of the original object, so we can release it mindlessly. */
        original->Release();

        if ((*ppv) == NULL)
        {
            return E_OUTOFMEMORY;
        }

        return S_OK;
    }
    else
    {
        return originalCoCreateInstance(rclsid, pUnkOuter, dwClsContext, riid, ppv);
    }
}

/* Ugly ass globals so that we can safely free thingies */
bool mhinitialized = FALSE;
bool mhhooked = FALSE;

/**
 *  @function logMHStatus
 *      A more user friendly print of MH_* status.
 *
 *  @param {MH_STATUS} status
 *      The status to write in the log file.
 *  @param {uint32_t} tabLevel
 *      Number of tab (4-space) to insert before.
 */
void logMHStatus(MH_STATUS status, uint32_t tabLevel)
{
    for (uint32_t i = 0; i < tabLevel; i++)
    {
        dbglog->write("    ");
    }

    switch (status)
    {
        case MH_UNKNOWN:
            dbglog->write("Unknown error. Should not be returned.\n");
            break;

        case MH_OK:
            dbglog->write("Successful.\n");
            break;

        case MH_ERROR_ALREADY_INITIALIZED:
            dbglog->write("MinHook is already initialized.\n");
            break;

        case MH_ERROR_NOT_INITIALIZED:
            dbglog->write("MinHook is not initialized yet, or already"
                " uninitialized.\n");
            break;

        case MH_ERROR_ALREADY_CREATED:
            dbglog->write("The hook for the specified target function is"
                " already created.\n");
            break;

        case MH_ERROR_NOT_CREATED:
            dbglog->write("The hook for the specified target function is"
                " not created yet.\n");
            break;

        case MH_ERROR_ENABLED:
            dbglog->write("The hook for the specified target function is"
                " already enabled.\n");
            break;

        case MH_ERROR_DISABLED:
            dbglog->write("The hook for the specified target function is"
                " not enabled yet, or already disabled.\n");
            break;

        case MH_ERROR_NOT_EXECUTABLE:
            dbglog->write("The specified pointer is invalid. It points the"
                " address of non-allocated and/or non-executable region.\n");
            break;

        case MH_ERROR_UNSUPPORTED_FUNCTION:
            dbglog->write("The specified target function cannot be"
                " hooked.\n");
            break;

        case MH_ERROR_MEMORY_ALLOC:
            dbglog->write("Failed to allocate memory.\n");
            break;

        case MH_ERROR_MEMORY_PROTECT:
            dbglog->write("Failed to change the memory protection.\n");
            break;

        case MH_ERROR_MODULE_NOT_FOUND:
            dbglog->write("The specified module is not loaded.\n");
            break;

        case MH_ERROR_FUNCTION_NOT_FOUND:
            dbglog->write("The specified function is not found.\n");
            break;

        default:
            dbglog->write("Okay 'faut qu'on m'explique\n");
            break;
    }
}

/**
 *  @function initialize
 *      Initialize hooking.
 *
 *  @returns {bool}
 *      FALSE if an error occured, preventing initialization.
 *      TRUE if all went fine.
 */
bool initialize(void)
{
    /* Init log */
    {
        HMODULE hm = NULL;
        BOOL result;
        char const SEPARATOR = '\\';
        char dllPath[MAX_PATH] = {0};
        char filename[MAX_PATH] = {0};
        size_t i = 0;
        DWORD stringSize;

        /* Allows us to retrieve the hHandle */
        result = GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
            GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
            (LPCSTR) &initialize, &hm);
        if (result == 0)
        {
            return FALSE;
        }

        stringSize = GetModuleFileNameA(hm, dllPath, MAX_PATH);
        if (stringSize == 0)
        {
            return FALSE;
        }

        /* Truncate path to remove exec name */
		i = stringSize - 1;
		while ((i > 0) && (dllPath[i] != SEPARATOR))
		{
			i--;
		}

        if (dllPath[i] != SEPARATOR)
        {
            return FALSE;
        }

        stringSize = i;
        dllPath[i] = '\0';

        strcpy(filename, dllPath);
        strcat(filename, "\\logfile.txt");

        dbglog = new Log(filename, "wt");
        if (dbglog->file == NULL)
        {
            delete dbglog;

            return FALSE;
        }
    }

    MH_STATUS status = MH_OK;

    status = MH_Initialize();
    if (status != MH_OK)
    {
        dbglog->write("[initialize()] Error in MH_Initialize()\n");
        logMHStatus(status, 1);
        return FALSE;
    }

    mhinitialized = TRUE;

    /* Hook CoCreateInstance so that we can proxy our interface */
    status = MH_CreateHook(reinterpret_cast<LPVOID>(&CoCreateInstance),
        reinterpret_cast<LPVOID>(&proxyCoCreateInstance),
        reinterpret_cast<LPVOID*>(&originalCoCreateInstance));
    if (status != MH_OK)
    {
        dbglog->write("[initialize()] Error in MH_CreateHook\n");
        logMHStatus(status, 1);
        return FALSE;
    }

    /* Enable the hook for CoCreateInstance */
    status = MH_EnableHook(reinterpret_cast<LPVOID>(&CoCreateInstance));
    if (status != MH_OK)
    {
        dbglog->write("[initialize()] Error in MH_EnableHook\n");
        logMHStatus(status, 1);
        return FALSE;
    }

    mhhooked = TRUE;

    return TRUE;
}

/**
 *  @function destroy
 *      Unset everything to cleanly unload DLL.
 *
 *  @returns {bool}
 *      FALSE if an error occured.
 *      TRUE if everything went fine.
 */
bool destroy(void)
{
    MH_STATUS status = MH_OK;

    if (dbglog)
    {
        delete dbglog;
    }

    /* Disable the hook for CoCreateInstance */
    if (mhhooked)
    {
        status = MH_DisableHook(reinterpret_cast<LPVOID>(&CoCreateInstance));
        if (status != MH_OK)
        {
            dbglog->write("[destroy()] Error in MH_DisableHook\n");
            logMHStatus(status, 1);
        }

        mhhooked = FALSE;
    }

    if (mhinitialized)
    {
        status = MH_Uninitialize();
        if (status != MH_OK)
        {
            dbglog->write("[destroy()] Error in MH_Uninitialize\n");
            logMHStatus(status, 1);
        }

        mhinitialized = FALSE;
    }

    return TRUE;
}

/**
 *  @function DllMain
 *      The function called upon loading DLL into another program.
 *
 *  @param {HINSTANCE} hinstDLL
 *      A handle to the DLL module. The value is the base address of the DLL.
 *      The HINSTANCE of a DLL is the same as the HMODULE of the DLL, so
 *      hinstDLL can be used in calls to functions that require a module handle.
 *  @param {DWORD} fdwReason
 *      The reason code that indicates why the DLL entry-point function is being
 *      called. This parameter can be one of the following values:
 *      - DLL_PROCESS_ATTACH (== 1): The DLL is being loaded into the virtual
 *      address space of the current process as a result of the process starting
 *      up or as a result of a call to LoadLibrary. DLLs can use this
 *      opportunity to initialize any instance data or to use the TlsAlloc
 *      function to allocate a thread local storage (TLS) index.
 *      The lpReserved parameter indicates whether the DLL is being loaded
 *      statically or dynamically.
 *      - DLL_PROCESS_DETACH (== 0): The DLL is being unloaded from the virtual
 *      address space of the calling process because it was loaded
 *      unsuccessfully or the reference count has reached zero (the processes
 *      has either terminated or called FreeLibrary one time for each time it
 *      called LoadLibrary).
 *      The lpReserved parameter indicates whether the DLL is being unloaded as
 *      a result of a FreeLibrary call, a failure to load, or process
 *      termination.
 *      The DLL can use this opportunity to call the TlsFree function to free
 *      any TLS indices allocated by using TlsAlloc and to free any thread local
 *      data.
 *      Note that the thread that receives the DLL_PROCESS_DETACH notification
 *      is not necessarily the same thread that received the DLL_PROCESS_ATTACH
 *      notification.
 *      - DLL_THREAD_ATTACH (== 2): The current process is creating a new
 *      thread. When this occurs, the system calls the entry-point function of
 *      all DLLs currently attached to the process. The call is made in the
 *      context of the new thread. DLLs can use this opportunity to initialize a
 *      TLS slot for the thread. A thread calling the DLL entry-point function
 *      with DLL_PROCESS_ATTACH does not call the DLL entry-point function with
 *      DLL_THREAD_ATTACH.
 *      Note that a DLL's entry-point function is called with this value only by
 *      threads created after the DLL is loaded by the process. When a DLL is
 *      loaded using LoadLibrary, existing threads do not call the entry-point
 *      function of the newly loaded DLL.
 *      - DLL_THREAD_DETACH (== 3): A thread is exiting cleanly. If the DLL has
 *      stored a pointer to allocated memory in a TLS slot, it should use this
 *      opportunity to free the memory. The system calls the entry-point
 *      function of all currently loaded DLLs with this value. The call is made
 *      in the context of the exiting thread.
 *  @param {LPVOID} lpvReserved
 *      If fdwReason is DLL_PROCESS_ATTACH, lpvReserved is NULL for dynamic
 *      loads and non-NULL for static loads.
 *      If fdwReason is DLL_PROCESS_DETACH, lpvReserved is NULL if FreeLibrary
 *      has been called or the DLL load failed and non-NULL if the process is
 *      terminating.
 *
 *  @returns {BOOL}
 *      When the system calls the DllMain function with the DLL_PROCESS_ATTACH
 *      value, the function returns TRUE if it succeeds or FALSE if
 *      initialization fails. If the return value is FALSE when DllMain is
 *      called because the process uses the LoadLibrary function, LoadLibrary
 *      returns NULL. (The system immediately calls your entry-point function
 *      with DLL_PROCESS_DETACH and unloads the DLL.) If the return value is
 *      FALSE when DllMain is called during process initialization, the process
 *      terminates with an error. To get extended error information, call
 *      GetLastError.
 *      When the system calls the DllMain function with any value other than
 *      DLL_PROCESS_ATTACH, the return value is ignored.
 *
 *  @note
 *      Documentation taken from
 *      https://docs.microsoft.com/fr-fr/windows/desktop/Dlls/dllmain
 *
 *  @warning
 *      This currently does not follow guidelines from
 *      https://docs.microsoft.com/fr-fr/windows/desktop/Dlls/dynamic-link-library-best-practices.
 */
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    (void) hinstDLL;
    (void) lpvReserved;
    (void) fdwReason;

    BOOL res = TRUE;

    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            res = initialize();
            if (dbglog)
            {
                dbglog->write("In DLL_PROCESS_ATTACH\n");
            }
            break;
        case DLL_PROCESS_DETACH:
            if (dbglog)
            {
                dbglog->write("In DLL_PROCESS_DETACH\n");
            }
            res = destroy();
            break;
        case DLL_THREAD_ATTACH:
            if (dbglog)
            {
                dbglog->write("DLL_THREAD_ATTACH not covered yet\n");
            }
            break;
        case DLL_THREAD_DETACH:
            if (dbglog)
            {
                dbglog->write("DLL_THREAD_DETACH not covered yet\n");
            }
            break;
        default:
            res = FALSE;
            break;
    }

    return res;
}
