/*
 *  File:   Log.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-12
 *
 *  Brief:  Implementation for a small logging class, for debug purpose.
 */

/* Standard library includes */
#include <cstdio>   /* fprintf(), fflush() */
#include <cstdarg>  /* va_list, va_start(), va_end() */

/* Project includes */
#include "Log.hpp"

Log * dbglog;

Log::Log(char const * filename, char const * mode)
{
    this->file = fopen(filename, mode);
}

Log::~Log(void)
{
    if (this->file)
    {
        fclose(file);
        this->file = NULL;
    }
}

/* Wrapper around fprintf and fflush for file object. */
void Log::write(const char * format, ...)
{
    va_list parameters;
    va_start(parameters, format);

    vfprintf(this->file, format, parameters);
    fflush(this->file);

    va_end(parameters);
}
