/*
 *  File:   IMMDeviceCollectionEmulator.cpp
 *  Author: Anenzoras
 *  Date:   2019-02-13
 *
 *  Brief:  Implementation of the class proxying an IMMDeviceCollection
 *      interface.
 */

/* Project includes */
#include "IMMDeviceCollectionEmulator.hpp"
#include "IMMDeviceEmulator.hpp"

IID const IID_IMMDeviceCollection = __uuidof(IMMDeviceCollection);

/*
 * IMMDeviceCollectionEmulator stuff
 */

/* Returns an instance of IMMDeviceCollectionEmulator to encapsulate
 * thingies. */
/* TODO safety checks */
IMMDeviceCollectionEmulator * IMMDeviceCollectionEmulator::CreateInstance(
    IMMDeviceCollection * original)
{
    IMMDeviceCollectionEmulator * proxy;

    proxy = new IMMDeviceCollectionEmulator(original);
    if (proxy == NULL)
    {
        return NULL;
    }

    proxy->AddRef();

    return proxy;
}

/* Destructor. Releases its reference to the original object. */
IMMDeviceCollectionEmulator::~IMMDeviceCollectionEmulator()
{
    this->original->Release();
}

/* Constructor. Private for reasons. */
IMMDeviceCollectionEmulator::IMMDeviceCollectionEmulator(
    IMMDeviceCollection * original)
{
    this->original = original;
    this->original->AddRef();
    this->referenceCount = 0;
}

/*
 * IMMDeviceCollection stuff
 */
/* The GetCount method retrieves a count of the devices in the device
 * collection. */
HRESULT IMMDeviceCollectionEmulator::GetCount(UINT * pcDevices)
{
    HRESULT status = S_OK;

    /* Original will perform error checks for us */
    status = this->original->GetCount(pcDevices);
    if (status != S_OK)
    {
        return status;
    }

    /* Faking things out, we say we got one more. */
    (*pcDevices)++;

    return S_OK;
}

/* The Item method retrieves a pointer to the specified item in the device
 * collection. */
HRESULT IMMDeviceCollectionEmulator::Item(UINT nDevice, IMMDevice ** ppDevice)
{
    HRESULT status = S_OK;
    UINT trueCount = 0;

    status = this->original->GetCount(&trueCount);
    if (status != S_OK)
    {
        return status;
    }

    /* Faking things out, we say our device is the last one of the list */
    if (nDevice == trueCount)
    {
        /* Check pointer */
        if (ppDevice == NULL)
        {
            return E_POINTER;
        }

        (*ppDevice) = IMMDeviceEmulator::CreateInstance();
        if ((*ppDevice) == NULL)
        {
            return E_OUTOFMEMORY;
        }

        return S_OK;
    }
    else
    {
        return this->original->Item(nDevice, ppDevice);
    }
}

/*
 * IUnknown stuff
 */
/* Increments the reference count for an interface on an object. This method
 * should be called for every new copy of a pointer to an interface on an
 * object. */
ULONG IMMDeviceCollectionEmulator::AddRef()
{
    InterlockedIncrement(&this->referenceCount);

    return this->referenceCount;
}

/* Decrements the reference count for an interface on an object. */
ULONG IMMDeviceCollectionEmulator::Release()
{
    ULONG ref = InterlockedDecrement(&this->referenceCount);
    if (this->referenceCount == 0)
    {
        /* End of life */
        delete this;
    }

    return ref;
}

/* Retrieves pointers to the supported interfaces on an object. */
HRESULT IMMDeviceCollectionEmulator::QueryInterface(REFIID riid,
    void ** ppvObject)
{
    if (ppvObject == NULL)
    {
        return E_POINTER;
    }
    *ppvObject = NULL;

    if (riid == IID_IUnknown
        || riid == IID_IMMDeviceCollection)
    {
        /* Increment the reference count and return the pointer. */
        *ppvObject = (LPVOID)this;
        this->AddRef();

        return S_OK;
    }
    else
    {
        /* We go agains one rule of QueryInterface doing that but not doing
         * that might cause unsuspected problems in windows internals. */
        return original->QueryInterface(riid, ppvObject);
    }
}
