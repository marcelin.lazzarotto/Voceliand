/*
 *  File:   Time.hpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

#ifndef VOCELIAND_TIME_HPP
#define VOCELIAND_TIME_HPP

/* standard library include */
#include <inttypes.h>

/* external library include */
/* project include */

namespace Voceliand
{
    /* Mostly taken from SFML code.
    https://github.com/SFML/SFML/blob/master/include/SFML/System/Time.hpp
    */
    /**
     *  @class Time
     *      Represent a time value in a consistant way.
     *      Also adds a few manipulation operators to ease things up.
     */
    class Time
    {
    public:
        /**
         *  @property [static] {Time const} ZERO
         *      Predefined "zero" time value.
         */
        static Time const ZERO;

        /**
         *  @function [static] inSeconds
         *      Returns a Time object using a value in seconds.
         *
         *      @param {} {
         *      loat} seconds.
         *      The time in seconds.
         *
         *      @returnss {}
         *      {Time}.
         *      The time asked for.
         */
        static Time inSeconds(float seconds);

        /**
         *  @function [static] inMilliseconds
         *      Returns a Time object using a value in milliseconds.
         *
         *      @param {} {
         *      nt32_t} milliseconds.
         *      The time in milliseconds.
         *
         *      @returnss {}
         *      {Time}.
         *      The time asked for.
         */
        static Time inMilliseconds(int32_t milliseconds);

        /**
         *  @function [static] inMicroseconds
         *      Returns a Time object using a value in microseconds.
         *
         *      @param {} {
         *      loat} microseconds.
         *      The time in microseconds.
         *
         *      @returnss {}
         *      {Time}.
         *      The time asked for.
         */
        static Time inMicroseconds(int64_t microseconds);

        /**
         *  @function Time
         *      Default constructor.
         *      Sets the time value to zero.
         */
        Time(void);

        /**
         *  @function asSeconds
         *      Return the time value as a number of seconds.
         *
         *  @returns {float}
         *      Time in seconds.
         */
        float asSeconds(void) const;

        /**
         *  @function asMilliseconds
         *      Return the time value as a number of milliseconds.
         *
         *  @returns {int32_t}
         *      Time in milliseconds.
         */
        int32_t asMilliseconds(void) const;

        /**
         *  @function asMicroseconds
         *      Return the time value as a number of microseconds.
         *
         *  @returns {int64_t}
         *      Time in microseconds.
         */
        int64_t asMicroseconds(void) const;

        /**
         *  @operator ==
         *      Overload of == operator to compare two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {bool}
         *      True if both time values are equal.
         */
        bool operator ==(Time const & right) const;

        /**
         *  @operator !=
         *      Overload of != operator to compare two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {bool}
         *      True if both time values are different.
         */
        bool operator !=(Time const & right) const;

        /**
         *  @operator <
         *      Overload of < operator to compare two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {bool}
         *      True if this is lesser than right.
         */
        bool operator <(Time const & right) const;

        /**
         *  @operator >
         *      Overload of > operator to compare two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {bool}
         *      True if this is greater than right.
         */
        bool operator >(Time const & right) const;

        /**
         *  @operator <=
         *      Overload of <= operator to compare two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {bool}
         *      True if this is lesser or equal than right.
         */
        bool operator <=(Time const & right) const;

        /**
         *  @operator >=
         *      Overload of >= operator to compare two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {bool}
         *      True if this is greater or equal than right.
         */
        bool operator >=(Time const & right) const;

        /**
         *  @operator -
         *      Overload of unary - operator to negate a time value.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time}
         *      Opposite of the time value.
         */
        Time operator -(void) const;

        /**
         *  @operator +
         *      Overload of binary + operator to add two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time}
         *      Sum of the two times values.
         */
        Time operator +(Time const & right) const;

        /**
         *  @operator -
         *      Overload of binary - operator to subtract two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time}
         *      Difference of the two times values.
         */
        Time operator -(Time const & right) const;

        /**
         *  @operator *
         *      Overload of binary * operator to scale a time value.
         *
         *  @param {float const} right
         *      Right operand (a number).
         *
         *  @returns {Time}
         *      this multiplied by right.
         */
        Time operator *(float const right) const;

        /**
         *  @operator *
         *      Overload of binary * operator to scale a time value.
         *
         *  @param {int64_t const} right
         *      Right operand (a number).
         *
         *  @returns {Time}
         *      this multiplied by right.
         */
        Time operator *(int64_t const right) const;

        /**
         *  @operator *
         *      Overload of binary * operator to scale a time value.
         *
         *  @param {float const} left
         *      Left operand (a number).
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time}
         *      this multiplied by right.
         */
        friend Time operator *(float const left, Time const & right);

        /**
         *  @operator *
         *      Overload of binary * operator to scale a time value.
         *
         *  @param {int64_t const} left
         *      Left operand (a number).
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time}
         *      this multiplied by right.
         */
        friend Time operator *(int64_t const left, Time const & right);

        /**
         *  @operator /
         *      Overload of binary / operator to scale a time value.
         *
         *  @param {float const} right
         *      Right operand (a number).
         *
         *  @returns {Time}
         *      this divided by right.
         */
        Time operator /(float const right) const;

        /**
         *  @operator /
         *      Overload of binary / operator to scale a time value.
         *
         *  @param {int64_t const} right
         *      Right operand (a number).
         *
         *  @returns {Time}
         *      this divided by right.
         */
        Time operator /(int64_t const right) const;

        /**
         *  @operator /
         *      Overload of binary / operator to compute the ratio of two time
         *      values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {float}
         *      this divided by right.
         */
        float operator /(Time const & right) const;

        /**
         *  @operator %
         *      Overload of binary % operator to compute remainder of a time
         *      value.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time}
         *      this modulo right.
         */
        Time operator %(Time const & right) const;

        /**
         *  @operator +=
         *      Overload of binary += operator to add/assign two time values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time &}
         *      reference to this as this of the two times values.
         */
        Time & operator +=(Time const & right);

        /**
         *  @operator -=
         *      Overload of binary -= operator to subtract/assign two time
         *      values.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time &}
         *      reference to this as this of the two times values.
         */
        Time & operator -=(Time const & right);

        /**
         *  @operator *=
         *      Overload of binary *= operator to scale/assign a time value.
         *
         *  @param {float const} right
         *      Right operand (a number).
         *
         *  @returns {Time &}
         *      reference to this as this multiplied by right.
         */
        Time & operator *=(float const right);

        /**
         *  @operator *=
         *      Overload of binary *= operator to scale/assign a time value.
         *
         *  @param {int64_t const} right
         *      Right operand (a number).
         *
         *  @returns {Time &}
         *      reference to this as this multiplied by right.
         */
        Time & operator *=(int64_t const right);

        /**
         *  @operator /=
         *      Overload of binary /= operator to scale/assign a time value.
         *
         *  @param {float const} right
         *      Right operand (a number).
         *
         *  @returns {Time &}
         *      reference to this as this divided by right.
         */
        Time & operator /=(float const right);

        /**
         *  @operator /=
         *      Overload of binary /= operator to scale/assign a time value.
         *
         *  @param {int64_t const} right
         *      Right operand (a number).
         *
         *  @returns {Time &}
         *      reference to this as this divided by right.
         */
        Time & operator /=(int64_t const right);

        /**
         *  @operator %=
         *      Overload of binary %= operator to compute/assign remainder of a time value.
         *
         *  @param {Time const &} right
         *      Right operand (a time).
         *
         *  @returns {Time &}
         *      reference to this as this modulo right.
         */
        Time & operator %=(Time const & right);

    private:
        /**
         *  @function [explicit] Time
         *      Construct from a number of microseconds.
         *      This function is internal. To construct time values, use
         *      Time::seconds, Time::milliseconds or Time::microseconds instead.
         *
         *      @param {} {
         *      nt64_t} microseconds.
         *      Number of microseconds
         */
        explicit Time(int64_t microseconds);

        /**
         *  @property {int64_t} microseconds
         *      The time value in microseconds.
         */
        int64_t microseconds;
    };
}; /* namespace Voceliand */

#endif // VOCELIAND_TIME_HPP
