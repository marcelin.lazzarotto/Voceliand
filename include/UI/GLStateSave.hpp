/*
 *  File:   GLStateSave.hpp
 *  Author: Anenzoras
 *  Date:   2017-03-31
 */

#ifndef VOCELIAND_GLSTATESAVE_HPP
#define VOCELIAND_GLSTATESAVE_HPP

/* standard library include */
/* external library include */
#include <GL/glew.h> // cross-platform opengl loader

/* project include */
namespace Voceliand
{
    namespace UI
    {
        /**
         *  @class GLStateSave
         *      Class that uses RAII principle to store gl state in an object
         *      and restore it when said object goes out of scope.
         */
        class GLStateSave
        {
        public:
            /**
             *  @function GLStateSave
             *      Default constructor.
             *      Only way to construct by the way. Store the gl state. State
             *      is stored as long as the created object stays in scope.
             *      State is automatically restored afterward (see default
             *      constructor).
             */
            GLStateSave(void);
            
            /**
             *  @function ~GLStateSave
             *      Destructor.
             *      Restore state before disappearing (see default constructor).
             */
            ~GLStateSave(void);

        private:
            /**
             *  @function restoreBooleanValue
             *      Restore the value of a boolean in the glstate.
             *      Called 4 times, so a function is cleaner, eh?
             */
            inline void restoreBooleanValue(bool enabledBefore, GLenum valueName)
            {
                if (enabledBefore)
                {
                    glEnable(valueName);
                }
                else
                {
                    glDisable(valueName);
                }
            }

            /**
             *  @function GLStateSave
             *      Copy constructor disabled to avoid messing up.
             *
             *  @param {GLStateSave const &}
             *      Unused.
             */
            GLStateSave(GLStateSave const &);

            /**
             *  @operator =
             *      Assignement operator disabled to avoid messing up.
             *
             *  @param {GLStateSave const &}
             *      Unused.
             *
             *  @returns {GLStateSave &}
             *      Reference to this. Well usually, but this operator is
             *      disabled.
             */
            GLStateSave & operator=(GLStateSave const &);

            // Backup GL state -- declarations
            // all state value. Screw comments!
            GLint program;
            GLint texture;
            GLint activeTexture;
            GLint arrayBuffer;
            GLint elementArrayBuffer;
            GLint vertexArray;
            GLint blendSrc;
            GLint blendDst;
            GLint blendEquationRgb;
            GLint blendEquationAlpha;

            GLint viewport[4];
            GLint scissorBox[4];

            GLboolean enableBlend;
            GLboolean enableCullFace;
            GLboolean enableDepthTest;
            GLboolean enableScissorTest;
        };
    };
};

#endif // VOCELIAND_GLSTATESAVE_HPP
