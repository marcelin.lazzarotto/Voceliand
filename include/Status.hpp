/*
 *  File:   Status.hpp
 *  Author: Anenzoras
 *  Date:   2017-03-29
 */

#ifndef VOCELIAND_STATUS_HPP
#define VOCELIAND_STATUS_HPP

/* standard library include */
/* external library include */
/* project include */

namespace Voceliand
{
    namespace Status
    {
        /**
        *  @enum Code
        *       Contains all return values from program, instead of all writing
        *       on stderr.
        *
        *   @value SUCCESS
        *       Default status code when everything went according to the plan.
        *
        *   @value FAILURE
        *       Default status code for undocumented error. Is normally used
        *       during developpement when error still has to be defined.
        *
        *   @value FAILURE_OUT_OF_MEMORY
        *       Returned due to a refused allocation by the system.
        *
        *   @value INITIALIZATION_GLFW_FAILURE
        *       Initialization status code when glfw fails to initialize.
        *   @value INITIALIZATION_GLEW_FAILURE
        *       Initialization status code when glew/opengl fails to
        *       initialize.
        *
        *   @value FAILURE_DEVICE
        *       Default device failure.
        *
        *   @value FAILURE_INVALID_DEVICEID
        *       AudioInputDevice: the given device Id is not valid.
        *   @value FAILURE_NO_INPUT_DEVICE_AVAILABLE
        *       AudioInputDevice: can't find an usable device.
        *   @value FAILURE_DEVICE_UNPROBABLE
        *       AudioInputDevice: can't probe an input device.
        *
        *   @value FAILURE_INPUT_STREAM_UNOPENABLE
        *       AudioInputDevice: can't open audio input stream.
        *   @value FAILURE_CANT_START_INPUT_DEVICE
        *       AudioInputDevice: can't start reading stream on input device.
        */
        enum Code
        {
            SUCCESS = 0,

            FAILURE,

            FAILURE_OUT_OF_MEMORY,

            INITIALIZATION_GLFW_FAILURE,
            INITIALIZATION_GLEW_FAILURE,

            FAILURE_DEVICE,

            FAILURE_INVALID_DEVICEID,
            FAILURE_NO_INPUT_DEVICE_AVAILABLE,
            FAILURE_DEVICE_UNPROBABLE,

            FAILURE_INPUT_STREAM_UNOPENABLE,
            FAILURE_CANT_START_INPUT_DEVICE,

            FAILURE_TODO_FILESTREAM_CRAP,

            Code_COUNT
        };
    };
};

#endif // VOCELIAND_STATUS_HPP
