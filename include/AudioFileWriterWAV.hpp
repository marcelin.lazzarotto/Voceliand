/*
 *  File:   AudioFileWriterWAV.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 *  Note:   Temporary. This file writer will be used for tests before writing
 *      the actuel driver.
 */

#ifndef WAV_HPP
#define WAV_HPP

/* standard library includes */
#include <inttypes.h>
#include <string>
#include <fstream>

/* external library includes */
#include <soundio/soundio.h>

/* project includes */

namespace Voceliand
{
    /**
     *  @function translateFormat
     *      Interface format with soundio, to give the proper to the file.
     *      Temporary function to be removed at the same time as this file.
     *
     *  @param {SoundIoFormat const} soundIoFormat
     *      The format as defined by soundio.
     *
     *  @return {uint16_t}
     *      A number corresponding to the format according to wav.
     */
    uint16_t translateFormat(SoundIoFormat const soundIoFormat);

    /*
     *  WAV description:
     *  [Bloc de déclaration d'un fichier au format WAVE]
     *     FileTypeBlocID  (4 octets) : Constante "RIFF"
     *                                  (0x52, 0x49, 0x46, 0x46)
     *     FileSize        (4 octets) : Taille du fichier moins 8 octets
     *     FileFormatID    (4 octets) : Format = "WAVE"
     *                                  (0x57, 0x41, 0x56, 0x45)
     *
     *  [Bloc décrivant le format audio]
     *     FormatBlocID    (4 octets) : Identifiant "fmt "
     *                                  (0x66, 0x6D, 0x74, 0x20)
     *     BlocSize        (4 octets) : Nombre d'octets du bloc - 16 (0x10)
     *
     *     AudioFormat     (2 octets) : Format du stockage dans le fichier
     *                                  (1: PCM, 2: ADPCM, 3: IEEE floating
     *                                  point, 7: µ-law, 65534:
     *                                  WaveFormatExtensable)
     *
     *     NbrCanaux       (2 octets) : Nombre de canaux (de 1 à 6, cf.
     *                                  ci-dessous)
     *     Frequence       (4 octets) : Fréquence d'échantillonnage en hertz
     *                                  (Valeurs standardisées : 11025, 22050,
     *                                  44100 et éventuellement 48000 et 96000)
     *     BytePerSec      (4 octets) : Nombre d'octets à lire par seconde
     *                                  (c.-à-d., Frequence * BytePerBloc).
     *     BytePerBloc     (2 octets) : Nombre d'octets par bloc
     *                                  d'échantillonnage (c.-à-d., tous canaux
     *                                  confondus :
     *                                  NbrCanaux * BitsPerSample / 8).
     *     BitsPerSample   (2 octets) : Nombre de bits utilisés pour le codage
     *                                  de chaque échantillon (8, 16, 24)
     *
     *  [Bloc des données]
     *     DataBlocID      (4 octets) : Constante "data"
     *                                  (0x64, 0x61, 0x74, 0x61)
     *     DataSize        (4 octets) : Nombre d'octets des données Data[]
     *                                  (taille_du_fichier - taille_de_l'entête
     *                                  (qui fait 44 octets normalement)).
     *     Data[] : [Octets du Sample 1 du Canal 1]
     *              [Octets du Sample 1 du Canal 2]
     *              [Octets du Sample 2 du Canal 1]
     *              [Octets du Sample 2 du Canal 2]
     *
     *     * Les Canaux :
     *        1 pour mono,
     *        2 pour stéréo
     *        3 pour gauche, droit et centre
     *        4 pour face gauche, face droit, arrière gauche, arrière droit
     *        5 pour gauche, centre, droit, surround (ambiant)
     *        6 pour centre gauche, gauche, centre, centre droit, droit,
     *          surround (ambiant)
     *
     *  NOTES IMPORTANTES : Les octets des mots sont stockés sous la forme
     *  suivante : [87654321][16..9][24..17] [8..1][16..9][24..17] [...
     *  C'est à dire en little endian.
    */

    /**
    *   @class AudioFileWriterWAV
    *       Class dealing with .wav files, so that we can perform the algorithm
    *       tests without performing implementation on microphone capture and
    *       device output, which is a fucking mess.
    */
    class AudioFileWriterWAV
    {
    public:
        static const uint16_t FORMAT_PCM = 1;
        static const uint16_t FORMAT_FLOAT = 3;

        /**
         *  @function [static] check
         *      Check if this writer can handle a file on disk.
         *
         *  @param {std::string const &} filename
         *      Path of the sound file to check.
         *
         *  @returns {bool}
         *      True if the file can be written by this writer
         */
        static bool check(std::string const & filename);

        /**
         *  @function AudioFileWriterWAV
         *      Default constructor.
         */
        AudioFileWriterWAV(void);

        /**
         *  @function AudioFileWriterWAV
         *      Destructor.
         */
        ~AudioFileWriterWAV(void);

        /**
         *  @function open
         *      Open a sound file for writing.
         *
         *  @param {std::string const &} filename
         *      Path of the file to open.
         *  @param {uint32_t} sampleRate
         *      Sample rate of the sound.
         *  @param {uint32_t} channelCount
         *      Number of channels of the sound.
         *  @param {uint16_t const} audioFormat
         *      Audio format of the sound. Either FORMAT_PCM or FORMAT_FLOAT.
         *  @param {uint16_t const} sampleSize
         *      Size of a sample in number of bytes.
         *
         *  @returns {bool}
         *      True if the file was succesfully opened.
         */
        bool open(std::string const & filename, uint32_t sampleRate,
            uint16_t channelCount, uint16_t const audioFormat,
            uint16_t const sampleSize);

        /**
         *  @function write
         *      Write audio samples to the open file.
         *
         *  @param {char const *} samples
         *      Pointer to the sample array to write. Even if samples are
         *      several bytes long, pass it as char to write it. It will be
         *      written according to the type and size given when opening the
         *      file.
         *  @param {uint64_t} count
         *      Number of samples to write.
         *
         *  @warning
         *      AudioFileWriterWAV::write expects each samples of several bytes
         *      to be with little endianness.
         */
        void write(char const * samples, uint64_t count);

    private:
        /**
         *  @function writeHeader
         *      Write the header of the open file.
         *
         *  @param {uint32_t} sampleRate
         *      Sample rate of the sound.
         *  @param {uint16_t} channelCount
         *      Number of channels of the sound.
         *  @param {AudioFormat const} audioFormat
         *      Audio format of the sound.
         *
         *  @returns {bool}
         *      True on success, false on error
         */
        bool writeHeader(uint32_t sampleRate, uint16_t channelCount,
            uint16_t const audioFormat);

        /**
         *  @function close
         *      Close the file
         */
        void close(void);

        /**
         *  @property {std::ofstream} fileStream
         *      File stream to write to.
         */
        std::ofstream fileStream;

        /**
         *  @property {uint64_t} sampleCount
         *      Total number of samples written to the file.
         */
        uint64_t sampleCount;

        /**
         *  @property {unsigned int} channelCount
         *      Number of channels of the sound.
         */
        unsigned int channelCount;

        /**
         *  @property {uint16_t} sampleSize
         *      Sample size in bytes.
         */
        uint16_t sampleSize;
    };

// /* header size = 44 bytes */
// static const uint32_t HEADER_SIZE = 44;
//
// /* file declaration */
// char FileTypeBlocID[4]; // == "RIFF"
// uint32_t FileSize;      // == size - 8 (bytes);
// char FileFormatID[4];   // == "WAVE"
//
// /* audio block description */
// char FormatBlocID[4];   // == "fmt "
// uint32_t BlockSize;     // == (nb bytes of block - 16) == 0x10
//
// uint16_t AudioFormat;   // ==
// uint16_t ChannelsCount; // == [1:6]
// uint32_t Frequency;     // == 11025 || 22050 || 44100 (|| 48000 || 96000)
// uint32_t BytesPerSec;   // == Frequency * BytePerBloc
// uint16_t BytesPerBlock; // == ChannelsCount * BitsPerSample / 8
// uint16_t BitsPerSample; // == 8 || 16 || 32
//
// /* data block */
// char DataBlockID[4];    // == "data"
// uint32_t DataSize;      // == number of bytes in DATA[] == FileSize - HEADER_SIZE
// char DATA[];            // == [bytes from sample 1 - channel 1]
//                         //    [bytes from sample 1 - channel 2]
//                         //    [bytes from sample 2 - channel 1]
//                         //    [bytes from sample 2 - channel 2]

}; /* namespace Voceliand */

#endif // WAV_HPP
