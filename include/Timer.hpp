/*
 *  File:   Timer.hpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

#ifndef VOCELIAND_TIMER_HPP
#define VOCELIAND_TIMER_HPP

/* standard library include */
/* external library include */
/* project include */
#include "Time.hpp"

namespace Voceliand
{
    /**
     *  @class Timer
     *      Class dealing with timers. When created, an object starts its own
     *      timer which can be used for specific time measurement.
     */
    class Timer
    {
    public:
        /**
         *  @function Timer
         *      Default constructor.
         *      The clock starts automatically after being constructed.
         */
        Timer(void);

        /**
         *  @function getElapsedTime
         *      Get the elapsed time since the last reset (restart() call or the
         *      construction of the instance).
         *
         *  @returns {Time}
         *      The elapsed time.
         */
        Time getElapsedTime(void) const;

        /**
         *  @function restart
         *      Restart the clock.
         *      This function puts the time counter back to zero.
         *      It also returns the time elapsed since the clock was started.
         *
         *  @returns {Time}
         *      The time elapsed since last reset.
         */
        Time restart(void);

    private:

        /**
         *  @property {Time} startTime
         *      Time of last reset.
         */
        Time startTime;
    };
};

#endif // VOCELIAND_TIMER_HPP
