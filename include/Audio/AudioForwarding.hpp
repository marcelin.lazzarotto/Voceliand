/*
 *  File:   AudioForwarding.hpp
 *  Author: Anenzoras
 *  Date:   2017-09-09
 */

#ifndef VOCELIAND_AUDIO_FORWARDING_HPP
#define VOCELIAND_AUDIO_FORWARDING_HPP

/* standard library includes */
#include <vector>

/* external library includes */
/* project includes */

namespace Voceliand
{
    namespace AudioForwarding
    {
        /**
         *  @function setup
         *      Prepare the forwarding: connect to the backend.
         *
         *  @returns {Status::Code}
         *      Status::SUCCESS if the connection is successful.
         *      TODO if an error occured.
         */
        Status::Code setUp(void);
        /**
         *  @function tearDown
         *      Disconnect from the backend.
         */
        void tearDown(void);
        /**
         *  @function sendSamples
         *      Send the samples to the backend.
         *
         *  @param {std::vector<char> const &} samples
         *      The samples, as a char-encoded format. 
         */
        void /* error ? */ sendSamples(std::vector<char> const & samples);
        void temp__SendAnteSamples(std::vector<char> const & samples);
    }; /* namespace AudioForwarding */
}; /* namespace Voceliand */

#endif // VOCELIAND_AUDIO_FORWARDING_HPP
