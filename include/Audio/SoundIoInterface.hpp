/*
 *  File:   SoundIoInterface.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-10
 */

#ifndef RECORD_SOUNDIO_INTERFACE
#define RECORD_SOUNDIO_INTERFACE

/* standard library includes */
/* external library includes */
#include <soundio/soundio.h>

/* project includes */

namespace Voceliand
{
    /**
     *  @class SoundIoInterface
     *      Singleton class (urg) to deal with soundio global structure.
     */
    class SoundIoInterface
    {
    public:
        /**
         *  @function [static] getInstance
         *      Singleton static function to get the instance.
         *
         *  @returns {SoundIoInterface &}
         *      The only instance of SoundIoInterface.
         */
        static SoundIoInterface & getInstance(void);

        /**
         *  @property {SoundIo *} soundio
         *      Pointer to the global soundio structure.
         *      We are in a singleton, so we can leave it like this (although
         *      its a bit -MEH-)
         */
        SoundIo * soundio;

    private:
        /**
         *  @function SoundIoInterface
         *      Constructor. Sets up soundio general structure in .soundio
         *      property. Checks the several error case, in which case the
         *      program exits.
         */
        SoundIoInterface(void);

        /**
         *  @function ~SoundIoInterface
         *      Destructor. Cleans up .soundio general structure.
         */
        ~SoundIoInterface(void);

        /* Removed function */
        SoundIoInterface(SoundIoInterface const &);
        /* Removed function */
        SoundIoInterface & operator =(SoundIoInterface const &);
    };
}; /* namespace Voceliand */

#endif // RECORD_SOUNDIO_INTERFACE
