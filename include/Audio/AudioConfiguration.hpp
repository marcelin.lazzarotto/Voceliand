/*
 *  File:   AudioConfiguration.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 */

#ifndef VOCELIAND_AUDIO_CONFIGURATION_HPP
#define VOCELIAND_AUDIO_CONFIGURATION_HPP

/* standard library includes */
#include <inttypes.h>

/* external library includes */
/* project includes */

namespace Voceliand
{
    namespace AudioConfiguration
    {
        enum Format_e
        {
            Format_SIGNED_PCM,
            Format_UNSIGNED_PCM,
            Format_FLOAT,

            Format_INVALID
        };

        enum Endianness_e
        {
            Endianness_NATIVE,
            Endianness_FOREIGN,

            Endianness_INVALID
        };

        /**
         *  @struct SampleInfo
         *      Structure to keep track of the currently used audio
         *      configuration. Info are stored in the global variable
         *      sampleInfo.
         */
        struct SampleInfo
        {
            /* size in bytes */
            uint32_t size;
            /* bit depth (or size * 8) */
            uint32_t bitDepth;
            /* sample rate */
            uint32_t rate;
            /* number of channel used */
            uint32_t channelCount;
            /* floating point sample or integer */
            enum Format_e format;
            /* native / foreign */
            enum Endianness_e endianness;
        }; /* struct SampleInfo */

        extern SampleInfo sampleInfo;

        /**
         *  @function unsetAll
         *      Set all configuration values to INVALID, or 0. Configuration in
         *      such a state means no sound input nor output can be used.
         */
        void unsetAll(void);
    }; /* namespace AudioConfiguration */
}; /* namespace Voceliand */

#endif // VOCELIAND_AUDIO_CONFIGURATION_HPP
