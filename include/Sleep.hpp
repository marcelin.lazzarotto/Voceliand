/*
 *  File:   Sleep.hpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

#ifndef VOCELIAND_SLEEP_HPP
#define VOCELIAND_SLEEP_HPP

/* standard library include */
/* external library include */
/* project include */
#include "Time.hpp"

namespace Voceliand
{
    /**
     *  @function sleep
     *      Cross-platform sleep function.
     *      Put the calling thread to sleep for the given duration.
     */
    void sleep(Time duration);
};

#endif // VOCELIAND_SLEEP_HPP
