/*
 *  File:   SoundEffect.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 */

#ifndef VOCELIAND_SOUND_EFFECT_HPP
#define VOCELIAND_SOUND_EFFECT_HPP

/* standard library includes */
/* external library includes */
/* project includes */

namespace Voceliand
{
    /**
     *  @class SoundEffect
     *      Empty mother class. For... yeah, things...
     */
    class SoundEffect
    {
    }; /* class SoundEffect */
}; /* namespace Voceliand */

#endif // VOCELIAND_SOUND_EFFECT_HPP
