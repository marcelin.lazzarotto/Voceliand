# Introduction

Voceliand, or Voceliand for short, aims to be a dynamic range compressor
software which process audio compression effect on voice in real-time. It aims
to improve voice signal before being used in third party applications such as
Skype/Mumble/TS/Discord/OBS so that any conversation sounds better even without
an expensive audio setup.

As of now, this whole project exists as a proof of concept of a proof of
concept (we are basically at version 0.0.-1). More details follow.

# Build

The Makefile contains most information to compile the current build on windows
and linux.

Be sure to precise the target

    make TARGET=<platform>

when calling the makefile. Use 'make run' afterward (precise TARGET again).
Choices are win32, win64 linux32, linux64. Default is win32.

On windows, the build environment was set on cygwin using mingw, although any
terminal provided with make and mingw will probably do the job. On linux,
any terminal with make and gcc do the job.

# Why

The software aims to solve 3 issues.

As of now, for an all-software solution to exists to perform voice effects
that can be used in third party softwares, 3 programs are needed at least: a
program that can redirect audio signals (such as Virtual Audio Cable (or VAC)
which, for the record is proprietary and cost a bit of money); a program that
can host VST effects and apply them in real time and a VST effect.

Although, more recently appeared VoiceMeter Banana a software that roughly
combine things, it is donationware and those using that software find it hard to
use.

The second issue is money. Although I perfectly understand that quality has a
price, I believe that to have a somewhat decent sound, price shouldn't be a
problem.

Currently, physical compressors or audio cards that could provide similar
effects cost around $50. In a similar way, a license for VAC costs $20-50
according to the official website. Alternatively, VB-Audio Cable (a similar
software to VAC) is donationware. Once again, quality deserve a decent price; I
however think we could provide a simple non-commercial solution.

The third issue is Open Sourcing. In this domain, all common softwares are under
proprietary licenses. I'm not a particular fanatic of Open Source, but I think
it's a good ideology and can definitively help in nurturing projects of this
kind.

To sum things up, Voceliand aims to be and stay an *all-in-one*, *free* and
*open source* software.

Another side reason is because I kinda like giving myself weird challenges.
Dumb, isn't it?

# How

## Short term goal

Voceliand takes an input stream from an input device (microphone), process
compression and then output stream as a virtual input device to be used by the
operating system and programs.

## Current state of the art

Voceliand takes an input stream from an input device (microphone), process
compression and then output both signals to 2 .wav files, to compare outputs as
a basic test.

On Windows, attempts are made to have a fake output device.

## How, Technically

Using libsoundio, a cross-platform library, to retrieve sound input; Voceliand
then implements by itself a real-time compression algorithm, retrieved from a
research paper (copied in the documentation/ directory). After this is done,
Voceliand will send (NOT COMPLETE YET) the resulting signal into a virtual
device, and this differs depending on system implementations.

### Windows

On Windows, Voceliand will rely on Boost.Interprocess, DLL injection and
hooking.

*Preamble*: There's only one official way to output onto a virtual device on
Windows: developing a kernel driver. The problem is that not only developing a
driver on Windows is a major hassle, it is an hell to test (since any driver
error on Windows can make the system go Blue Screen) and debug. To top it all,
Windows requires for a driver to be certified, certification requiring money
(because...... yeah), said money being a few hundreds dollars.

I wouldn't really mind paying, if the goal of the project wasn't for a user to
not spend a single dime, myself being included. So I needed a solution to allow
any third-party software to access a fake device (end preamble).

The solution was to proxy Windows' User Mode Audio Interfaces using a technique
known as Hooking. Current implementation is located in the directory
DllHookTest/ as a standalone test to just attempt to output audio data into a
program (for now audacity). There's yet several problems to solve before being
successful but we are onto something. For now, we can successfully inject the
DLL (that will hook onto Windows API) into a third-party application, which has
access to several proxy interfaces but there's still problems to solve.

Once this is done, DllHookTest will attempt to extend injection to all currently
running process so that we can successfully mimic a functional audio device
system-wide. And after this, the code in DllHookTest will somewhat merge into
Voceliand, so that we redirect the signal onto it. Boost.Interprocess will then
be used to correctly exchange signal information between Voceliand and all
hooked processes.

### Linux

Nothing is done on Linux for now.

However, after several short research, implementation seems more straightforward
on that system. There's apparently ways for ALSA (the Linux audio subsystem) to
generate a fake device and even if this is not usable in the end, making a
device driver for a fake device is way more safe and reliable than on Windows.

### Mac

Nothing is done for Mac for now.

No research have been performed.

# Licensing information

Voceliand is licensed under the zlib/libpng license. It uses several libraries:

- glew is licensed under the Modified BSD License, Mesa 3-D License and the
Khronos License
- glfw is licensed under the zlib/libpng license
- Dear ImGui is licensed under the MIT license
- libsoundio is licensed under the MIT license
