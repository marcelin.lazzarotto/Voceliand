/*
 *  File:   Time.cpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

/* standard library include */
/* external library include */
/* project include */
#include "../include/Time.hpp"

namespace Voceliand
{
    /* Mostly taken from SFML code.
    https://github.com/SFML/SFML/blob/master/src/SFML/System/Time.cpp
    */

    /* Predefined "zero" time value. */
    Time const Time::ZERO;

    /* Sets the time value to zero. */
    Time::Time(void) : microseconds(0)
    {
    }

    /* Construct from a number of microseconds. */
    Time::Time(int64_t microseconds) : microseconds(microseconds)
    {
    }

    /* Returns a Time object using a value in seconds. */
    Time Time::inSeconds(float seconds)
    {
        return Time(static_cast<int64_t>(seconds * 1000000));
    }

    /* Returns a Time object using a value in milliseconds. */
    Time Time::inMilliseconds(int32_t milliseconds)
    {
        return Time(static_cast<int32_t>(milliseconds) * 1000);
    }

    /* Returns a Time object using a value in microseconds. */
    Time Time::inMicroseconds(int64_t microseconds)
    {
        return Time(microseconds);
    }

    /* Return the time value as a number of seconds. */
    float Time::asSeconds(void) const
    {
        return this->microseconds / 1000000.f;
    }

    /* Return the time value as a number of milliseconds. */
    int32_t Time::asMilliseconds(void) const
    {
        return static_cast<int32_t>(this->microseconds / 1000);
    }

    /* Return the time value as a number of microseconds. */
    int64_t Time::asMicroseconds(void) const
    {
        return this->microseconds;
    }

    /* Overload of == operator to compare two time values. */
    bool Time::operator ==(Time const & right) const
    {
        return this->asMicroseconds() == right.asMicroseconds();
    }

    /* Overload of != operator to compare two time values. */
    bool Time::operator !=(Time const & right) const
    {
        return this->asMicroseconds() != right.asMicroseconds();
    }

    /* Overload of < operator to compare two time values. */
    bool Time::operator <(Time const & right) const
    {
        return this->asMicroseconds() < right.asMicroseconds();
    }

    /* Overload of > operator to compare two time values. */
    bool Time::operator >(Time const & right) const
    {
        return this->asMicroseconds() > right.asMicroseconds();
    }

    /* Overload of <= operator to compare two time values. */
    bool Time::operator <=(Time const & right) const
    {
        return this->asMicroseconds() <= right.asMicroseconds();
    }

    /* Overload of >= operator to compare two time values. */
    bool Time::operator >=(Time const & right) const
    {
        return this->asMicroseconds() >= right.asMicroseconds();
    }

    /* Overload of unary - operator to negate a time value. */
    Time Time::operator -(void) const
    {
        return inMicroseconds(-(this->asMicroseconds()));
    }

    /* Overload of binary + operator to add two time values. */
    Time Time::operator +(Time const & right) const
    {
        return inMicroseconds(this->asMicroseconds() + right.asMicroseconds());
    }

    /* Overload of binary - operator to subtract two time values. */
    Time Time::operator -(Time const & right) const
    {
        return inMicroseconds(this->asMicroseconds() - right.asMicroseconds());
    }

    /* Overload of binary * operator to scale a time value. */
    Time Time::operator *(float const right) const
    {
        return inSeconds(this->asSeconds() * right);
    }

    /* Overload of binary * operator to scale a time value. */
    Time Time::operator *(int64_t const right) const
    {
        return inMicroseconds(this->asMicroseconds() * right);
    }

    /* Overload of binary * operator to scale a time value. Symetry */
    Time operator *(float const left, Time const & right)
    {
        return right * left;
    }

    /* Overload of binary * operator to scale a time value. Symetry */
    Time operator *(int64_t const left, Time const & right)
    {
        return right * left;
    }

    /* Overload of binary / operator to scale a time value. */
    Time Time::operator /(float const right) const
    {
        return inSeconds(this->asSeconds() / right);
    }

    /* Overload of binary / operator to scale a time value. */
    Time Time::operator /(int64_t const right) const
    {
        return inMicroseconds(this->asMicroseconds() / right);
    }

    /* Overload of binary / operator to compute the ratio of two time */
    float Time::operator /(Time const & right) const
    {
        return this->asSeconds() / right.asSeconds();
    }

    /* Overload of binary % operator to compute remainder of a time */
    Time Time::operator %(Time const & right) const
    {
        return inMicroseconds(this->asMicroseconds() % right.asMicroseconds());
    }

    /* Overload of binary += operator to add/assign two time values. */
    Time & Time::operator +=(Time const & right)
    {
        return (*this) = (*this) + right;
    }

    /* Overload of binary -= operator to subtract/assign two time */
    Time & Time::operator -=(Time const & right)
    {
        return (*this) = (*this) - right;
    }

    /* Overload of binary *= operator to scale/assign a time value. */
    Time & Time::operator *=(float const right)
    {
        return (*this) = (*this) * right;
    }

    /* Overload of binary *= operator to scale/assign a time value. */
    Time & Time::operator *=(int64_t const right)
    {
        return (*this) = (*this) * right;
    }

    /* Overload of binary /= operator to scale/assign a time value. */
    Time & Time::operator /=(float const right)
    {
        return (*this) = (*this) / right;
    }

    /* Overload of binary /= operator to scale/assign a time value. */
    Time & Time::operator /=(int64_t const right)
    {
        return (*this) = (*this) / right;
    }

    /* Overload of binary %= operator to compute/assign remainder of a time
    value. */
    Time & Time::operator %=(Time const & right)
    {
        return (*this) = (*this) % right;
    }
}; /* namespace Voceliand */
