/*
 *  File:   Sleep.cpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

/* standard library include */
#include <inttypes.h>

/* external library include */
/* project include */
#include "../include/Configuration.hpp"
#include "../include/Sleep.hpp"

/* Mostly taken from SFML source code.
https://github.com/SFML/SFML/blob/master/src/SFML/System/Unix/SleepImpl.cpp
https://github.com/SFML/SFML/blob/master/src/SFML/System/Win32/SleepImpl.cpp
https://github.com/SFML/SFML/blob/master/src/SFML/System/Sleep.cpp
*/
#if defined(VOCELIAND_SYSTEM_WINDOWS)
    #include <windows.h>

namespace Voceliand
{
    void crossPlatformSleep(Time time)
    {
        // Get the supported timer resolutions on this system
        TIMECAPS tc;
        timeGetDevCaps(&tc, sizeof(TIMECAPS));

        // Set the timer resolution to the minimum for the Sleep call
        timeBeginPeriod(tc.wPeriodMin);

        // Wait...
        ::Sleep(time.asMilliseconds());

        // Reset the timer resolution back to the system default
        timeEndPeriod(tc.wPeriodMin);
    }
};
#else /* unix basically */
    #include <errno.h>
    #include <time.h>

namespace Voceliand
{
    void crossPlatformSleep(Time time)
    {
        uint64_t usecs = time.asMicroseconds();

        // Construct the time to wait
        timespec ti;
        ti.tv_nsec = (usecs % 1000000) * 1000;
        ti.tv_sec = usecs / 1000000;

        // Wait...
        // If nanosleep returns -1, we check errno. If it is EINTR
        // nanosleep was interrupted and has set ti to the remaining
        // duration. We continue sleeping until the complete duration
        // has passed. We stop sleeping if it was due to an error.
        while ((nanosleep(&ti, &ti) == -1) && (errno == EINTR))
        {
        }
    }
};
#endif

namespace Voceliand
{
    /* cross-platform sleep function */
    void sleep(Time duration)
    {
        if (duration >= Time::ZERO)
        {
            crossPlatformSleep(duration);
        }
    }
};
