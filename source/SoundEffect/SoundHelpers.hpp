/*
 *  File:   SoundHelpers.hpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 *  Note:   SoundHelpers is a private file to help dealing with sound buffers.
 */

#ifndef VOCELIAND_SOUND_HELPERS_HPP
#define VOCELIAND_SOUND_HELPERS_HPP

/* standard library includes */
#include <vector>

/* external library includes */
/* project includes */

namespace Voceliand
{
    namespace SoundHelpers
    {
        /**
         *  @function toFloatSamples
         *      Because Voceliand makes no assumption on the audio signal, this
         *      function is used when a computation uses a specific type.
         *      Mostly sound signal computations are way easier when using
         *      floating point samples.
         *      This function takes an input vector of char whose carry data as
         *      defined in AudioConfiguration::sampleInfo and transform it in a
         *      vector of char holding floating point values.
         *
         *  @note
         *      To be sure to be correct with bit depth, the functions converts
         *      samples to the higher precision (double).
         *
         *  @param {std::vector<char> const &} samples
         *      The samples in the form given by AudioConfiguration::sampleInfo
         *  @param {std::vector<char> &} translatedSamples
         *      The samples as floating point values inside a vector of char.
         */
        void toFloatSamples(std::vector<char> const & samples,
            std::vector<double> & translatedSamples);

        /**
         *  @function reencode
         *      Complement to toFloatSamples(), retransform the given vector
         *      encoded as floating point values back to the format
         *      corresponding to AudioConfiguration::sampleInfo.
         *
         *  @param {std::vector<double> const &} floatSamples
         *      Vector of char encoding floating point samples.
         *  @param {std::vector<char> &} samples
         *      Resulting samples encoded back to normal.
         */
        void reencode(std::vector<double> const & floatSamples,
            std::vector<char> & samples);

        /**
         *  @function getNthSampleOfChannel()
         *      Access the sample in the given vector of sample within the given
         *      channel and returns its value.
         *      This avoids splitting the vector of sample into one vector per
         *      channel.
         *
         *  @param {std::vector<double> const &} samples
         *      Vector of char encoding floating point samples.
         *  @param {size_t} channel
         *      The index of the channel to access.
         *  @param {size_t} index
         *      Index of the element to access.
         *
         *  @warning
         *      - The vector is expected to be a double representation of
         *      samples;
         *      - Channel are indexed from 0 to channelCount;
         *      - Undefined behavior if index is greater than the number of
         *      frame in the vector (samples.size() / channelCount);
         *      - Undefined behavior if channel is greater than the number of
         *      channels used, as defined in AudioConfiguration::sampleInfo.
         *
         *  @returns {double &}
         *      Reference to the accessed sample.
         */
        double getNthSampleOfChannel(std::vector<double> const & samples,
            size_t channel, size_t index);

        /**
         *  @function setNthSampleOfChannel()
         *      Access the sample in the given vector of sample within the given
         *      channel and sets its value to the given one.
         *      This avoids splitting the vector of sample into one vector per
         *      channel.
         *
         *  @param {std::vector<double> const &} samples
         *      Vector of char encoding floating point samples.
         *  @param {size_t} channel
         *      The index of the channel to access.
         *  @param {size_t} index
         *      Index of the element to access.
         *  @param {double} value
         *      Value to set.
         *
         *  @warning
         *      - The vector is expected to be a double representation of
         *      samples;
         *      - Channel are indexed from 0 to channelCount;
         *      - Undefined behavior if index is greater than the number of
         *      frame in the vector (samples.size() / channelCount);
         *      - Undefined behavior if channel is greater than the number of
         *      channels used, as defined in AudioConfiguration::sampleInfo.
         */
        void setNthSampleOfChannel(std::vector<double> & samples,
            size_t channel, size_t index, double value);

        /**
         *  @function computeFrameCount
         *      Returns the number of frame inside the given vector of samples.
         *      A frame correspond to the same sample of each channel. Thus,
         *      the frame count is equal to the number of samples divided by the
         *      number of used channels.
         *
         *  @param {std::vector<double> const &} samples
         *      The samples vector (just to access the size() function).
         *
         *  @returns {size_t}
         *      The number of frames.
         */
        size_t computeFrameCount(std::vector<double> const & samples);
    }; /* namespace SoundHelpers */
}; /* namespace Voceliand */

#endif // VOCELIAND_SOUND_HELPERS_HPP
