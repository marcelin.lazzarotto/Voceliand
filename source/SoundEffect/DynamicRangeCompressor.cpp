/*
 *  File:   DynamicRangeCompressor.cpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 */

/* standard library includes */
#include <cmath>
#include <cstdio>

/* external library includes */
/* project includes */
#include "../../include/Audio/AudioConfiguration.hpp"
#include "../../include/SoundEffect/DynamicRangeCompressor.hpp"
#include "SoundHelpers.hpp"

namespace Voceliand
{
    /*
        Most of the following code is inspired from
        https://github.com/chipaudette/OpenAudio_ArduinoLibrary/blob/master/AudioEffectCompressor_F32.h
        Shoutouts to Chip Audette.
        Note 1: there's an error on calcGain function where the function do
        gain = (signal - threshold / ratio) - (signal - threshold)
        while it should be
        gain = (signal - threshold / ratio) - signal
        TODO send an issue on his github project
    */
    /*
        After consulting the PDF in documentation, the way compression is
        implemented here can be summed up as:
        1. Take the signal
        2. Apply a pregain (I prefer doing it here in order to apply compression
           over the gained signal in case it goes too far)
        3. Transform to absolute values (here we square)
        4. Transform to dB (== 10 * log(squared))
        5. Gain computation over the dB signal
        6. Level detection over the given gain
        7. Translation of the gain back to linear signal
        8. Apply compression (multiply computed gain with pregained signal)
    */

    /**
     *  @function computeDBLevel
     *      This function estimates the level of the audio signal in dB.
     *      It squares the given input signal and apply a low-pass
     *      filter to get a time-averaged signal power.
     *
     *  @note
     *      From my understanding, the squaring and the low-pass filter act as
     *      a RMS (root means squared) approximation in order to get a proper
     *      amplitude-to-dB conversion.
     *      From the article in documentation: "In the implementation of real-
     *      world effects this measurement is often approximated by filtering
     *      the squared input signal with a first-order low-pass IIR filter and
     *      taking the square root of the output."
     *
     *  @param {std::vector<double> const &} input
     *      The input vector of samples.
     *  @param {std::vector<double> &} dBLevel
     *      The output vector storing the dBLevel of the signal.
     */
    void DynamicRangeCompressor::computeDBLevel(
        std::vector<double> const & input,
        std::vector<double> & dBLevel)
    {
        /* Safety. */
        dBLevel.resize(input.size());

        /* Clarity. */
        uint32_t const channelCount = AudioConfiguration::sampleInfo.channelCount;
        uint32_t frameCount = SoundHelpers::computeFrameCount(input);

        /* Here we have to split by channel, since we have to use the previous
         * value with the current sample. */
        for (size_t frame = 0; frame < frameCount; frame++)
        {
            for (size_t channel = 0; channel < channelCount; channel++)
            {
                double squaredSample;
                double sampleDB;

                /* Compute the instantaneous signal power (square the signal)
                * (1) */
                squaredSample = SoundHelpers::getNthSampleOfChannel(input, channel, frame);
                squaredSample *= squaredSample;

                /* First-order low-pass filter to get a running estimate of the
                * average power. */
                /* The first order low-pass filter operation:
                * y[i] = a * x[i] + (1-a) * y[i-1]
                * where
                *  x is the vector of sample before filtering
                *  y is the vector of sample after filtering
                *  a is the smoothing factor of the filter
                * https://en.wikipedia.org/wiki/Low-pass_filter
                * Except here, trusting the paper in the documentation, we got
                * y²[i] = a * y²[i - 1] + (1 - a) x²[i]
                */
                sampleDB = (1.0 - this->smoothingFactor) * squaredSample
                + (this->smoothingFactor) * this->previousLowPassLevel[channel];

                /* save the state of the first order low pass filter */
                this->previousLowPassLevel[channel] = sampleDB;

                /* Limit the amount that the state of the smoothing filter can
                * go toward negative infinity.
                * Never go less than -130 dBFS.
                */
                if (this->previousLowPassLevel[channel] < (1.0E-13))
                {
                    this->previousLowPassLevel[channel] = 1.0E-13;
                }

                /* Now convert the signal power to dB*/
                sampleDB = 10.0 * std::log10(sampleDB);

                /* Set in the output. */
                SoundHelpers::setNthSampleOfChannel(dBLevel, channel, frame, sampleDB);
            }
        }

        /* (1) The normal float to dB approximation is something like
         * $20 * log10(amplitude)$. The fact is that we got a signal
         * squared, so we basically got $10 * log10(amplitude²)$ which
         * is... basically the same thing.
         *
         * Note 1: the $20 * log10(amplitude)$ formula seems to work
         * only with values between 0.0 and 1.0, which bugs me about the
         * code I found.
         * Note 2: with further research, the [0;1] interval is the
         * signal amplitude (duh), which, if I could remember my physics
         * lesson, is the distance between a signal and its median at
         * time t. So... simply an absolute value of our [-1;1]
         * signal...
         * Oh, and since we got it squared, in the end it doesn't even
         * matter :D
         */
    }

    /**
     *  @function computeInstantaneousTargetGain
     *      Compute the instantaneous desired gain, including the
     *      compression ratio and threshold for where the compression
     *      kicks in.
     *
     *  @param {std::vector<double> const &} dBLevel
     *      The dBLevel of the signal.
     *  @param {std::vector<double> &} instantaneousGain
     *      The instantaneous gain computed
     */
    void DynamicRangeCompressor::computeInstantaneousTargetGain(
        std::vector<double> const & dBLevel,
        std::vector<double> & instantaneousGain)
    {
        /* Safety. */
        instantaneousGain.resize(dBLevel.size());

        for (size_t i = 0; i < dBLevel.size(); i++)
        {
            /* If we are under threshold, no gain to apply (default). */
            instantaneousGain[i] = 0.0;

            /* Hard-knee threshold:
             *  if x[i] > T
             *      y[i] = T + (x[i] − T ) / R
             *      g[i] = y[i] - x[i] */
            if (dBLevel[i] > this->threshold)
            {
                double desiredOutput = this->threshold
                    + (dBLevel[i] - this->threshold) / this->ratio;
                instantaneousGain[i] = desiredOutput - dBLevel[i];
            }
        }
    }

    /**
     *  @function computeSmoothedGain
     *      This method applies the "attack" and "release" constants to smooth
     *      the target gain level through time.
     *
     *  @param {std::vector<double> const &} instantaneousGain
     *      Previously computed instantaneous gain.
     *  @param {std::vector<double> &} smoothedGain
     *      The resulting smoothed gain to apply.
     */
    void DynamicRangeCompressor::computeSmoothedGain(
        std::vector<double> const & instantaneousGain,
        std::vector<double> & smoothedGain)
    {
        /* Safety. */
        smoothedGain.resize(instantaneousGain.size());

        uint32_t const channelCount = AudioConfiguration::sampleInfo.channelCount;
        uint32_t frameCount = SoundHelpers::computeFrameCount(instantaneousGain);

        /* Split by channel again, since we need the previous value again. */
        for (size_t frame = 0; frame < frameCount; frame++)
        {
            for (size_t channel = 0; channel < channelCount; channel++)
            {
                double sampleGain = SoundHelpers::getNthSampleOfChannel(instantaneousGain,
                    channel, frame);
                double sampleSmoothedGain;

                /* Smooth the gain using the attack or release constants.
                 *  if (x[n] > y[n − 1]) // attack phase
                 *      y[n] = αAttack * y[n − 1] + (1 − αAttack) * x[n]
                 *  else // if (x[n] ≤ y [n − 1]) // release phase
                 *      y[n] = αRelease * y[n − 1] + (1 − αRelease) * x[n]
                 */
                if (sampleGain < this->previousSmoothedGainDB[channel])
                {
                    sampleSmoothedGain = this->attackFactor * this->previousSmoothedGainDB[channel]
                        + (1.0 - this->attackFactor) * sampleGain;
                }
                else
                {
                    sampleSmoothedGain = this->releaseFactor * this->previousSmoothedGainDB[channel]
                        + (1.0 - this->releaseFactor) * sampleGain;
                }

                /* Save the value for the next operation */
                this->previousSmoothedGainDB[channel] = sampleSmoothedGain;

                SoundHelpers::setNthSampleOfChannel(smoothedGain, channel, frame,
                    sampleSmoothedGain);
            }
        }
    }

    /**
     *  @function computeDynamicGain
     *      Computes the desired gain for the compression from the
     *      estimate of the signal level (in dB).
     *
     *  @param {std::vector<double> const &} dBLevel
     *      The vector of the signal's audio level in dB.
     *  @param {std::vector<double> &} gain
     *      The gain computed from the audio level.
     */
    void DynamicRangeCompressor::computeDynamicGain(
        std::vector<double> const & dBLevel,
        std::vector<double> & gain)
    {
        std::vector<double> instantaneousGain;
        std::vector<double> smoothedGain;

        /* first, calculate the instantaneous target gain based on the
         * compression ratio */
        computeInstantaneousTargetGain(dBLevel, instantaneousGain);

        //second, smooth in time (attack and release) by stepping through each sample
        computeSmoothedGain(instantaneousGain, smoothedGain);

        /* Finally, convert from dB to linear gain
         * gain = 10^(smoothedGain / 20) */
        for (size_t i = 0; i < smoothedGain.size(); i++)
        {
            gain[i] = std::pow(10.0, smoothedGain[i] / 20.0);
        }
    }

    /* Perform audio compression over the signal given by the input vector of
     * samples. */
    void DynamicRangeCompressor::process(void)
    {
        /*
         * Safety checks: if no input or output have been defined or if the
         * processor hasn't been properly parameted, return nothing.
         */
        if (this->input == NULL || this->output == NULL)
        {
            return;
        }
        if (this->attackFactor == 0 || this->releaseFactor == 0
            || this->smoothingFactor == 0)
        {
            /* write in error console
             * TODO check if maybe this is not so much of a problem, although
             * the resulting compression may be poor */
            fprintf(stderr, "[DynamicRangeCompressor] Factors not properly set"
                " up.\n");
            return;
        }

        std::vector<double> inputFloat;   /* samples in floating point */
        std::vector<double> inputDB;      /* samples power in dB */
        std::vector<double> inputGain;    /* gain computed from amplitude */
        std::vector<double> outputFloat;  /* result samples in float */

        /* This function transform input samples to floating point samples,
         * still stored as a char vector.
         * We do so because computation on sound signal is way easier in
         * float.
         */
        SoundHelpers::toFloatSamples(*(this->input), inputFloat);
        inputDB.resize(inputFloat.size());
        inputGain.resize(inputFloat.size());
        outputFloat.resize(inputFloat.size());

        /* Apply pregain (a multiplication). */
        if (this->preGain > 0.0)
        {
            for (size_t i = 0; i < inputFloat.size(); i++)
            {
                inputFloat[i] = this->preGain * inputFloat[i];
            }
        }

        /* Transform signal into decibel equivalent.
         * Result in dB. */
        computeDBLevel(inputFloat, inputDB);

        /* Compute the desired gain we can deduce from given audio level. */
        computeDynamicGain(inputDB, inputGain);

        /* Multiply input values by gain computed values and put it in
         * output float. */
        for (size_t i = 0; i < inputFloat.size(); i++)
        {
            outputFloat[i] = inputFloat[i] * inputGain[i];
        }

        /* Encode back the signal into the current sample configuration. */
        SoundHelpers::reencode(outputFloat, *(this->output));
    }
}; /* namespace Voceliand */
