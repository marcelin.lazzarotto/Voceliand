/*
 *  File:   Timer.cpp
 *  Author: Anenzoras
 *  Date:   2017-03-30
 */

/* standard library include */
/* external library include */
/* project include */
#include "../include/Configuration.hpp"
#include "../include/Timer.hpp"

/* Cross platform clock - imperfect microsecond precision */
/* Mostly taken from SFML source code.
https://github.com/SFML/SFML/blob/master/src/SFML/System/Win32/ClockImpl.cpp
https://github.com/SFML/SFML/blob/master/src/SFML/System/Unix/ClockImpl.cpp
https://github.com/SFML/SFML/blob/master/src/SFML/System/Clock.cpp
*/
#if defined(VOCELIAND_SYSTEM_WINDOWS)
    // TODO: may need a check on older windows version but then need
    // mutex so -meh-
    #include <windows.h>

    namespace Voceliand
    {
        namespace
        {
            LARGE_INTEGER getFrequency()
            {
                LARGE_INTEGER frequency;
                QueryPerformanceFrequency(&frequency);
                return frequency;
            }
        };

        Time getCurrentTime(void)
        {
            static LARGE_INTEGER frequency = getFrequency();

            LARGE_INTEGER time;

            // Get the current time
            QueryPerformanceCounter(&time);

            // Return the current time as microseconds
            return Time::inMicroseconds(1000000 * time.QuadPart
                / frequency.QuadPart);
        }
    };
#else
    #if defined(VOCELIAND_SYSTEM_MACOS) || defined(VOCELIAND_SYSTEM_IOS)
        #include <mach/mach_time.h>
    #else /* if unix, basically */
        #include <time.h>
    #endif

    namespace Voceliand
    {
        Time getCurrentTime(void)
        {
            #if defined(VOCELIAND_SYSTEM_MACOS) || defined(VOCELIAND_SYSTEM_IOS)
                // Mac OS X specific implementation (it doesn't support
                // clock_gettime)
                static mach_timebase_info_data_t frequency = {0, 0};
                if (frequency.denom == 0)
                {
                    mach_timebase_info(&frequency);
                }
                uint64_t nanoseconds = mach_absolute_time()
                    * frequency.numer / frequency.denom;
                return Time::inMicroseconds(nanoseconds / 1000);
            #else
                // POSIX implementation
                timespec time;
                clock_gettime(CLOCK_MONOTONIC, &time);
                return Time::inMicroseconds(static_cast<uint64_t>(time.tv_sec)
                    * 1000000 + time.tv_nsec / 1000);
            #endif
        }
    };
#endif

namespace Voceliand
{
    /* default constructor */
    Timer::Timer(void) :
    startTime(getCurrentTime())
    {
    }

    /* returns elapsed time since last reset */
    Time Timer::getElapsedTime(void) const
    {
        return getCurrentTime() - this->startTime;
    }

    /* returns elapsed time since last reset and reset the time */
    Time Timer::restart(void)
    {
        Time now = getCurrentTime();
        Time elapsed = now - this->startTime;
        this->startTime = now;

        return elapsed;
    }
};
