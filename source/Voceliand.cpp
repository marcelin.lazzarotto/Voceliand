/*
 *  File:   Voceliand.cpp
 *  Author: Anenzoras
 *  Date:   2017-01-28
 */

/* standard library include */
#include <cstdio>  // TODO remove by a proper log class
#include <cstdlib> // EXIT_SUCCESS, EXIT_FAILURE
#include <map>     // std::map to use for deviceRefs
#include <string>  // std::string to use for deviceRefs

/* external library include */
#include <GL/glew.h>        // cross-platform opengl loader
#include <GLFW/glfw3.h>     // cross-platform windows opener
#include <imgui.h>          // cross-platform imgui library

/* project include */
#include "../include/Status.hpp"
#include "../include/UI/UI.hpp"
#include "../include/Time.hpp"
#include "../include/Timer.hpp"
#include "../include/Sleep.hpp"
#include "../include/Audio/SoundIoInterface.hpp"
#include "../include/Audio/AudioInputDevice.hpp"
#include "../include/Audio/AudioConfiguration.hpp"
#include "../include/Audio/AudioForwarding.hpp"
#include "../include/SoundEffect/DynamicRangeCompressor.hpp"

/*
 * OVER-MEH-
 */
// TODO change this
// deviceRefs arrays
std::vector<char *> deviceIds;
std::vector<char *> deviceNames;
std::vector<int> deviceIndexes;
int selectedDeviceRef                           = 0;
Voceliand::AudioInputDevice * usedDevice        = NULL;
/* TODO in the future allow for several SoundEffect */
Voceliand::DynamicRangeCompressor compressor;
/* TODO in the future: change this too. Thoses are the current "entry points"
 * for compression. */
std::vector<char> samples;
std::vector<char> processedSamples;
/* </> */

/* got from a libsoundio example: list_device function
https://github.com/andrewrk/libsoundio/blob/master/example/sio_list_devices.c*/
void listInputDevices(struct SoundIo * soundio)
{
    int inputCount = soundio_input_device_count(soundio);

    /* -MEH- */
    deviceNames.clear();
    deviceIds.clear();
    deviceIndexes.clear();

    for (int i = 0; i < inputCount; i++)
    {
        struct SoundIoDevice * device = soundio_get_input_device(soundio, i);

        /*
         *  TODO: only put non-raw device for now
         */
        if(!device->is_raw)
        {
            deviceNames.push_back(device->name);
            deviceIds.push_back(device->id);
            deviceIndexes.push_back(i);
        }

        soundio_device_unref(device);
    }
}

/**
 *  @function onDeviceChanged
 *      Callback function for soundio to call whenever a device changed.
 *      (Should only be called when the list of device changed.)
 */
void onDeviceChanged(void)
{
    /*
    TODO
    log.debug("Device changed\n");

    if (active_device is not in soundio)
        stopEverything();
    */

    listInputDevices(Voceliand::SoundIoInterface::getInstance().soundio);
}

/**
 *  @function startRecording
 *      All routines to start the audio recording (and compression).
 */
Voceliand::Status::Code startRecording(void)
{
    if(usedDevice)
    {
        return Voceliand::Status::SUCCESS;
    }

    usedDevice = new Voceliand::AudioInputDevice(/* isRaw */ false,
        deviceIds[selectedDeviceRef]);
    if(!usedDevice->isGood())
    {
        fprintf(stderr, "Unable to properly create device.\n");
        delete usedDevice;

        return Voceliand::Status::FAILURE_DEVICE;
    }

    /* Update the audio configuration. */
    Voceliand::AudioConfiguration::sampleInfo.size = usedDevice->getSampleSize();
    Voceliand::AudioConfiguration::sampleInfo.bitDepth = Voceliand::AudioConfiguration::sampleInfo.size * 8;
    Voceliand::AudioConfiguration::sampleInfo.rate = usedDevice->getSampleRate();
    Voceliand::AudioConfiguration::sampleInfo.channelCount = usedDevice->getChannelCount();
    enum SoundIoFormat format = usedDevice->getFormat();
    switch (format) /* -OVERMEH- */
    {
        case SoundIoFormatFloat32NE:
        case SoundIoFormatFloat32FE:
        case SoundIoFormatFloat64NE:
        case SoundIoFormatFloat64FE:
            Voceliand::AudioConfiguration::sampleInfo.format = Voceliand::AudioConfiguration::Format_FLOAT;
            switch(format)
            {
                case SoundIoFormatFloat32FE:
                case SoundIoFormatFloat64FE:
                    Voceliand::AudioConfiguration::sampleInfo.endianness = Voceliand::AudioConfiguration::Endianness_FOREIGN;
                    break;
                default:
                    Voceliand::AudioConfiguration::sampleInfo.endianness = Voceliand::AudioConfiguration::Endianness_NATIVE;
                    break;
            }
            break;

        case SoundIoFormatS32NE:
        case SoundIoFormatS32FE:
        case SoundIoFormatS24NE:
        case SoundIoFormatS24FE:
        case SoundIoFormatS16NE:
        case SoundIoFormatS16FE:
        case SoundIoFormatS8:
            Voceliand::AudioConfiguration::sampleInfo.format = Voceliand::AudioConfiguration::Format_SIGNED_PCM;
            switch(format)
            {
                case SoundIoFormatS32FE:
                case SoundIoFormatS24FE:
                case SoundIoFormatS16FE:
                    Voceliand::AudioConfiguration::sampleInfo.endianness = Voceliand::AudioConfiguration::Endianness_FOREIGN;
                    break;
                default:
                    Voceliand::AudioConfiguration::sampleInfo.endianness = Voceliand::AudioConfiguration::Endianness_NATIVE;
                    break;
            }
            break;
        default:
            Voceliand::AudioConfiguration::sampleInfo.format = Voceliand::AudioConfiguration::Format_UNSIGNED_PCM;
            switch(format)
            {
                case SoundIoFormatU32FE:
                case SoundIoFormatU24FE:
                case SoundIoFormatU16FE:
                    Voceliand::AudioConfiguration::sampleInfo.endianness = Voceliand::AudioConfiguration::Endianness_FOREIGN;
                    break;
                default:
                    Voceliand::AudioConfiguration::sampleInfo.endianness = Voceliand::AudioConfiguration::Endianness_NATIVE;
                    break;
            }
            break;
    } /* OVER-MEH- */

    /* TODO init compressor */
    /* TODO -DELAMERDE- another way: update attack and release to allow
    * recalculation using current sampleRate within the compressor. */
    compressor.setInput(&samples);
    compressor.setOutput(&processedSamples);
    compressor.setAttack(compressor.getAttack());
    compressor.setRelease(compressor.getRelease());

    Voceliand::Status::Code status;

    status = Voceliand::AudioForwarding::setUp();

    if(status != Voceliand::Status::SUCCESS)
    {
        compressor.setInput(NULL);
        compressor.setOutput(NULL);

        Voceliand::AudioConfiguration::unsetAll();

        delete usedDevice;
    }

    fflush(stderr);

    return Voceliand::Status::SUCCESS;
}

/**
 *  @function stopRecording
 *      Stop audio recording and compression.
 */
void stopRecording(void)
{
    #define FREE_IF_NOT_NULL(pointer) \
    {\
        if(pointer)\
        {\
            delete pointer;\
            pointer = NULL;\
        }\
    }\

    /* Deactivate forwarding. */
    Voceliand::AudioForwarding::tearDown();

    /* Unset compressor. */
    compressor.setInput(NULL);
    compressor.setOutput(NULL);

    /* Reset configuration. */
    Voceliand::AudioConfiguration::unsetAll();

    /* Remove the rest. */
    FREE_IF_NOT_NULL(usedDevice);

    #undef FREE_IF_NOT_NULL
}

/**
 *  @function processInput
 *      Process mouse/keyboards input.
 *      Gamedev deformation, I guess.
 *      Shall be used anyway when rewriting UI. Shall be rewrited alongside UI
 *      by the way, since copy pasted code contains input processing too (did I
 *      hear someone say 'bad design'?).
 */
void processInput(void)
{
    glfwPollEvents();
}

/**
 *  @function updateWindow
 *      Function to gather all the window creation thingies, for clarity.
 *
 *  @returns {bool}
 *      TRUE if update should toggle recording.
 *      FALSE to change nothing.
 */
bool updateWindow(void)
{
    bool selectedChanged = false;
    bool buttonPressed   = false;
    /* FIXME (DisableCombo) when ImGui will support disabled state on Combo */
    int previous = selectedDeviceRef;
    int displayW, displayH;
    ImVec2 size;

    glfwGetWindowSize(Voceliand::UI::window, &displayW, &displayH);
    size.x = displayW;
    size.y = displayH;

    // create the windows
    // flags necessary to make one full window.
    ImGui::Begin("Voceliand", NULL, ImGuiWindowFlags_NoTitleBar
        | ImGuiWindowFlags_NoResize
        | ImGuiWindowFlags_NoMove
        | ImGuiWindowFlags_NoScrollbar
        | ImGuiWindowFlags_NoSavedSettings);
    ImGui::SetWindowSize("Voceliand", size);
    selectedChanged = ImGui::Combo("Input device", &selectedDeviceRef,
        deviceNames.data(), deviceNames.size(), deviceNames.size());
    (void) selectedChanged; /* -MEH- */

    if (usedDevice)
    {
        /* FIXME (DisableCombo) act as if Combo is disabled */
        selectedDeviceRef = previous;

        buttonPressed = ImGui::Button("Stop");
    }
    else /* if (!usedDevice) */
    {
        buttonPressed = ImGui::Button("Start");
    }

    ImGui::Text("\n\n\n");
    ImGui::Text("Dgb : Size - x: %4.4f", size.x);
    ImGui::Text("Dgb : Size - y: %4.4f", size.y);
    ImGui::End();

    return buttonPressed;
}

/**
 *  @function update
 *      Update all informations about the frame before rendering.
 *      Gamedev deformation, I guess.
 *      Shall be used anyway when rewriting UI.
 */
void update(void)
{
    bool toggleRecording = false;

    Voceliand::UI::newFrame();

    /* TODO -BAD- */
    toggleRecording = updateWindow();

    if(toggleRecording && !usedDevice)
    {
        /* TODO: treat errors */
        startRecording();
    }
    else if(toggleRecording && usedDevice)
    {
        stopRecording();
    }

    /* using samples */
    if(usedDevice)
    {
        samples.clear(); /* -MEH-? */

        soundio_flush_events(Voceliand::SoundIoInterface::getInstance().soundio);

        usedDevice->pollSamples(samples);

        /* Pass through compressor
         * TODO: will need to be rewritten for "Make it better" */
        compressor.process(); /* HERE GOES NOTHING */

        Voceliand::AudioForwarding::temp__SendAnteSamples(samples);
        Voceliand::AudioForwarding::sendSamples(processedSamples);
    }
}

/**
 *  @function render
 *      Call the rendering routines to render a frame.
 *      Gamedev deformation, I guess.
 *      Shall be used anyway when rewriting UI.
 */
void render(void)
{
    static ImVec4 const clearColor = ImColor(114, 144, 154);

    int displayW, displayH;

    glfwGetFramebufferSize(Voceliand::UI::window, &displayW, &displayH);
    glViewport(0, 0, displayW, displayH);
    glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
    glClear(GL_COLOR_BUFFER_BIT);
    ImGui::Render();
    glfwSwapBuffers(Voceliand::UI::window);
}

/* Voceliand entry point */
int main()
{
    // enum SoundIoBackend backend;
    // (void) backend;

    Voceliand::Status::Code status;

    status = Voceliand::UI::initialize();
    switch (status)
    {
        case Voceliand::Status::INITIALIZATION_GLFW_FAILURE:
            fprintf(stderr, "Failed to initialize GLFW\n");
            return EXIT_FAILURE;
            break;
        case Voceliand::Status::INITIALIZATION_GLEW_FAILURE:
            fprintf(stderr, "Failed to initialize GLEW\n");
            return EXIT_FAILURE;
            break;
        default:
            break;
    }

    /* TODO
     * Voceliand::SoundIoInterface::getInstance().soundio.on_devices_change = &onDeviceChanged;
     */
    listInputDevices(Voceliand::SoundIoInterface::getInstance().soundio);

    /* For sleeping */
    Voceliand::Timer loopTimer;

    /* Main loop. */
    while (!glfwWindowShouldClose(Voceliand::UI::window))
    {
        processInput();

        update();

        render();

        /* Be sure to lock FPS count, to avoid breaking the processor. */
        Voceliand::sleep(Voceliand::Time::inSeconds(1 / 60) - loopTimer.restart());
    }

    /* Cleanup. */
    Voceliand::UI::shutdown();

    return EXIT_SUCCESS;
}
