/*
 *  File:   UI.cpp
 *  Author: Anenzoras
 *  Date:   2017-03-29
 */

/* standard library include */
#include <cstdio> // fprintf, stderr

/* external library include */
#include <GL/glew.h>        // cross-platform opengl loader
#include <GLFW/glfw3.h>     // cross-platform windows opener
#include <imgui.h>          // cross-platform imgui library

/* project include */
#include "../../include/UI/UI.hpp"
#include "../../include/UI/GLStateSave.hpp"

namespace Voceliand
{
    namespace UI
    {
        /* DISCLAIMER:
         * Most of those things comes from dear imgui opengl3 example code.
         * I read a few of it but not everything, just getting it to work is
         * enough for now.
         */

        // Data
        GLFWwindow * window;

        bool mousePressed[3] = {false, false, false};
        float mouseWheel = 0.0f;

        static double   deltaTime = 0.0f;

        static GLuint   fontTextureId = 0;
        static GLuint   shaderProgramId = 0, vertexShaderId = 0, fragmentShaderId = 0;
        static GLuint   vertexBufferObjectId = 0, vertexArrayObjectId = 0,
                        elementsId = 0;
        static GLint    attribLocationTexId = 0, attribLocationProjMtxId = 0;
        static GLint    attribLocationPositionId = 0, attribLocationUVId = 0,
                        attribLocationColorId = 0;

        static const int WINDOW_DEFAULT_SIZE_X = 600;
        static const int WINDOW_DEFAULT_SIZE_Y = 400;

        /**
         *  @function ImGuiCallback_GetClipboardText
         *      User function callback to retrieve clipboard text.
         *
         *  @param {void *} userData
         *      The pointer to the window as given to dear imgui. Actually
         *      a {GLFWwindow *}
         *
         *  @returns {const char *}
         *      The clipboard text.
         */
        const char * ImGuiCallback_GetClipboardText(void * userData)
        {
            return glfwGetClipboardString((GLFWwindow *) userData);
        }

        /**
         *  @function ImGuiCallback_SetClipboardText
         *      User function callback to write into clipboard text.
         *
         *  @param {void *} userData
         *      The pointer to the window as given to dear imgui.
         *  @param {const char *} text
         *      The text to the write in the clipboard.
         */
        void ImGuiCallback_SetClipboardText(void * userData, const char * text)
        {
            glfwSetClipboardString((GLFWwindow *) userData, text);
        }

        /**
         *  @function ImGuiCallback_RenderDrawLists
         *      Callback to render with dear imgui.
         *
         *      "This is the main rendering function that you have to implement
         *      and provide to ImGui (via setting up 'RenderDrawListsFn' in the
         *      ImGuiIO structure). If text or lines are blurry when integrating
         *      ImGui in your engine: in your Render function, try translating
         *      your projection matrix by (0.5f, 0.5f) or (0.375f, 0.375f)
         *
         *  @param {ImDrawData *} drawData
         *
         */
        void ImGuiCallback_RenderDrawLists(ImDrawData * drawData)
        {
            ImGuiIO & io = ImGui::GetIO();

            // Avoid rendering when minimized
            // (screen coordinates != framebuffer coordinates)
            int fbWidth  = (int) (io.DisplaySize.x
                * io.DisplayFramebufferScale.x);
            int fbHeight = (int) (io.DisplaySize.y
                * io.DisplayFramebufferScale.y);
            if (fbWidth == 0 || fbHeight == 0)
            {
                return;
            }

            // scale coordinates for retina displays
            drawData->ScaleClipRects(io.DisplayFramebufferScale);

            // Backup GL state, RAII style
            GLStateSave previousState;

            // Setup render state: alpha-blending enabled, no face culling,
            // no depth testing, scissor enabled
            glEnable(GL_BLEND);
            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            glDisable(GL_CULL_FACE);
            glDisable(GL_DEPTH_TEST);
            glEnable(GL_SCISSOR_TEST);
            glActiveTexture(GL_TEXTURE0);

            // Setup viewport, orthographic projection matrix
            glViewport(0, 0, (GLsizei) fbWidth, (GLsizei) fbHeight);
            const float orthoProjection[4][4] =
            {
                { 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
                { 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
                { 0.0f,                  0.0f,                  -1.0f, 0.0f },
                {-1.0f,                  1.0f,                   0.0f, 1.0f },
            };
            glUseProgram(shaderProgramId);
            glUniform1i(attribLocationTexId, 0);
            glUniformMatrix4fv(attribLocationProjMtxId, 1, GL_FALSE,
                &orthoProjection[0][0]);
            glBindVertexArray(vertexArrayObjectId);

            for (int n = 0; n < drawData->CmdListsCount; n++)
            {
                const ImDrawList * cmdList = drawData->CmdLists[n];
                const ImDrawIdx * idxBufferOffset = 0;

                glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectId);
                glBufferData(GL_ARRAY_BUFFER,
                    (GLsizeiptr) cmdList->VtxBuffer.Size * sizeof(ImDrawVert),
                    (const GLvoid *) cmdList->VtxBuffer.Data, GL_STREAM_DRAW);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsId);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                    (GLsizeiptr) cmdList->IdxBuffer.Size * sizeof(ImDrawIdx),
                    (const GLvoid *) cmdList->IdxBuffer.Data, GL_STREAM_DRAW);

                // for each command
                for (int i = 0; i < cmdList->CmdBuffer.Size; i++)
                {
                    const ImDrawCmd * command = &cmdList->CmdBuffer[i];

                    if (command->UserCallback)
                    {
                        command->UserCallback(cmdList, command);
                    }
                    else
                    {
                        glBindTexture(GL_TEXTURE_2D,
                            (GLuint)(intptr_t) command->TextureId);
                        glScissor((int) command->ClipRect.x,
                            (int) (fbHeight - command->ClipRect.w),
                            (int) (command->ClipRect.z - command->ClipRect.x),
                            (int) (command->ClipRect.w - command->ClipRect.y));
                        glDrawElements(GL_TRIANGLES, (GLsizei) command->ElemCount,
                            sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT,
                            idxBufferOffset);
                    }

                    idxBufferOffset += command->ElemCount;
                }
            }

            // gl state is restored as previousState goes out of scope
        }

        /**
         *  @function initializeImgui
         *      Initialize dear imgui. Set the key values for its IO KeyMap.
         *      And set the drawing and clipboarding callbacks
         */
        void initializeImgui(void)
        {
            ImGuiIO & io = ImGui::GetIO();

            // Keyboard mapping. ImGui will use those indices to peek into the
            // io.KeyDown[] array.
            io.KeyMap[ImGuiKey_Tab]         = GLFW_KEY_TAB;
            io.KeyMap[ImGuiKey_LeftArrow]   = GLFW_KEY_LEFT;
            io.KeyMap[ImGuiKey_RightArrow]  = GLFW_KEY_RIGHT;
            io.KeyMap[ImGuiKey_UpArrow]     = GLFW_KEY_UP;
            io.KeyMap[ImGuiKey_DownArrow]   = GLFW_KEY_DOWN;
            io.KeyMap[ImGuiKey_PageUp]      = GLFW_KEY_PAGE_UP;
            io.KeyMap[ImGuiKey_PageDown]    = GLFW_KEY_PAGE_DOWN;
            io.KeyMap[ImGuiKey_Home]        = GLFW_KEY_HOME;
            io.KeyMap[ImGuiKey_End]         = GLFW_KEY_END;
            io.KeyMap[ImGuiKey_Delete]      = GLFW_KEY_DELETE;
            io.KeyMap[ImGuiKey_Backspace]   = GLFW_KEY_BACKSPACE;
            io.KeyMap[ImGuiKey_Enter]       = GLFW_KEY_ENTER;
            io.KeyMap[ImGuiKey_Escape]      = GLFW_KEY_ESCAPE;
            io.KeyMap[ImGuiKey_A]           = GLFW_KEY_A;
            io.KeyMap[ImGuiKey_C]           = GLFW_KEY_C;
            io.KeyMap[ImGuiKey_V]           = GLFW_KEY_V;
            io.KeyMap[ImGuiKey_X]           = GLFW_KEY_X;
            io.KeyMap[ImGuiKey_Y]           = GLFW_KEY_Y;
            io.KeyMap[ImGuiKey_Z]           = GLFW_KEY_Z;

            // Alternatively you can set this to NULL and call
            // ImGui::GetDrawData() after ImGui::Render() to get the same
            // ImDrawData pointer:
            io.RenderDrawListsFn  = ImGuiCallback_RenderDrawLists;
            io.SetClipboardTextFn = ImGuiCallback_SetClipboardText;
            io.GetClipboardTextFn = ImGuiCallback_GetClipboardText;
            io.ClipboardUserData  = window;
        }

        /*
         *  @function GLFWCallback_error
         *      Passed to glfw in case of error. Simply prints the error to
         *      stderr.
         *
         *  @param {int} error
         *      Error code as given by GLFW.
         *  @param {const char *} description
         *      Error description as given by GLFW.
         */
        static void GLFWCallback_error(int error, const char * description)
        {
            fprintf(stderr, "Error %d: %s\n", error, description);
        }

        /**
         *  @function GLFWCallback_MouseButton
         *      Callback to pass to glfw when a mouse button has been pressed.
         *      Function signature defined by what glfw expects.
         *
         *  @param {GLFWwindow *} window
         *      The glfw window. Unused.
         *  @param {int} button
         *      The button pressed.
         *  @param {int} action
         *      The action.
         *  @param {int} mods
         *      Mods key implied. Unused.
         */
        void GLFWCallback_MouseButton(GLFWwindow * window, int button,
            int action, int mods)
        {
            (void) window; // unused
            (void) mods;   // unused

            // mouse released is treated during new frame function
            if (action == GLFW_PRESS && button >= 0 && button < 3)
            {
                mousePressed[button] = true;
            }
        }

        /**
         *  @function GLFWCallback_Scroll
         *      Callback to pass to glfw when scrolling occurs.
         *      Function signature defined by what glfw expects.
         *
         *  @param {GLFWwindow *} window
         *      The glfw window. Unused.
         *  @param {int} xoffset
         *      Horizontal scroll. Unused.
         *  @param {int} yoffset
         *      Vertical scroll.
         */
        void GLFWCallback_Scroll(GLFWwindow * window, double xoffset,
            double yoffset)
        {
            (void) window;  // unused
            (void) xoffset; // unused

            // Use fractional mouse wheel, 1.0 unit 5 lines.
            mouseWheel += (float) yoffset;
        }

        /**
         *  @function GLFWCallback_Key
         *      Callback to pass to glfw when a key event occurs.
         *      Function signature defined by what glfw expects.
         *
         *  @param {GLFWwindow *} window
         *      The glfw window. Unused.
         *  @param {int} key
         *      Key on which the event occured.
         *  @param {int}
         *      ?
         *  @param {int} action
         *      The action of the event.
         *  @param {int} mods
         *      Modifier key
         */
        void GLFWCallback_Key(GLFWwindow * window, int key, int, int action,
            int mods)
        {
            (void) window; // unused
            (void) mods; // Modifiers are not reliable across systems

            ImGuiIO& io = ImGui::GetIO();

            if (action == GLFW_PRESS)
            {
                io.KeysDown[key] = true;
            }
            if (action == GLFW_RELEASE)
            {
                io.KeysDown[key] = false;
            }

            // custom-made modifier reader
            io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL]
                || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
            io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT]
                || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
            io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT]
                || io.KeysDown[GLFW_KEY_RIGHT_ALT];
            io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER]
                || io.KeysDown[GLFW_KEY_RIGHT_SUPER];
        }

        /**
         *  @function GLFWCallback_Key
         *      Callback to pass to glfw when a key event occurs.
         *      Function signature defined by what glfw expects.
         *
         *  @param {GLFWwindow *} window
         *      The glfw window. Unused.
         *  @param {unsigned int} c
         *      Int-sized character.
         */
        void GLFWCallback_Char(GLFWwindow * window, unsigned int c)
        {
            (void) window;

            ImGuiIO & io = ImGui::GetIO();

            if (c > 0 && c < 0x10000)
            {
                io.AddInputCharacter((unsigned short) c);
            }
        }

        /**
         *  @function GLFWCallback_setUp
         *      Set up the MouseButton/Scroll/Key/Char callbacks to glfw.
         */
        void GLFWCallback_setUp(void)
        {
            glfwSetMouseButtonCallback(window, &GLFWCallback_MouseButton);
            glfwSetScrollCallback(window, &GLFWCallback_Scroll);
            glfwSetKeyCallback(window, &GLFWCallback_Key);
            glfwSetCharCallback(window, &GLFWCallback_Char);
        }

        /**
         *  @function initialize
         *      Library entry point, completelly initialize glfw/glew/soundio/dear
         *      imgui.
         *
         *  @returns {Status::Code}
         *      Status::INITIALIZATION_GLFW_FAILURE if glfw couldn't initialize
         *      properly.
         *      Status::INITIALIZATION_GLEW_FAILURE if glew couldn't initialize
         *      properly.
         *      Status::SUCESS if all went well.
         */
        Status::Code initialize(void)
        {
            // Setup window
            // Initialize GLFW
            if (glfwInit() != GLFW_TRUE)
            {
                return Status::INITIALIZATION_GLFW_FAILURE;
            }
            glfwSetErrorCallback(&GLFWCallback_error);

            // use opengl 3.3, core functionalities
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            // to solve apple warning/error (useless for now, but then it's cleared)
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

            // be sure to create the window before glew happens.
            window = glfwCreateWindow(WINDOW_DEFAULT_SIZE_X,
                WINDOW_DEFAULT_SIZE_Y, "Voceliand", NULL, NULL);
            glfwMakeContextCurrent(window);

            // Initialize GLEW
            if (glewInit() != GLEW_OK)
            {
                return Status::INITIALIZATION_GLEW_FAILURE;
            }

            // Set ImGui binding up
            initializeImgui();

            // Set the callbacks up
            GLFWCallback_setUp();

            return Status::SUCCESS;
        }

        /**
         *  @function invalidateDeviceObjects
         *      Delete all OpenGL objects used.
         */
        void invalidateDeviceObjects(void)
        {
            if (vertexArrayObjectId)
            {
                glDeleteVertexArrays(1, &vertexArrayObjectId);
            }
            if (vertexBufferObjectId)
            {
                glDeleteBuffers(1, &vertexBufferObjectId);
            }
            if (elementsId)
            {
                glDeleteBuffers(1, &elementsId);
            }

            vertexArrayObjectId = vertexBufferObjectId = elementsId = 0;

            if (shaderProgramId && vertexShaderId)
            {
                glDetachShader(shaderProgramId, vertexShaderId);
            }
            if (vertexShaderId)
            {
                glDeleteShader(vertexShaderId);
            }
            vertexShaderId = 0;

            if (shaderProgramId && fragmentShaderId)
            {
                glDetachShader(shaderProgramId, fragmentShaderId);
            }
            if (fragmentShaderId)
            {
                glDeleteShader(fragmentShaderId);
            }
            fragmentShaderId = 0;

            if (shaderProgramId)
            {
                glDeleteProgram(shaderProgramId);
            }
            shaderProgramId = 0;

            if (fontTextureId)
            {
                glDeleteTextures(1, &fontTextureId);
                ImGui::GetIO().Fonts->TexID = 0;
                fontTextureId = 0;
            }
        }

        /**
         *  @function shutdown
         *      Deactivate user interface by shuting down devices, dear imgui
         *      and glfw.
         */
        void shutdown(void)
        {
            invalidateDeviceObjects();
            ImGui::Shutdown();
            glfwTerminate();
        }

        /**
         *  @function createFontsTexture
         *      Creates texture for fonts.
         */
        void createFontsTexture(void)
        {
            // Build texture atlas
            ImGuiIO & io = ImGui::GetIO();

            unsigned char * pixels;
            int width, height;

            // Load as RGBA 32-bits (75% of the memory is wasted, but default font is
            // so small) because it is more likely to be compatible with user's existing
            // shaders. If your ImTextureId represent a higher-level concept than just
            // a GL texture id, consider calling GetTexDataAsAlpha8() instead to save
            // on GPU memory.
            io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

            // Upload texture to graphics system
            GLint lastTexture;
            glGetIntegerv(GL_TEXTURE_BINDING_2D, &lastTexture);
            glGenTextures(1, &fontTextureId);
            glBindTexture(GL_TEXTURE_2D, fontTextureId);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
                GL_UNSIGNED_BYTE, pixels);

            // Store our identifier
            io.Fonts->TexID = (void *)(intptr_t) fontTextureId;

            // Restore state
            glBindTexture(GL_TEXTURE_2D, lastTexture);
        }

        /**
         *  @function createDeviceObjects
         *      Does things. Create shaders program and links them.
         *      And does things with texture.
         */
        void createDeviceObjects(void)
        {
            // Backup GL state
            GLint lastTexture, lastArrayBuffer, lastVertexArray;
            glGetIntegerv(GL_TEXTURE_BINDING_2D, &lastTexture);
            glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &lastArrayBuffer);
            glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &lastVertexArray);

            const GLchar * vertexShader =
                "#version 330\n"
                "uniform mat4 ProjMtx;\n"
                "in vec2 Position;\n"
                "in vec2 UV;\n"
                "in vec4 Color;\n"
                "out vec2 Frag_UV;\n"
                "out vec4 Frag_Color;\n"
                "void main()\n"
                "{\n"
                "    Frag_UV = UV;\n"
                "    Frag_Color = Color;\n"
                "    gl_Position = ProjMtx * vec4(Position.xy, 0, 1);\n"
                "}\n";

            const GLchar * fragmentShader =
                "#version 330\n"
                "uniform sampler2D Texture;\n"
                "in vec2 Frag_UV;\n"
                "in vec4 Frag_Color;\n"
                "out vec4 Out_Color;\n"
                "void main()\n"
                "{\n"
                "    Out_Color = Frag_Color * texture(Texture, Frag_UV.st);\n"
                "}\n";

            shaderProgramId = glCreateProgram();
            vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
            fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(vertexShaderId, 1, &vertexShader, 0);
            glShaderSource(fragmentShaderId, 1, &fragmentShader, 0);
            glCompileShader(vertexShaderId);
            glCompileShader(fragmentShaderId);
            glAttachShader(shaderProgramId, vertexShaderId);
            glAttachShader(shaderProgramId, fragmentShaderId);
            glLinkProgram(shaderProgramId);

            attribLocationTexId = glGetUniformLocation(shaderProgramId,
                "Texture");
            attribLocationProjMtxId = glGetUniformLocation(shaderProgramId,
                "ProjMtx");
            attribLocationPositionId = glGetAttribLocation(shaderProgramId,
                "Position");
            attribLocationUVId = glGetAttribLocation(shaderProgramId,
                "UV");
            attribLocationColorId = glGetAttribLocation(shaderProgramId,
                "Color");

            glGenBuffers(1, &vertexBufferObjectId);
            glGenBuffers(1, &elementsId);

            glGenVertexArrays(1, &vertexArrayObjectId);
            glBindVertexArray(vertexArrayObjectId);
            glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectId);
            glEnableVertexAttribArray(attribLocationPositionId);
            glEnableVertexAttribArray(attribLocationUVId);
            glEnableVertexAttribArray(attribLocationColorId);

            {
                #define OFFSETOF(TYPE, ELEMENT) ((size_t) &(((TYPE *) 0)->ELEMENT))
                glVertexAttribPointer(attribLocationPositionId, 2, GL_FLOAT,
                    GL_FALSE, sizeof(ImDrawVert),
                    (GLvoid*) OFFSETOF(ImDrawVert, pos));
                glVertexAttribPointer(attribLocationUVId, 2, GL_FLOAT, GL_FALSE,
                    sizeof(ImDrawVert), (GLvoid*) OFFSETOF(ImDrawVert, uv));
                glVertexAttribPointer(attribLocationColorId, 4, GL_UNSIGNED_BYTE,
                    GL_TRUE, sizeof(ImDrawVert),
                    (GLvoid*) OFFSETOF(ImDrawVert, col));
                #undef OFFSETOF
            }

            createFontsTexture();

            // Restore modified GL state
            glBindTexture(GL_TEXTURE_2D, lastTexture);
            glBindBuffer(GL_ARRAY_BUFFER, lastArrayBuffer);
            glBindVertexArray(lastVertexArray);
        }

        void newFrame(void)
        {
            if (!fontTextureId)
            {
                createDeviceObjects();
            }

            ImGuiIO & io = ImGui::GetIO();

            // Setup display size (every frame to accommodate for window
            // resizing)
            int windowW, windowH;
            int displayW, displayH;

            glfwGetWindowSize(Voceliand::UI::window, &windowW, &windowH);
            glfwGetFramebufferSize(Voceliand::UI::window, &displayW, &displayH);

            io.DisplaySize = ImVec2((float) windowW, (float) windowH);
            io.DisplayFramebufferScale = ImVec2(
                windowW > 0 ? ((float) displayW / windowW) : 0,
                windowH > 0 ? ((float) displayH / windowH) : 0);

            // Setup time step
            double currentTime = glfwGetTime();
            io.DeltaTime = deltaTime > 0.0 ? (float) (currentTime - deltaTime) :
                (float)(1.0f/60.0f);
            deltaTime = currentTime;

            // Setup inputs
            // (we already got mouse wheel, keyboard keys & characters from glfw
            // callbacks polled in glfwPollEvents())
            if (glfwGetWindowAttrib(Voceliand::UI::window, GLFW_FOCUSED))
            {
                double mouse_x, mouse_y;

                glfwGetCursorPos(Voceliand::UI::window, &mouse_x, &mouse_y);

                // Mouse position in screen coordinates
                // (set to -1,-1 if no mouse / on another screen, etc.)
                io.MousePos = ImVec2((float)mouse_x, (float)mouse_y);
            }
            else
            {
                io.MousePos = ImVec2(-1, -1);
            }

            for (int i = 0; i < 3; i++)
            {
                // If a mouse press event came, always pass it as "mouse held
                // this frame", so we don't miss click-release events that are
                // shorter than 1 frame.
                io.MouseDown[i] = mousePressed[i]
                    || glfwGetMouseButton(Voceliand::UI::window, i) != 0;
                mousePressed[i] = false;
            }

            io.MouseWheel = mouseWheel;
            mouseWheel = 0.0f;

            // Hide OS mouse cursor if ImGui is drawing it
            glfwSetInputMode(Voceliand::UI::window, GLFW_CURSOR,
                io.MouseDrawCursor ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);

            // Start the frame
            ImGui::NewFrame();
        }
    };
};
