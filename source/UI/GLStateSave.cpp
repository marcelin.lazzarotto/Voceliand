/*
 *  File:   GLStateSave.cpp
 *  Author: Anenzoras
 *  Date:   2017-03-29
 */

/* standard library include */
/* external library include */
/* project include */
#include "../../include/UI/GLStateSave.hpp"

namespace Voceliand
{
    namespace UI
    {
        /* Default constructor. Stores GL state. */
        GLStateSave::GLStateSave(void)
        {
            // Backup GL state -- operations
            glGetIntegerv(GL_CURRENT_PROGRAM, &program);
            glGetIntegerv(GL_TEXTURE_BINDING_2D, &texture);
            glGetIntegerv(GL_ACTIVE_TEXTURE, &activeTexture);
            glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &arrayBuffer);
            glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &elementArrayBuffer);
            glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &vertexArray);
            glGetIntegerv(GL_BLEND_SRC, &blendSrc);
            glGetIntegerv(GL_BLEND_DST, &blendDst);
            glGetIntegerv(GL_BLEND_EQUATION_RGB, &blendEquationRgb);
            glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &blendEquationAlpha);
            glGetIntegerv(GL_VIEWPORT, viewport);
            glGetIntegerv(GL_SCISSOR_BOX, scissorBox);
            enableBlend = glIsEnabled(GL_BLEND);
            enableCullFace = glIsEnabled(GL_CULL_FACE);
            enableDepthTest = glIsEnabled(GL_DEPTH_TEST);
            enableScissorTest = glIsEnabled(GL_SCISSOR_TEST);
        }

        /* Default destructor. Restores GL state. */
        GLStateSave::~GLStateSave(void)
        {
            // Restore modified GL state
            glUseProgram(program);
            glActiveTexture(activeTexture);
            glBindTexture(GL_TEXTURE_2D, texture);
            glBindVertexArray(vertexArray);
            glBindBuffer(GL_ARRAY_BUFFER, arrayBuffer);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementArrayBuffer);
            glBlendEquationSeparate(blendEquationRgb, blendEquationAlpha);
            glBlendFunc(blendSrc, blendDst);
            restoreBooleanValue(enableBlend, GL_BLEND);
            restoreBooleanValue(enableCullFace, GL_CULL_FACE);
            restoreBooleanValue(enableDepthTest, GL_DEPTH_TEST);
            restoreBooleanValue(enableScissorTest, GL_SCISSOR_TEST);
            glViewport(viewport[0], viewport[1],
                (GLsizei) viewport[2], (GLsizei) viewport[3]);
            glScissor(scissorBox[0], scissorBox[1],
                (GLsizei) scissorBox[2], (GLsizei) scissorBox[3]);
        }
    };
};
