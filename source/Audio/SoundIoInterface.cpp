/*
 *  File:   SoundIoInterface.cpp
 *  Author: Anenzoras
 *  Date:   2017-04-10
 */

/* standard library includes */
#include <cstdio>
#include <cstdlib>

/* external library includes */
#include <soundio/soundio.h>

/* project includes */
#include "../../include/Audio/SoundIoInterface.hpp"

namespace Voceliand
{
    SoundIoInterface & SoundIoInterface::getInstance(void)
    {
        static SoundIoInterface instance;
        /* Guaranteed to be destroyed.
         * Instantiated on first use. */
        return instance;
    }

    SoundIoInterface::SoundIoInterface(void)
    {
        int error;

        this->soundio = soundio_create();
        if (!this->soundio)
        {
            fprintf(stderr, "out of memory\n");
            fflush(stderr);
            exit(1);
        }

        error = soundio_connect(this->soundio);
        if (error)
        {
            fprintf(stderr, "error connecting: %s", soundio_strerror(error));
            fflush(stderr);
            exit(1); /* -MEH- but kindof okay */
        }

        soundio_flush_events(this->soundio);
    }

    SoundIoInterface::~SoundIoInterface(void)
    {
        soundio_destroy(this->soundio);
    }
}; /* namespace Voceliand */
