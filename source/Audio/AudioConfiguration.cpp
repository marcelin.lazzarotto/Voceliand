/*
 *  File:   AudioConfiguration.cpp
 *  Author: Anenzoras
 *  Date:   2017-04-02
 */

/* standard library includes */
/* external library includes */
/* project includes */
#include "../../include/Audio/AudioConfiguration.hpp"

namespace Voceliand
{
    namespace AudioConfiguration
    {
        SampleInfo sampleInfo;

        /* Set all configuration values to INVALID, or 0. */
        void unsetAll(void)
        {
            sampleInfo.size = 0;
            sampleInfo.bitDepth = 0;
            sampleInfo.rate = 0;
            sampleInfo.channelCount = 0;
            sampleInfo.format = Format_INVALID;
            sampleInfo.endianness = Endianness_INVALID;
        }
    }; /* namespace AudioConfiguration */
}; /* namespace Voceliand */
