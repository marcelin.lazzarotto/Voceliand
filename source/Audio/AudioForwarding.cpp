/*
 *  File:   AudioForwarding.cpp
 *  Author: Anenzoras
 *  Date:   2017-09-09
 */

/* standard library includes */
#include <cstdio>
#include <cstring>

/* external library includes */
/* project includes */
#include "../../include/Status.hpp"
#include "../../include/Audio/AudioForwarding.hpp"
#include "../../include/Audio/AudioConfiguration.hpp"

#include "../../include/AudioFileWriterWAV.hpp"

namespace Voceliand
{
    namespace
    {
        /* TODO debug only, remove this with driver thing */
        Voceliand::AudioFileWriterWAV * fileWriterAnte  = NULL;
        Voceliand::AudioFileWriterWAV * fileWriterPost  = NULL;
    } /* namespace <anonymous> */

    namespace AudioForwarding
    {
        /* Prepare the forwarding: connect to the backend. */
        Status::Code setUp(void)
        {
            /*
             TODO: WHEN DRIVER COMPLETED, CHANGE THIS
             */
            std::string const anteName = "testAnte.wav";
            std::string const postName = "testPost.wav";

            fileWriterAnte = new AudioFileWriterWAV();
            fileWriterPost = new AudioFileWriterWAV();

            // get sample size and format for recording
            if(!fileWriterAnte->open(anteName,
                AudioConfiguration::sampleInfo.rate,
                AudioConfiguration::sampleInfo.channelCount,
                AudioConfiguration::sampleInfo.format ==
                    AudioConfiguration::Format_FLOAT
                        ? AudioFileWriterWAV::FORMAT_FLOAT
                        : AudioFileWriterWAV::FORMAT_PCM,
                AudioConfiguration::sampleInfo.size))
            {
                fprintf(stderr, "unable to open %s: %s\n", anteName.c_str(),
                    strerror(errno));
                delete fileWriterAnte;

                return Status::FAILURE_TODO_FILESTREAM_CRAP;
            }
            if(!fileWriterPost->open(postName,
                AudioConfiguration::sampleInfo.rate,
                AudioConfiguration::sampleInfo.channelCount,
                AudioConfiguration::sampleInfo.format ==
                    AudioConfiguration::Format_FLOAT
                        ? AudioFileWriterWAV::FORMAT_FLOAT
                        : AudioFileWriterWAV::FORMAT_PCM,
                AudioConfiguration::sampleInfo.size))
            {
                fprintf(stderr, "unable to open %s: %s\n", postName.c_str(),
                    strerror(errno));
                delete fileWriterAnte;
                delete fileWriterPost;

                return Status::FAILURE_TODO_FILESTREAM_CRAP;
            }

            return Status::SUCCESS;
        }

        void tearDown(void)
        {
            #define FREE_IF_NOT_NULL(pointer) \
            {\
                if(pointer)\
                {\
                    delete pointer;\
                    pointer = NULL;\
                }\
            }\

            FREE_IF_NOT_NULL(fileWriterAnte); /* TODO */
            FREE_IF_NOT_NULL(fileWriterPost); /* TODO */

            #undef FREE_IF_NOT_NULL
        }

        void /* error ? */ sendSamples(std::vector<char> const & samples)
        {
            /* TODO replace all by OS-dependent communication */
            fileWriterPost->write(&samples.front(),
                samples.size() / AudioConfiguration::sampleInfo.size);
        }

        /* TODO: remove alongside filewriterpost */
        void temp__SendAnteSamples(std::vector<char> const & samples)
        {
            fileWriterAnte->write(&samples.front(),
                samples.size() / AudioConfiguration::sampleInfo.size);
        }
    }; /* namespace AudioForwarding */
}; /* namespace Voceliand */
